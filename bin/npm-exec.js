var fs = require('fs');
var resolve = require('path').resolve;
var join = require('path').join;
var cp = require('child_process');

var command;
var paths = [];

process.argv.forEach(function (val) {
  var arg = val.split("=");
  if (arg[0] === '-command') {
    command = arg[1];
  }

  if (arg[0] === '-path') {
    arg[1].split(',').forEach(function (p) {
      paths.push(p);
    });
  }
});

// get library path
paths.forEach(function (path) {
  var lib = resolve(__dirname, path);
  console.log(`Performing "npm ${command}" for modules`);
  fs.readdirSync(lib)
    .forEach(function (mod) {
      var modPath = join(lib, mod);

      // ensure path has package.json
      if (!fs.existsSync(join(modPath, 'package.json'))) {
        return;
      }
      console.log(`Running "npm ${command}" in ${mod}`);

      if (command === 'uninstall') {
        console.log(`Removing node_modules in ${modPath}, be patient because this may be slow.`);
        if (process.platform === 'win32') {
          cp.spawn('rimraf.cmd', ['node_modules'], {env: process.env, cwd: modPath, stdio: 'inherit'});
        } else {
          cp.spawn('rimraf', ['node_modules'], {env: process.env, cwd: modPath, stdio: 'inherit'});
        }
      } else {
        // Run command
        if (process.platform === 'win32') {
          cp.spawn('npm.cmd', [command], {env: process.env, cwd: modPath, stdio: 'inherit'});
        } else {
          cp.spawn('npm', [command], {env: process.env, cwd: modPath, stdio: 'inherit'});
        }
      }
    });
});
