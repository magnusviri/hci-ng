/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import "./vendor.ts";

import {enableProdMode} from "@angular/core";
import {platformBrowser} from "@angular/platform-browser";

import {environment} from './environments/environment';
import {DemoModule} from "./app/demo.module";

/**
 * The entry point of the demo application.
 *
 * @since 1.0.0
 */

if (environment.production) {
  enableProdMode();
}
 
platformBrowser().bootstrapModule(DemoModule)
  .catch(err => console.error(err));
