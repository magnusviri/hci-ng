import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {MiscModule} from "@huntsman-cancer-institute/misc";
import {NavigationModule} from "@huntsman-cancer-institute/navigation";

import {MiscDemoComponent} from "./misc.component";
import {MISC_ROUTING} from "./misc.routes";
import {MiscHomeComponent} from "./home.component";
import {AccordNavDemoComponent} from "./accord-nav.component";
import {BusyDemoComponent} from "./busy.component";
import {PairedDataDemoComponent} from "./paired-data.component";

/**
 * @since 1.2.0
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    MISC_ROUTING,
    NavigationModule,
    MiscModule
  ],
  declarations: [
    MiscDemoComponent,
    MiscHomeComponent,
    AccordNavDemoComponent,
    BusyDemoComponent,
    PairedDataDemoComponent
  ]
})
export class MiscDemoModule {}
