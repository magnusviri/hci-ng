import {Component, HostBinding} from "@angular/core";

@Component({
  selector: "misc-home",
  template: `
    <div class="d-flex flex-column flex-shrink-1 flex-grow-1 m-0">
      <div class="d-flex flex-column flex-shrink-0 m-3">
        <h3 class="mb-3">Miscellaneous Components</h3>
        <div class="d-flex">
          This demo is for the misc library which is a holder for standalone components that don't belong to any particular
          parent.
        </div>
      </div>
    </div>
  `
})
export class MiscHomeComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

}
