/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {RouterModule, Routes} from "@angular/router";

import {MiscDemoComponent} from "./misc.component";
import {BusyDemoComponent} from "./busy.component";
import {AccordNavDemoComponent} from "./accord-nav.component";
import {PairedDataDemoComponent} from "./paired-data.component";
import {MiscHomeComponent} from "./home.component";


/**
 * @since 1.0.0
 */
const ROUTES: Routes = [
  {path: "", component: MiscDemoComponent,
    children: [
      {path: "", redirectTo: "home", pathMatch: "full"},
      {path: "busy", component: BusyDemoComponent},
      {path: "accord-nav", component: AccordNavDemoComponent},
      {path: "paired-data", component: PairedDataDemoComponent},
      {path: "home", component: MiscHomeComponent}
    ]
  }
];

export const MISC_ROUTING = RouterModule.forChild(ROUTES);
