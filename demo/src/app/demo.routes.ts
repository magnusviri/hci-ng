/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Routes} from "@angular/router";

import {RouteGuardService} from "@huntsman-cancer-institute/authentication";

import {HomeComponent} from "./home/home.component";
import {DocumentationComponent} from "./documentation/documentation.component";
import {HelpDemoComponent} from "./help/help.component";

/**
 * @since 1.2.0
 */
export const ROUTES: Routes = [
  {path: "", redirectTo: "home", pathMatch: "full"},
  {path: "cod", loadChildren: () => import("./cod/cod-demo.module").then(m => m.CodDemoModule)},
  {path: "crud", loadChildren: () => import("./crud/crud.module").then(m => m.CrudDemoModule)},
  {path: "dashboard", loadChildren: () => import("./dashboard/dashboard.module").then(m => m.DashboardDemoModule)},
  {path: "dictionary-editor", loadChildren: () => import("./dictionary/dictionary-editor-wrapper.module").then(m => m.DictionaryEditorModuleWrapper)},
  {path: "documentation", component: DocumentationComponent},
  {path: "help", loadChildren: () => import("./help/help.module").then(m => m.HelpDemoModule)},
  {path: "home", component: HomeComponent},
  {path: "icon", loadChildren: () => import("./icon/icon.module").then(m => m.IconDemoModule)},
  {path: "input", loadChildren: () => import("./input/input.module").then(m => m.InputDemoModule)},
  {path: "misc", loadChildren: () => import("./misc/misc.module").then(m => m.MiscDemoModule)},
  {path: "navigation", loadChildren: () => import("./navigation/navigation.module").then(m => m.NavigationDemoModule)},
  {path: "notification", loadChildren: () => import("./notification/notification.module").then(m => m.NotificationDemoModule)},
  {path: "reporting-framework", loadChildren: () => import("./reporting/reporting-framework.module").then(m => m.ReportingFrameworkDemoModule)}
];
