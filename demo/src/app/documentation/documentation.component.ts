/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, Renderer2} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
import {HttpClient} from "@angular/common/http";

/**
 * Webpack copies the docs generated from the build of the library and includes it in the distribution.
 */
@Component({
  selector: "demo",
  template: `    
    <div class="d-flex flex-column flex-grow-1">
      <div class="d-flex m-3">
        <div ngbDropdown class="d-inline-block">
          <button class="btn btn-outline-primary" id="ddPackage" ngbDropdownToggle>Select Documentation</button>
          <div ngbDropdownMenu aria-labelledby="ddPackage">
            <ng-container *ngFor="let package of packages">
              <button class="dropdown-item" (click)="src = package.url">{{package.name}}</button>
            </ng-container>
          </div>
        </div>
      </div>
      <iframe [src]="src" class="iframe flex-grow-1" *ngIf="src"></iframe>
    </div>
  `,
  host: {class: "outlet-column"}
})
export class DocumentationComponent {

  src: string;
  packages: any = [];

  constructor(private renderer: Renderer2, private http: HttpClient, private sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.http.get("assets/package.json").subscribe((data: any) => {
      let deps: any = data.optionalDependencies;
      for (let dep in deps) {
        this.packages.push({
          name: dep.substring(dep.lastIndexOf("/") + 1),
          url: this.sanitizer.bypassSecurityTrustResourceUrl("compodoc/" + dep.substring(dep.lastIndexOf("/") + 1) + "/index.html")
        });
      }

      this.packages.sort((a: any, b: any) => {
        return a.name.localeCompare(b.name);
      });

      if (this.packages && this.packages.length > 0) {
        this.src = this.packages[0].url;
      }
    });
  }
}
