import {isDevMode} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";

import * as jwt from "jsonwebtoken";
import {CoolLocalStorage} from "@angular-cool/storage";

import {AuthenticationService} from "@huntsman-cancer-institute/authentication";

export class JwtUtil {

  constructor(private authenticationService: AuthenticationService,
              private http: HttpClient,
              private localStorageService: CoolLocalStorage) {}

  authenticate() {
    let headers: HttpHeaders = new HttpHeaders()
        .set("Content-Type", "text/plain; charset=utf-8");

    if (isDevMode()) {
      console.debug("AuthDemoService.authenticate");
    }

    this.http.get("/assets/private.key", {headers: headers, responseType: "text"}).subscribe((privateKey: string) => {
      let payload: Object = {
        username: "user",
        password: "temppass"
      };

      let signOptions: Object = {
        issuer:  "Issuer",
        subject:  "user@test",
        audience:  "localhost",
        expiresIn:  "1h",
        algorithm:  "RS256"
      };

      let token: string = jwt.sign(payload, privateKey, signOptions);

      if (isDevMode()) {
        console.debug("Token: " + token);
      }

      this.authenticationService.storeToken(token);

      this.localStorageService.setItem("token", token);
    });
  }

  getToken() {
    this.localStorageService.getItem("token");
  }
}
