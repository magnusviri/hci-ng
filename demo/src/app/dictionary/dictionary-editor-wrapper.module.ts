import {NgModule} from "@angular/core";

import {DictionaryEditorModule} from "@huntsman-cancer-institute/dictionary-editor";

@NgModule({
  imports: [
    DictionaryEditorModule
  ]
})
export class DictionaryEditorModuleWrapper {}
