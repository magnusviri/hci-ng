import {Injectable, isDevMode} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";

import {Observable, of, throwError} from "rxjs";
import {catchError, delay, mergeMap} from "rxjs/operators";
import {CoolLocalStorage} from "@angular-cool/storage";

import {DemoInterceptor} from "../demo.interceptor";
let DICTIONARIES: any = require("./data/dictionaries.json");
let ADDRESS_TYPE_DROPDOWN_ENTRIES: any = require("./data/address-type.dropdown-entries.json");
let GENDER_ENTRIES: any = require("./data/gender.data.json");
let GENDER_DROPDOWN_ENTRIES: any = require("./data/gender-type.dropdown-entries.json");
let GENDER_DICTIONARY: any = require("./data/gender.dictionary.json");
let NAME_ENTRIES: any = require("./data/name.data.json");
let NAME_DICTIONARY: any = require("./data/name.dictionary.json");
let RACE_ENTRIES: any = require("./data/race.data.json");
let RACE_DICTIONARY: any = require("./data/race.dictionary.json");
let TEST_TARGET_DROPDOWN_ENTRIES: any = require("./data/test-target.dropdown-entries.json");
let TEST_TARGET_ENTRIES: any = require("./data/test-target.data.json");
let TEST_TARGET_DICTIONARY: any = require("./data/test-target.dictionary.json");
let TEST_ALTERATION_ENTRIES: any = require("./data/test-alteration.data.json");
let TEST_ALTERATION_DICTIONARY: any = require("./data/test-alteration.dictionary.json");
let YES_NO_DROPDOWN_ENTRIES: any = require("./data/yes-no.dropdown-entries.json");
let YES_NO_DICTIONARY: any = require("./data/yes-no.dictionary.json");
let SPANISH_SOURCE_ENTRIES: any = require("./data/spanish-source.data.json");
let SPANISH_TYPE_ENTRIES: any = require("./data/spanish-type.data.json");

/**
 *
 */
@Injectable()
export class DictionaryInterceptor extends DemoInterceptor implements HttpInterceptor {

  constructor(private coolLocalStorage: CoolLocalStorage) {
    super();
    coolLocalStorage.setObject("dropdown-entries.hci.ri.core.subject.model.dictionary.AddressType", ADDRESS_TYPE_DROPDOWN_ENTRIES);
    coolLocalStorage.setObject("dictionary.hci.ri.core.subject.model.dictionary.GenderType", GENDER_DICTIONARY);
    coolLocalStorage.setObject("entries.hci.ri.core.subject.model.dictionary.GenderType", GENDER_ENTRIES);
    coolLocalStorage.setObject("dropdown-entries.hci.ri.core.subject.model.dictionary.GenderType", GENDER_DROPDOWN_ENTRIES);
    coolLocalStorage.setObject("dictionary.hci.ri.core.subject.model.dictionary.NameType", NAME_DICTIONARY);
    coolLocalStorage.setObject("entries.hci.ri.core.subject.model.dictionary.NameType", NAME_ENTRIES);
    coolLocalStorage.setObject("dictionary.hci.ri.core.subject.model.dictionary.RaceType", RACE_DICTIONARY);
    coolLocalStorage.setObject("entries.hci.ri.core.subject.model.dictionary.RaceType", RACE_ENTRIES);
    coolLocalStorage.setObject("dictionary.hci.ri.dictionary.model.provideddictionary.YesNoDictionary", YES_NO_DICTIONARY);
    coolLocalStorage.setObject("dropdown-entries.hci.ri.dictionary.model.provideddictionary.YesNoDictionary", YES_NO_DROPDOWN_ENTRIES);
    coolLocalStorage.setObject("dictionary.hci.ri.core.pathology.model.MolecularTestTarget", TEST_TARGET_DICTIONARY);
    coolLocalStorage.setObject("entries.hci.ri.core.pathology.model.MolecularTestTarget", TEST_TARGET_ENTRIES);
    coolLocalStorage.setObject("dropdown-entries.hci.ri.core.pathology.model.MolecularTestTarget", TEST_TARGET_DROPDOWN_ENTRIES);
    coolLocalStorage.setObject("dictionary.hci.ri.core.pathology.model.MolecularTestAlteration", TEST_ALTERATION_DICTIONARY);
    coolLocalStorage.setObject("entries.hci.ri.core.pathology.model.MolecularTestAlteration", TEST_ALTERATION_ENTRIES);
    coolLocalStorage.setObject("entries.hci.updb.model.dict.SpanishSourceDict", SPANISH_SOURCE_ENTRIES);
    coolLocalStorage.setObject("entries.hci.updb.model.dict.SpanishTypeDict", SPANISH_TYPE_ENTRIES);
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isDevMode()) {
      console.debug("DictionaryInterceptor.intercept: " + request.url);
    }

    let delayTime: number = Math.floor(Math.random() * 200 + 100);

    return of(null)
      .pipe(mergeMap(() => {
        if (request.url.match(".*/api/dictionaries")) {
          let className: string = this.getClassName(request.url);
          console.debug("DictionaryInterceptor: method:" + request.method + ", className: " + className);
          console.debug(request.body);

          if (request.url.endsWith("/api/dictionaries/")) {
            if (isDevMode()) {
              console.info("GET /api/dictionaries/");
            }
            return of(new HttpResponse<any>({status: 200, body: DICTIONARIES}));
          } else if (request.url.match(".*/entries/add")) {
            let data: any[] = this.addData(className, this.getId(className), this.getDisplay(className), request.body);
            return of(new HttpResponse<any>({status: 200, body: data}));
          } else if (request.url.match(".*/entries/update")) {
            this.updateData(className, this.getId(className), this.getDisplay(className), request.body);
            return of(new HttpResponse<any>({status: 200, body: request.body}));
          } else if (request.url.match(".*/entries/delete")) {
            this.deleteData(className, this.getId(className), request.body);
            return of(new HttpResponse<any>({status: 200, body: request.body}));
          } else if (request.url.match(".*/dropdown-entries") || request.url.match(".*/active-dropdown-entries")) {
            return of(new HttpResponse<any>({status: 200, body: this.getDropdownEntries(className)}));
          } else if (request.url.match(".*/entries") || request.url.match(".*/active-entries")) {
            return of(new HttpResponse<any>({status: 200, body: this.getEntries(className)}));
          } else if (request.url.match(".*/api/dictionaries/.*")) {
            return of(new HttpResponse<any>({status: 200, body: this.getDictionary(className)}));
          } else {
            delayTime = 0;
          }
        } else {
          delayTime = 0;
        }

        return next.handle(request)
          .pipe(
            catchError(response => {
              if (response instanceof HttpErrorResponse) {
                console.warn("Http error", response);
              }
              return throwError(response);
            }));
      }))
      .pipe(delay(delayTime));
  }

  getClassName(url: string): string {
    try {
      let regex: RegExp = new RegExp(".*\\/api\\/dictionaries\\/([.a-zA-Z]*)[\\/]?");
      return regex.exec(url)[1];
    } catch (error) {
      return undefined;
    }
  }

  getId(className: string): string {
    console.debug("getId for: " + className);

    let dictionary: any = this.coolLocalStorage.getObject("dictionary." + className);

    for (let field of dictionary.fields) {
      if (field.id === true) {
        return field.name;
      }
    }

    return undefined;
  }

  getDisplay(className: string): string {
    console.debug("getDisplay for: " + className);

    let dictionary: any = this.coolLocalStorage.getObject("dictionary." + className);

    for (let field of dictionary.fields) {
      if (field.dictionaryDisplay === true) {
        return field.name;
      }
    }

    return undefined;
  }

  getDictionary(className: string): any[] {
    return this.coolLocalStorage.getObject("dictionary." + className);
  }

  getEntries(className: string): any[] {
    return this.coolLocalStorage.getObject("entries." + className);
  }

  getDropdownEntries(className: string): any[] {
    return this.coolLocalStorage.getObject("dropdown-entries." + className);
  }

  addData(className: string, id: string, display: string, adds: any[]): any[] {
    let data: any[] = this.coolLocalStorage.getObject("entries." + className);

    let newId: number = data.length;
    for (let add of adds) {
      add[id] = ++newId;
    }

    for (let add of adds) {
      data.push(add);
    }

    this.coolLocalStorage.setObject("entries." + className, data);

    data = this.coolLocalStorage.getObject("dropdown-entries." + className);
    if (data) {

      for (let add of adds) {
        let dropdownEntry: any = {};
        dropdownEntry[id] = add[id];
        dropdownEntry[display] = add[display];
        data.push(dropdownEntry);
      }

      this.coolLocalStorage.setObject("dropdown-entries." + className, data);
    }

    return adds;
  }

  updateData(className: string, id: string, display: string, updates: any[]): void {
    let data: any[] = this.coolLocalStorage.getObject("entries." + className);

    for (let update of updates) {
      for (let i = 0; i < data.length; i++) {
        if (data[i][id] === update[id]) {
          data[i] = update;
          break;
        }
      }
    }

    this.coolLocalStorage.setObject("entries." + className, data);

    data = this.coolLocalStorage.getObject("dropdown-entries." + className);
    if (data) {
      for (let update of updates) {
        for (let i = 0; i < data.length; i++) {
          if (data[i].id === update[id]) {
            data[i].id = update[id];
            data[i].display = update[display];
            break;
          }
        }
      }

      this.coolLocalStorage.setObject("dropdown-entries." + className, data);
    }
  }

  deleteData(className: string, id: string, deletes: any[]): void {
    console.info("deleteData");
    console.info(deletes);

    let data: any[] = this.coolLocalStorage.getObject("entries." + className);

    for (let i = data.length - 1; i >= 0; i--) {
      for (let item of deletes) {
        if (data[i][id] === item[id]) {
          data.splice(i, 1);
          break;
        }
      }
    }

    this.coolLocalStorage.setObject("entries." + className, data);
  }
}
