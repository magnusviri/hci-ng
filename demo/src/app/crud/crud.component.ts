/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, ViewChild} from "@angular/core";

import {AppHeaderComponent, UlNavComponent, LiNavComponent} from "@huntsman-cancer-institute/navigation";

/**
 * @since 9.0.0
 */
@Component({
  selector: "hci-crud-demo",
  template: `
    <hci-app-header #subHeader></hci-app-header>
    <router-outlet></router-outlet>
  `
})
export class CrudDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column";

  @ViewChild("subHeader", {static: true}) subHeader: AppHeaderComponent;

  ngAfterViewInit() {
    this.subHeader.setConfig({
      id: "crud-sub-header",
      navbarClasses: "sub-header",
      children: [
        {
          type: UlNavComponent, ulClass: "nav-container",
          children: [
            {type: LiNavComponent, liClass: "nav-item", title: "CRUD Home", route: "home"},
            {type: LiNavComponent, liClass: "nav-item", title: "Form", route: "form"},
            {type: LiNavComponent, liClass: "nav-item", title: "Get", route: "get"}
          ]
        }
      ]
    });
  }

}
