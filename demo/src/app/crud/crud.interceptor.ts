import {Injectable, isDevMode} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";

import {Observable, of, throwError} from "rxjs";
import {delay, mergeMap} from "rxjs/operators";

import {HciDataDto} from "hci-ng-grid-dto";

import {DemoInterceptor} from "../demo.interceptor";

let NAMES: any = require("./data/names.json");
let ADDRESS_METADATA: any = require("./data/address.meta-data.json");
let GENDER_METADATA: any = require("./data/gender.meta-data.json");
let NAME_METADATA: any = require("./data/name.meta-data.json");
let STUDY_SUBJECT_21: any = require("./data/study-subject-21.json");

@Injectable()
export class CrudInterceptor extends DemoInterceptor implements HttpInterceptor {

  names: any[];
  uniqueId: number = 100000;

  crudDataMap: Map<string, any> = new Map<string, any>();

  constructor() {
    super();

    this.names = NAMES;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isDevMode()) {
      console.debug("CrudInterceptor.intercept: " + request.url);
    }

    return of(null)
      .pipe(mergeMap(() => {
        if (request.url.match(".*/api/crud/hci.ri.core.subject.model.Address/metadata") && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET .*/api/crud/hci.ri.core.subject.model.Address/metadata");
          }

          return of(new HttpResponse<any>({ status: 200, body: ADDRESS_METADATA }));
        } else if (request.url.match(".*/api/crud/hci.ri.core.subject.model.Gender/metadata") && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET .*/api/crud/hci.ri.core.subject.model.Gender/metadata");
          }

          return of(new HttpResponse<any>({ status: 200, body: GENDER_METADATA }));
        } else if (request.url.match(".*/api/crud/hci.ri.core.subject.model.Name/metadata") && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET .*/api/crud/hci.ri.core.subject.model.Name/metadata");
          }

          return of(new HttpResponse<any>({ status: 200, body: NAME_METADATA }));
        } else if (request.url.match(".*/api/crud/hci.ri.core.subject.model.Name") && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET .*/api/crud/hci.ri.core.subject.model.Name");
          }
          return of(new HttpResponse<any>({ status: 200, body: new HciDataDto(this.clone(this.names)) }));
        } else if (request.url.match(".*/api/crud/hci.ri.core.subject.model.Name/[0-9]+") && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET .*/api/crud/hci.ri.core.subject.model.Name/[0-9]+");
          }
          let id: number = +request.url.substring(request.url.lastIndexOf("/") + 1);
          if (isDevMode()) {
            console.debug("Id: " + id);
          }

          for (let row of NAMES) {
            for (let key in row) {
              if (row[key] == id) {
                return of(new HttpResponse<any>({ status: 200, body: row }));
              } else {
                break;
              }
            }
          }

          return throwError({status: 500, message: "idName does not exist."});
        } else if (request.url.match(".*/api/crud/hci.ri.core.subject.model.StudySubject/21") && request.method === "GET") {
          if (isDevMode()) {
            console.debug("GET .*/api/crud/hci.ri.core.subject.model.StudySubject/21");
          }

          let data: any = STUDY_SUBJECT_21;
          if (!this.crudDataMap.get(this.getClassName(request.url) + "-" + data.idStudySubject)) {
            this.crudDataMap.set(this.getClassName(request.url) + "-" + data.idStudySubject, data);
            this.crudDataMap.set("hci.ri.core.subject.model.Name-" + data.namePref.idName, data.namePref);
            this.crudDataMap.set("hci.ri.core.subject.model.Gender-" + data.genderPref.idGender, data.genderPref);
            this.crudDataMap.set("hci.ri.core.subject.model.Address-" + data.addressPref.idAddress, data.addressPref);
          }

          return of(new HttpResponse<any>({ status: 200, body: data }));
        } else if (request.url.match(".*/api/crud/hci.ri.core.subject.model.Name/[0-9]+") && request.method === "DELETE") {
          if (isDevMode()) {
            console.debug("DELETE .*/api/crud/hci.ri.core.subject.model.Name/[0-9]+");
          }
          let id: number = +request.url.substring(request.url.lastIndexOf("/") + 1);
          if (isDevMode()) {
            console.debug("Id: " + id);
          }

          for (let i = 0; i < this.names.length; i++) {
            if (id == this.names[i].idName) {
              this.names.splice(i, 1);
              break;
            }
          }
          if (isDevMode()) {
            console.debug(this.names);
          }

          return of(new HttpResponse<any>({ status: 204 }));
        } else if (request.url.match(".*/api/crud/.*/[0-9].+") && request.method === "PUT") {
          if (isDevMode()) {
            console.debug("PUT .*/api/crud/.*/[0-9].+");
            console.debug(request.body);
          }

          let className: string = this.getClassName(request.url);
          let data: any = request.body;
          let id: number = +request.url.substring(request.url.lastIndexOf("/") + 1);

          if (className === "hci.ri.core.subject.model.Gender" && data.idGenderType === 3) {
            return throwError("500 Internal Server Error");
          }

          this.crudDataMap.set(className + "-" + id, data);

          return of(new HttpResponse<any>({ status: 201, body: data }));
        } else if (request.url.match(".*/api/crud/.*") && request.method === "POST") {
          if (isDevMode()) {
            console.debug("POST .*/api/crud/.*");
            console.debug(request.body);
          }

          let className: string = this.getClassName(request.url);
          let name: string = className.substring(className.lastIndexOf(".") + 1);
          let data: any = request.body;
          let id: number = this.uniqueId++;

          data["id" + name] = id;

          this.crudDataMap.set(className + "-" + id, data);

          return of(new HttpResponse<any>({ status: 201, body: data }));
        } else {
          this.delayTime = 0;
        }

        return next.handle(request);
      }))
      .pipe(delay(Math.random() * 500 + 250));
  }

  getClassName(url: string): string {
    try {
      let regex: RegExp = new RegExp(".*\\/api\\/crud\\/([.a-zA-Z]*)[\\/]?");
      return regex.exec(url)[1];
    } catch (error) {
      return undefined;
    }
  }

  clone(array: any[]): any[] {
    let clone: any[] = [];

    for (let o of array) {
      clone.push(Object.assign({}, o));
    }

    return clone;
  }
}
