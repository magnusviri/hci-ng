/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, isDevMode} from "@angular/core";

import {Subject} from "rxjs";
import * as prism from "prismjs";

import {CrudService} from "@huntsman-cancer-institute/crud";
import {SafeHtml} from "@angular/platform-browser";
import {HciDataDto} from "hci-ng-grid-dto";

/**
 * @since 9.0.0
 */
@Component({
  selector: "hci-crud-get-demo",
  template: `
    <div class="d-flex flex-column flex-shrink-0 m-3">
      <h3 class="mb-3">Get and Get All</h3>
      <div class="d-flex mb-3">
        Using subject name as an example, this is how the Crud service can be used to fetch data.
      </div>
      <div class="d-flex flex-column flex-grow-1 mb-3">
        <hci-busy [busySubjects]="namesLoading"></hci-busy>
        <h5>Get All</h5>

        <div class="d-flex flex-row">
          <button type="button" class="btn btn-outline-primary" [ngbPopover]="configGetAll" popoverTitle="Config" placement="right">Show Config</button>
          <ng-template #configGetAll>
            <div [innerHTML]="getAllHtml"></div>
          </ng-template>
        </div>
        
        <div class="d-flex flex-row">
          <ng-container *ngFor="let col of cols">
            <div [style.width.%]="100 / cols.length" style="overflow-x: hidden; text-overflow: ellipsis; white-space: nowrap; font-weight: bold;">
              {{col}}
            </div>
          </ng-container>
        </div>
        <ng-container *ngFor="let row of names">
          <div class="d-flex flex-row">
            <ng-container *ngFor="let col of cols">
              <div [style.width.%]="100 / cols.length" style="overflow-x: hidden; text-overflow: ellipsis; white-space: nowrap;">
                {{row[col]}}
              </div>
            </ng-container>
          </div>
        </ng-container>
      </div>
      <div class="d-flex flex-column flex-grow-1 mb-3">
        <h5>Get idName of 11</h5>

        <div class="d-flex flex-row">
          <button type="button" class="btn btn-outline-primary" [ngbPopover]="configGet" popoverTitle="Config" placement="right">Show Config</button>
          <ng-template #configGet>
            <div [innerHTML]="getHtml"></div>
          </ng-template>
        </div>
        
        <div [innerHTML]="nameHtml"></div>
      </div>
    </div>
  `
})
export class CrudGetDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  cols: any[];
  names: any[];
  namesLoading: Subject<boolean>[];

  name: any;
  nameHtml: SafeHtml;

  getAllHtml: SafeHtml;
  getAll: string = `
    names: any[];
    namesLoading: Subject<boolean>[];

    this.namesLoading = [this.crudService.getAllLoading("hci.ri.core.subject.Name", undefined, undefined, (data: HciDataDto) => {
      this.names = data.data;
    })];
  `;

  getHtml: SafeHtml;
  get: string = `
    names: any[];

    this.crudService.get("hci.ri.core.subject.model.Name", 11).subscribe((name: any) => {
      this.name = name;
    }, (error) => {
      this.name = error;
    });
  `;

  constructor(private crudService: CrudService) {}

  ngOnInit() {
    this.getAllHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">";
    this.getAllHtml += prism.highlight(this.getAll, prism.languages["html"]);
    this.getAllHtml += "</code></pre>";
    this.getHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">";
    this.getHtml += prism.highlight(this.get, prism.languages["html"]);
    this.getHtml += "</code></pre>";

    this.namesLoading = [this.crudService.getAllLoading("hci.ri.core.subject.model.Name", undefined, undefined, (data: HciDataDto) => {
      this.cols = [];
      if (data.data && data.data.length > 0) {
        for (let col in data.data[0]) {
          this.cols.push(col);
        }
      }
      this.names = data.data;
    }, (error) => {
      console.error(error);
    }, () => {
      if (isDevMode()) {
        console.debug("Finalize");
      }
    })];

    this.crudService.get("hci.ri.core.subject.model.Name", 11).subscribe((name: any) => {
      this.name = name;

      this.setNameHtml(this.name);
    }, (error) => {
      this.name = error;

      this.setNameHtml(this.name);
    });
  }

  setNameHtml(name: any): void {
    this.nameHtml = "<pre class=\"language-js\"><code #code class=\"language-js\">";
    this.nameHtml += prism.highlight(JSON.stringify(name, undefined, 2), prism.languages["javascript"]);
    this.nameHtml += "</code></pre>";
  }
}
