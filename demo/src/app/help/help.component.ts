/**
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";

/**
 * @since 9.0.0
 */
@Component({
  selector: "hci-help-demo",
  template: `
    <div class="d-flex flex-column m-3">
      <ul>
        <li>
          A help with static content (defined on the client) used for both tooltip and help modal consisting of plain text.
          <hci-help [staticTooltip]="_staticTooltipValue" [staticHelp]="_staticModalHelpValue"></hci-help>
        </li>
        <li>
          A help with static content (defined on the client) used for both tooltip and help modal consisting of markup.
          <hci-help [staticTooltip]="_staticTooltipHtmlValue" [staticHelp]="_staticModalHelpHtmlValue"></hci-help>
        </li>
        <li>
          A help with dynamic tooltip and help modal values for a single id of '42' and the GNOMEX application context.
          Both the modal and tooltip present text rather than markup.
          <hci-help #hciHelp="hciHelp" id="42" [applicationContext]="hciHelp.applicationContexts.GNOMEX" [helpEditRole]="_editRole">
          </hci-help>
        </li>
        <li>
          A help with dynamic tooltip with a id of '21' and help modal with an id of '84' and the GNOMEX application context.
          Both the modal and tooltip includes markup that is similar to the help in the GNOMEX ContextSensitiveHelp. This help
          also has a custom tooltip placement of 'bottom'.
          <hci-help #hciHelp="hciHelp"
              tooltipId="21"
              tooltipPlacement="bottom"
              modalId="84"
              [applicationContext]="hciHelp.applicationContexts.GNOMEX"
              [helpEditRole]="_editRole">
          </hci-help>
        </li>
        <li>
          A help with dynamic tooltip with a id of '42' and help modal with an id of '420' and the GNOMEX application context.
          The tooltip and modal includes markup, but the title is not available from the server and so no modal dialog header
          is shown.
          <hci-help #hciHelp="hciHelp"
              tooltipId="42"
              modalId="420"
              [applicationContext]="hciHelp.applicationContexts.GNOMEX"
              [helpEditRole]="_editRole">
          </hci-help>
        </li>
        <li>
          A help with dynamic tooltip with a id of '42' and help modal with an id of '420' and the GNOMEX application context.
          The tooltip and modal includes markup, but the title is not available from the server and the default HelpComponent
          modal title value of 'Help Information' is used.
          <hci-help #hciHelp="hciHelp"
              tooltipId="42"
              modalId="420"
              [applicationContext]="hciHelp.applicationContexts.GNOMEX"
              [helpEditRole]="_editRole"
              [useDefaultTitle]="true">
          </hci-help>
        </li>
        <li>
          A help with dynamic tooltip and help that both return errors from the server.
          <hci-help id="13" [applicationContext]="hciHelp.applicationContexts.GNOMEX" [helpEditRole]="_editRole"></hci-help>
        </li>
      </ul>
    </div>
  `
})
export class HelpDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column";

  public _staticTooltipValue: string = "This is a static tooltip, so no trip to the server!";

  public _staticModalHelpValue: any = {
    title: "This is a static title",
    body: "This is a static help body."
  };

  public _staticTooltipHtmlValue: string = "This is a static tooltip with <strong>markup</strong>";

  public _staticModalHelpHtmlValue: any = {
    title: "<p class=\"demo-title\">This has a <em>marked up</em> static title!</p>",
    body: "This static body has <strong>markup</strong>."
  };

  public _editRole: string = "admin";
}
