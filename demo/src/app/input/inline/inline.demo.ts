/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, OnInit} from "@angular/core";

import * as prism from "prismjs";
import {SafeHtml} from "@angular/platform-browser";

/**
 * @since 1.2.0
 */
@Component({
  selector: "hci-input-inline-demo",
  template: `
    <div class="d-flex flex-column m-3">
      <h3>Inline</h3>
      <div class="card-group flex-column flex-shrink-0">
        <div class="card">
          <div class="card-header">
            <h4>Inline Input How To</h4>
          </div>
          <div class="card-body">
            <div class="card-text">
              These components are designed to work similar to JIRA inputs.  Under normal circumstances they appear as
              plain text.  If you hover over, you have the option of editing the field.  While editing, if you click the
              check button, type enter, or click outside, then the data saves.  If you click on the cancel button or type
              escape, then the data doesn't save.
            </div>
          </div>
        </div>
        
        <div class="card">
          <div class="card-header">
            <h4>Native Input</h4>
          </div>
          <div class="card-body d-flex col-md-12">
            <div class="col-md-4 d-flex flex-column">
              <div class="mb-3">
                Default renderer with a defined string value.
              </div>
              <hci-inline-input [(ngModel)]="inlineEditValue">
                <input #input ngModel/>
              </hci-inline-input>
              <div class="d-flex align-items-center mt-3">
                <button mat-button [matMenuTriggerFor]="inputPopup1" class="mr-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
                <mat-menu #inputPopup1="matMenu" class="prism" xPosition="after">
                  <div [innerHTML]="input1Html"></div>
                </mat-menu>
                Bound Data: {{ inlineEditValue }}
              </div>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>Native Select</h4>
          </div>
          <div class="card-body d-flex col-md-12">
            <div class="col-md-4 d-flex flex-column">
              <div class="mb-3">
                Default renderer with an undefined value.
              </div>
              <hci-inline-input [(ngModel)]="inlineSelectValue1" [noValueMessage]="'&nbsp;'">
                <hci-native-select #input ngModel [entries]="inlineSelectList"></hci-native-select>
              </hci-inline-input>
              <div class="d-flex align-items-center mt-3">
                <button mat-button [matMenuTriggerFor]="selectPopup1" class="mr-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
                <mat-menu #selectPopup1="matMenu" class="prism" xPosition="after">
                  <div [innerHTML]="select1Html"></div>
                </mat-menu>
                Bound Data: {{ inlineSelectValue1 }}
              </div>
            </div>

            <div class="col-md-4 d-flex flex-column">
              <div class="mb-3">
                Custom renderer with id 1 as the initial value.
              </div>
              <hci-inline-input [(ngModel)]="inlineSelectValue2">
                <ng-template #renderTemplate let-value="value">
                  Id: {{value}}
                </ng-template>
                <hci-native-select #input ngModel [entries]="inlineSelectList"></hci-native-select>
              </hci-inline-input>
              <div class="d-flex align-items-center mt-3">
                <button mat-button [matMenuTriggerFor]="selectPopup2" class="mr-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
                <mat-menu #selectPopup2="matMenu" class="prism" xPosition="after">
                  <div [innerHTML]="select2Html"></div>
                </mat-menu>
                Bound Data: {{ inlineSelectValue2 }}
              </div>
            </div>

            <div class="col-md-4 d-flex flex-column">
              <div class="mb-3">
                Custom renderer with id 3 as the initial value.
              </div>
              <hci-inline-input [(ngModel)]="inlineSelectValue3">
                <ng-template #renderTemplate let-value="value">
                  {{value}}
                </ng-template>
                <hci-native-select #input ngModel [entries]="inlineSelectList"></hci-native-select>
              </hci-inline-input>
              <div class="d-flex align-items-center mt-3">
                <button mat-button [matMenuTriggerFor]="selectPopup3" class="mr-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
                <mat-menu #selectPopup3="matMenu" class="prism" xPosition="after">
                  <div [innerHTML]="select3Html"></div>
                </mat-menu>
                Bound Data: {{ inlineSelectValue3 }}
              </div>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>Hci Material Multi Select</h4>
          </div>
          
          <div class="card-body d-flex col-md-12">
            <div class="col-md-4 d-flex flex-column">
              <div class="mb-3">
                Material multi select with ids 2 and 5 selected by default.  This renderTemplate just iterates
                over the array of selected values.
              </div>
              <hci-inline-input [(ngModel)]="inlineMultiSelectValues">
                <ng-template #renderTemplate let-value="value">
                  <div class="d-flex">
                    <div *ngFor="let v of value" class="pl-1 pr-1">
                      {{v}}
                    </div>
                  </div>
                </ng-template>
                <hci-md-multi-select #input
                                     [entries]="inlineMultiSelectList"
                                     ngModel>
                </hci-md-multi-select>
              </hci-inline-input>
              <div class="d-flex mt-3">
                Bound Data: 
                <div *ngFor="let value of inlineMultiSelectValues" class="ml-2 mr-2">{{ value }}</div>
              </div>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>Date (TODO to ngModel)</h4>
          </div>
          <div class="card-body d-flex col-md-12">
            <div class="col-md-4 d-flex flex-column">
              TODO: Refactor hci-date with ngModel.
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h4>Date Range (TODO to ngModel)</h4>
          </div>
          <div class="card-body">
            <!--
            <hci-inline-date-range (inputDataChange)="updateDateRangeValues($event)" [(inputStartData)]="inlineStartDateValue" [(inputEndData)]="inlineEndDateValue" [dateFormat]="'longDate'"></hci-inline-date-range>
            -->
          </div>
          <div class="card-body">
            Bound Data: <b>From</b>: {{ inlineStartDateValue }} <b>To</b>:  {{ inlineEndDateValue }}
          </div>
        </div>
        
      </div>
    </div>
  `
})
export class InputInlineComponent implements OnInit {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  public inlineEditValue: string = "Test Input String";

  public inlineSelectValue1: any;
  public inlineSelectValue2: any;
  public inlineSelectValue3: any;
  public inlineSelectList: any[] = [
    {"id": 1, "display": "Alpha"},
    {"id": 2, "display": "Beta"},
    {"id": 3, "display": "Charlie"}
  ];

  public inlineMultiSelectValues: Object[] = [];
  public inlineMultiSelectList: Object[] = [
    {"id": 1, "display": "Alpha"},
    {"id": 2, "display": "Beta"},
    {"id": 3, "display": "Charlie"},
    {"id": 4, "display": "Avacado"},
    {"id": 5, "display": "Brick"},
    {"id": 6, "display": "Candle"},
    {"id": 7, "display": "Apple"},
    {"id": 8, "display": "Bundle"},
    {"id": 9, "display": "Candy"},
    {"id": 10, "display": "Banzai"},
    {"id": 11, "display": "A Very Long Option; Very Very but Not Very Long"},
  ];

  public inlineDateValue: string = "2018-01-18T12:00-06:00";
  public inlineDateValue2: string = "2011-11-11T12:00-06:00";

  public inlineStartDateValue: string = "2009-11-11T12:00-06:00";
  public inlineEndDateValue: string = "2018-01-02T12:00-06:00";

  input1: string = `
    <hci-inline-input [(ngModel)]="inlineEditValue">
      <input #input ngModel/>
    </hci-inline-input>
  `;
  input1Html: SafeHtml;

  select1: string = `
    <hci-inline-input [(ngModel)]="inlineSelectValue1">
      <hci-native-select #input ngModel [entries]="inlineSelectList"></hci-native-select>
    </hci-inline-input>
  `;
  select1Html: SafeHtml;

  select2: string = `
    <hci-inline-input [(ngModel)]="inlineSelectValue2">
      <ng-template #renderTemplate let-value="value">
        Id: {{value}}
      </ng-template>
      <hci-native-select #input ngModel [entries]="inlineSelectList"></hci-native-select>
    </hci-inline-input>
  `;
  select2Html: SafeHtml;

  select3: string = `
    <hci-inline-input [(ngModel)]="inlineSelectValue3">
      <ng-template #renderTemplate let-value="value">
        {{value}}
      </ng-template>
      <hci-native-select #input ngModel [entries]="inlineSelectList"></hci-native-select>
    </hci-inline-input>
  `;
  select3Html: SafeHtml;

  /**
   * By default, initialize the select to option 1 and the multi-select to option 1 and 4.
   */
  ngOnInit() {
    this.inlineSelectValue1 = undefined;
    this.inlineSelectValue2 = this.inlineSelectList[0].id;
    this.inlineSelectValue3 = this.inlineSelectList[2].id;

    this.inlineMultiSelectValues = [2, 5];

    this.input1Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.input1, prism.languages["html"]) + "</code></pre>";
    this.select1Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.select1, prism.languages["html"]) + "</code></pre>";
    this.select2Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.select2, prism.languages["html"]) + "</code></pre>";
    this.select3Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.select3, prism.languages["html"]) + "</code></pre>";
  }

  public updateDateRangeValues(inputData: any[]) {
    if (inputData[0]) {
      this.inlineStartDateValue = <string>inputData[0];
    } else {
      this.inlineStartDateValue = "------------------------";
    }
    if (inputData[1]) {
      this.inlineEndDateValue = <string>inputData[1];
    } else {
      this.inlineEndDateValue = "------------------------";
    }
  }
}
