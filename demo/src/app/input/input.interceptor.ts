import {Injectable, isDevMode} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";

import {Observable, of} from "rxjs";
import {delay, mergeMap} from "rxjs/operators";

import {DemoInterceptor} from "../demo.interceptor";

let ORGANIZATION_DICTIONARY: any = require("./data/organization.dictionary.json");

@Injectable()
export class InputInterceptor extends DemoInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isDevMode()) {
      console.debug("InputInterceptor.intercept: " + request.url);
    }

    return of(null)
      .pipe(mergeMap(() => {
        if (request.url.match(".*Organization/entries")) {
          if (isDevMode()) {
            console.info("GET .*Organization/entries");
          }
          return of(new HttpResponse<any>({ status: 200, body: ORGANIZATION_DICTIONARY }));
        } else {
          this.delayTime = 0;
        }

        return next.handle(request);
      }))
      .pipe(delay(this.delayTime));
  }

}
