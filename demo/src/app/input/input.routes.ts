/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {RouterModule, Routes} from "@angular/router";

import {InputComponent} from "./input.demo";
import {InputHomeComponent} from "./home/input-home.demo";
import {InputDateComponent} from "./date/date.demo";
import {InputDropdownComponent} from "./dropdown/dropdown.demo";
import {InputInlineComponent} from "./inline/inline.demo";
import {SelectDemo} from "./select/select.demo";
import {ComboBoxDemoComponent} from "./combobox/combobox.demo";

/**
 * @since 1.0.0
 */
const ROUTES: Routes = [
  {path: "", component: InputComponent,
    children: [
      {path: "", redirectTo: "home", pathMatch: "full"},
      {path: "date",  component: InputDateComponent},
      {path: "dropdown",  component: InputDropdownComponent},
      {path: "inline",  component: InputInlineComponent},
      {path: "combobox",  component: ComboBoxDemoComponent},
      {path: "select",  component: SelectDemo},
      {path: "home", component: InputHomeComponent}
    ]
  }
];

export const INPUT_ROUTING = RouterModule.forChild(ROUTES);
