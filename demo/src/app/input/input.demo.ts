/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, OnInit, ViewChild} from "@angular/core";

import {AppHeaderComponent, UlNavComponent, LiNavComponent} from "@huntsman-cancer-institute/navigation";

/**
 * @since 1.2.0
 */
@Component({
  selector: "hci-input-demo",
  template: `
    <hci-app-header #subHeader></hci-app-header>
    <router-outlet></router-outlet>
  `
})
export class InputComponent implements OnInit {

  @HostBinding("class") classlist: string = "outlet-column";

  @ViewChild("subHeader", {static: true}) subHeader: AppHeaderComponent;

  ngOnInit() {
    this.subHeader.setConfig({
      id: "input-sub-header",
      navbarClasses: "sub-header",
      children: [
        {
          type: UlNavComponent, ulClass: "nav-container",
          children: [
            {type: LiNavComponent, liClass: "nav-item", title: "Home", route: "home"},
            {type: LiNavComponent, liClass: "nav-item", title: "Date", route: "date"},
            {type: LiNavComponent, liClass: "nav-item", title: "Dropdown", route: "dropdown"},
            {type: LiNavComponent, liClass: "nav-item", title: "Select", route: "select"},
            {type: LiNavComponent, liClass: "nav-item", title: "Inline", route: "inline"},
            {type: LiNavComponent, liClass: "nav-item", title: "ComboBox", route: "combobox"},
          ]
        }
      ]
    });
  }

}
