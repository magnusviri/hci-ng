/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";

/**
 * @since 1.2.0
 */
@Component({
  selector: "hci-input-home-demo",
  template: `
    <div class="d-flex flex-column m-3">
      <h3>Input</h3>
      <div class="mb-3">
        The @huntsman-cancer-institute/input library contains all components related to controls that read and or modify data from the backend.
        These may be based upon existing controls, but are wrapped to handle special use cases, workflows, and custom
        data structure.  Input components are broken down by function type.
      </div>
      <div class="mb-3">
        <span [routerLink]="['../dropdown']" style="display: contents; font-weight: bold; margin-right: 10px;">Dropdown -</span>
        Dropdowns are existing components but are wrapped to handle our dictionary data.  These come in two types,
        selects for a single value and chips for multi value.  In either case, a basic dropdown is used to handle finding
        the value to select.
      </div>
      <div class="mb-3">
        <span [routerLink]="['../inline']" style="display: contents; font-weight: bold; margin-right: 10px;">Inline -</span>
        Inline components are those that appear as plain text, but upon mouse hover, will show the actual control
        such as an input, select, or date picker.  Like controls in JIRA, when editing these fields, you can always
        cancel out of the edit before persisting data or you can confirm by hitting a check box, hitting enter, or moving
        on to the next control.
      </div>
    </div>
  `
})
export class InputHomeComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

}
