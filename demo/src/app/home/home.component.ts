/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, OnInit} from "@angular/core";
import {HttpClient} from "@angular/common/http";

/**
 * @since 1.0.0
 */
@Component({
  selector: "home",
  template: `
    <div class="d-flex flex-column flex-shrink-1 y-auto p-5">
      <div class="d-flex flex-wrap justify-content-around mb-5">
        <img src="assets/logo-hci.png" />
        <img src="assets/logo-risr.png" />
      </div>
      <div class="d-flex flex-column mb-5">
        <h3>RISR Angular Library</h3>
        <div class="mb-2">
          This mono repo contains all libaries that Angular based RISR applications use.  This demo, starting from this
          home page, uses the styling, header, footer and navigation libraries that form the basis of our Angular
          applications.
        </div>
        <div>
          Click on the hamburger menu to select more detailed demos of these libraries.
        </div>
      </div>

      <div class="d-flex flex-column mb-5">
        <h3>Build Dependecies (Branch {{branch}})</h3>
        <div class="d-flex flex-row flex-wrap">
          <div *ngFor="let library of libraries"
               class="flex-column chip">
            <div class="font-weight-bold">
              @huntsman-cancer-institute/{{library.name}}
            </div>
            <div>
              {{library.version}}
            </div>
          </div>
        </div>
      </div>
      
      <div class="d-flex flex-column mb-5">
        <h3>Powered By</h3>
        <div class="d-flex flex-row flex-wrap justify-content-around">
          <div class="brand">
            <a href="https://github.com/angular/angular" target=”_blank”>
              <img src="https://angular.io/assets/images/logos/angular/angular.svg" class="aspect" />
            </a>
          </div>
          <div class="brand">
            <a href="https://getbootstrap.com/docs/4.1/getting-started/introduction/" target=”_blank”>
              <img src="https://getbootstrap.com/docs/4.1/assets/img/bootstrap-stack.png" class="aspect" />
            </a>
          </div>
          <div class="brand">
            <a href="https://ng-bootstrap.github.io/#/home" target=”_blank”>
              <img src="https://ng-bootstrap.github.io/img/logo-stack.png" class="aspect" />
            </a>
          </div>
          <div class="d-flex aspect brand flex-column justify-content-center">
            <div class="d-flex justify-content-center">
              <a href="https://fontawesome.com/icons?d=gallery" target=”_blank”>
                <i class="fab fa-font-awesome fa-10x"></i>
              </a>
            </div>
          </div>
          <div class="brand">
            <div>
              <a href="https://webpack.js.org/" target=”_blank”>
                <img src="https://webpack.js.org/d19378a95ebe6b15d5ddea281138dcf4.svg" class="aspect" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
  
    .aspect {
      max-height: 200px;
      min-height: 200px;
      max-width: 200px;
      min-width: 200px;
    }
    
    .chip {
      background-color: #205081;
      color: white;
      padding: 20px;
      border-radius: 20px;
      margin: 10px;
    }
    
    .brand:hover {
      border: darkred 1px solid;
      border-radius: 20px;
      box-shadow: 4px 4px 8px grey;
    }
    
  `]
})
export class HomeComponent implements OnInit {

  @HostBinding("class") classlist: string = "outlet-column";

  branch: string;
  libraries: any = [];

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.http.get("assets/package.json").subscribe((data: any) => {
      this.branch = data.branch;

      let deps: any = data.optionalDependencies;
      for (let dep in deps) {
        this.libraries.push({
          name: dep,
          version: deps[dep]
        });
      }
    });
  }
}
