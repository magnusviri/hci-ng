import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatCardModule} from "@angular/material/card";

import {ColorPickerModule, ColorPickerService} from "ngx-color-picker";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {NavigationModule} from "@huntsman-cancer-institute/navigation";

import {IconComponent, SelectedPipe} from "./icon.component";

/**
 * @since 1.2.0
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatIconModule,
    MatCardModule,
    NgbModule,
    ColorPickerModule,
    NavigationModule,
    RouterModule.forChild([
      {path: "", component: IconComponent}
    ])
  ],
  providers: [
    ColorPickerService
  ],
  declarations: [
    IconComponent,
    SelectedPipe
  ]
})
export class IconDemoModule {}
