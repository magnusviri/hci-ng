/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {ChangeDetectorRef, Component, HostBinding, Pipe, PipeTransform} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";

import * as prism from "prismjs";

/**
 * @since 1.2.0
 */
@Component({
  selector: "hci-icon-demo",
  template: `
    <div class="d-flex flex-column flex-shrink-0 m-5">
      <nav ngbNav #nav="ngbNav" class="nav-tabs">
        <ng-container ngbNavItem>
          <a ngbNavLink>HCI Icons</a>
          <ng-template ngbNavContent>
            <div style="padding: 20px; display: inline-flex;">
              <ng-container *ngFor="let size of sizes">
                <button class="btn btn-default size" style="height: 2rem"
                        [class.selected]="size.selected"
                        (click)="updateSize(size)">{{size.display}}</button> <!--added height style-->
              </ng-container>
              <div class="input-group" style="margin-left: 20px;">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="search">
                      <i class="hci fa-core fa-lg mr-2"></i>
                    <i class="fas fa-search fa-lg"></i>
                  </span>
                </div>
                <input type="text" class="form-control" [ngModel]="searchText" (ngModelChange)="search($event)"
                       aria-label="search" aria-describedby="search"
                       style="width: 150px; height: 2rem;" /> <!--added height style-->
              </div>
              <div class="input-group" style="margin-left: 20px;">
                <div class="input-group-prepend">
                  <span class="input-group-text" style="height: 2rem"> <!--added style-->
                    Color
                  </span>
                </div>
                <input [colorPicker]="color"
                       (colorPickerChange)="onChangeColor($event)"
                       [style.background]="color"
                       [cpPositionRelativeToArrow]="true"
                       [cpCancelButton]="true"
                       [cpOKButtonClass]="'btn btn-primary btn-xs'"
                       [cpPosition]="'bottom'"
                       style="width: 40px; height: 2rem;" /> <!--added height style-->
              </div>
              <div class="input-group" style="margin-left: 20px;">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    Background Color
                  </span>
                </div>
                <input [colorPicker]="backgroundColor"
                       (colorPickerChange)="onChangeBackgroundColor($event)"
                       [style.background]="backgroundColor"
                       [cpPositionRelativeToArrow]="true"
                       [cpCancelButton]="true"
                       [cpOKButtonClass]="'btn btn-primary btn-xs'"
                       [cpPosition]="'bottom'"
                       style="width: 40px; height: 2rem;" /> <!--added height style-->
              </div>
            </div>
            <div class="card-group">

              <ng-container *ngFor="let icon of icons">
                <div class="card">
                  <div class="card-header">{{icon}}</div>
                  <div class="card-body" [style.color]="color" [style.background-color]="backgroundColor" style="white-space: normal !important;">
                    <ng-container *ngFor="let size of sizes">
                      <div class="icon mr-1" *ngIf="size.selected">
                        <i [class]="'hci fa-' + icon + ' ' + size.size"></i>
                      </div>
                    </ng-container>
                  </div>
                </div>
              </ng-container>

            </div>
          </ng-template>
        </ng-container>
        <ng-container ngbNavItem>
          <a ngbNavLink>HCI Using Font Awesome</a>
          <ng-template ngbNavContent>
            <div class="m-3 flex-column">
              <h4 class="mb-3">A Font Awesome 5 Original Icon</h4>
              <div>
                <i class="fas fa-camera-retro fa-xs"></i>
                <i class="fas fa-camera-retro fa-sm"></i>
                <i class="fas fa-camera-retro"></i>
                <i class="fas fa-camera-retro fa-lg"></i>
                <i class="fas fa-camera-retro fa-2x"></i>
                <i class="fas fa-camera-retro fa-3x"></i>
                <i class="fas fa-camera-retro fa-5x"></i>
                <i class="fas fa-camera-retro fa-7x"></i>
                <i class="fas fa-camera-retro fa-10x"></i>
              </div>
            </div>
            <div class="mb-3 flex-column">
              <h4>Our Core Icon</h4>
              <div>
                <i class="hci fa-core fa-xs"></i>
                <i class="hci fa-core fa-sm"></i>
                <i class="hci fa-core"></i>
                <i class="hci fa-core fa-lg"></i>
                <i class="hci fa-core fa-2x"></i>
                <i class="hci fa-core fa-3x"></i>
                <i class="hci fa-core fa-5x"></i>
                <i class="hci fa-core fa-7x"></i>
                <i class="hci fa-core fa-10x"></i>
              </div>
            </div>
            <div class="mb-3 flex-column">
              <h4>Spin It</h4>
              <div>
                <i class="hci fa-core fa-7x fa-spin"></i>
              </div>
            </div>
            <div class="mb-3 flex-column">
              <h4>Coloring</h4>
              <div>
                <i class="hci fa-core fa-7x" style="background: MistyRose;"></i>
                <i class="hci fa-core fa-7x" style="color: red;"></i>
                <i class="hci fa-core fa-7x" style="background: black; color: white;"></i>
              </div>
            </div>
            <div class="mb-3 flex-column">
              <h4>Transform</h4>
              <div style="margin-top: 50px;">
                <span class="fa-layers fa-fw" style="background: MistyRose">
                  <i class="fas fa-certificate fa-7x"></i>
                  <i class="hci fa-core fa-inverse fa-7x" data-fa-transform="shrink-6 rotate--30"></i>
                </span>
              </div>
            </div>
            <div style="margin: 100px;">
            </div>
          </ng-template>
        </ng-container>
        <ng-container ngbNavItem>
          <a ngbNavLink>Usage</a>
          <ng-template ngbNavContent>
            <div class="m-3 flex-column">
              <div id="icon-code">To create a
                <span style="display: inline-flex;" class="ml-1 mr-1">
                  <i class="hci fa-core fa-sm"></i>
                </span>
                icon, use the following syntax:
              </div>
              <div [innerHTML]="iconExampleHtml"></div>
              <div class="mb-3">
                This is the same as font awesome, but the icon group is now "hci".  Like font awesome, our font names
                still have to be prepended by "fa-".
              </div>
              <div>
                Core icon as a Material icon.  Tested as an avatar in a material card.
                <mat-card>
                  <mat-card-header>
                    <mat-icon mat-card-avatar fontSet="hci" fontIcon="fa-core" class="fa-2x"></mat-icon>
                    <mat-card-title>Core FA Icon as Avatar</mat-card-title>
                  </mat-card-header>
                </mat-card>
              </div>
              <div [innerHTML]="materialExampleHtml"></div>
            </div>
          </ng-template>
        </ng-container>
      </nav>
      <div [ngbNavOutlet]="nav"></div>
    </div>
  `,
  styles: [`

    .card {
      min-width: 225px;
      max-width: 250px;
    }

    .card-header {
      padding: 0.5rem 0.5rem;
      border: gray 1px solid;
      border-top-left-radius: 8px !important;
      border-top-right-radius: 8px !important;
    }

    .card-body {
      align-self: center;
    }

    .svg-inline--fas {
      vertical-align: top;
    }

    .btn {
      background-color: lightblue;
    }

    .btn.selected {
      background-color: lawngreen;
    }

    .btn.size {
      margin-right: 4px;
      border: darkgray 1px solid;
    }

    .form-control, .input-group, input, select {
      height: auto;
    }
  `]
})
export class IconComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  icons = [
    "accounting-units",
    "accounts",
    "additional-details-solid",
    "and-open-caps",
    "and-open-lower",
    "and-solid-caps",
    "and-solid-lower",
    "and-word-caps",
    "and-word-lower",
    "arrow-long-down-outline",
    "arrow-long-left-outline",
    "arrow-long-right-outline",
    "arrow-long-up-outline",
    "biopsy",
    "boolean-yesno-reg",
    "boolean-yesno-solid",
    "boolean-yesnounknown-reg",
    "boolean-yesnounknown-solid",
    "breadcrumb-cap-right-solid",
    "breadcrumb-tail-right-solid",
    "bucket",
    "caret-angle-left-hollow",
    "caret-angle-left-shadow",
    "caret-angle-right-hollow",
    "caret-angle-right-shadow",
    "caret-down-hollow",
    "caret-down-shadow",
    "caret-left-hollow",
    "caret-left-shadow",
    "caret-right-hollow",
    "caret-right-shadow",
    "caret-up-hollow",
    "caret-up-shadow",
    "ccr-logo",
    "clipboard-regular",
    "consent-hex-open",
    "consent-solid",
    "core",
    "courses",
    "create",
    "creates",
    "date-time-solid",
    "desktop-arrow-up",
    "diagnosis",
    "core-gradient",
    "dictionary-hex-open",
    "dx-regular",
    "edw-clipboard",
    "edw-hex-open",
    "event-solid",
    "file-data",
    "file-object",
    "file-view",
    "filter-bydate-down",
    "filter-bydate-up",
    "folder-analysis",
    "folder-experiment",
    "folder-project",
    "gnomex-solid",
    "grid-view-regular",
    "grid-view-solid",
    "grip-lineslong-horizontal",
    "grip-lineslong-vertical",
    "import-hex",
    "iq-hex-open",
    "lab",
    "logo-hci",
    "logo-rad",
    "logo-risr",
    "logo-risr-solid",
    "logo-rsr",
    "logo-updb",
    "member",
    "metastasis",
    "microscope-regular",
    "nlp-letters-reg",
    "or-open-caps",
    "or-open-lower",
    "or-solid-caps",
    "or-solid-lower",
    "or-word-caps",
    "or-word-lower",
    "overview-dx",
    "overview-stage",
    "overview-tx",
    "pathology-solid",
    "phi-view",
    "phi-view-ident",
    "phi-view-limited",
    "project",
    "projects",
    "purchase-card",
    "purchase-groups",
    "purchase-group-user",
    "purchasing-admin",
    "pushpin-solid",
    "radiation-regular",
    "radiology-regular",
    "recent",
    "recent-projects",
    "recurrence",
    "related-samples-hex",
    "related-samples-solid",
    "sample-aliases",
    "sample-hex-open",
    "settings",
    "share",
    "site-specific",
    "stage",
    "staging-clinical",
    "staging-details",
    "staging-dominant",
    "staging-pathologic",
    "study-hex-open",
    "sub-account",
    "sub-account-set",
    "subject-hex-open",
    "subject-solid",
    "surgery",
    "tabset-add-solid",
    "tabset-solid",
    "testtube-regular",
    "text-view-regular",
    "text-view-solid",
    "therapy-chemo",
    "therapy-hormone",
    "therapy-immuno",
    "therapy-radiation",
    "therapy-systemic",
    "trash-toss",
    "treatment",
    "time-regular",
    "tumor-registry-hex",
    "tumor-registry",
    "uhealth",
    "un-archive-regular",
    "un-archive-solid",
    "vendor",
  ];

  sizes = [
    {display: "xs", size: "fa-xs", selected: false},
    {display: "sm", size: "fa-sm", selected: false},
    {display: "md", size: "", selected: false},
    {display: "lg", size: "fa-lg", selected: false},
    {display: "2x", size: "fa-2x", selected: true},
    {display: "3x", size: "fa-3x", selected: false},
    {display: "5x", size: "fa-5x", selected: false},
    {display: "7x", size: "fa-7x", selected: false},
    {display: "10x", size: "fa-10x", selected: false},
    {display: "Spin", size: "fa-2x fa-spin", selected: false}
  ];

  color: string = "black";
  backgroundColor: string = "white";
  searchText: string = "";
  originalIcons = [];

  iconExampleHtml: SafeHtml;
  iconExample: string = `
    <i class="hci fa-core fa-sm"></i>
  `;

  materialExampleHtml: SafeHtml;
  materialExample: string = `
    <mat-card>
      <mat-card-header>
        <mat-icon mat-card-avatar fontSet="hci" fontIcon="fa-core" class="fa-2x"></mat-icon>
        <mat-card-title>Core FA Icon as Avatar</mat-card-title>
      </mat-card-header>
    </mat-card>
  `;

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.iconExampleHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.iconExample, prism.languages["html"]) + "</code></pre>";
    this.materialExampleHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.materialExample, prism.languages["html"]) + "</code></pre>";
  }

  onChangeColor(color: string) {
    this.color = color;
  }

  onChangeBackgroundColor(backgroundColor: string) {
    this.backgroundColor = backgroundColor;
  }

  search(searchText: string) {
    this.searchText = searchText;

    if (this.originalIcons.length === 0) {
      this.originalIcons = this.icons;
    }
    this.icons = this.originalIcons;

    if (this.searchText.length > 0) {
      this.icons = this.icons.filter((icon: string) => {
        return icon.indexOf(this.searchText) >= 0;
      });
    }
  }

  updateSize(size: any) {
    size.selected = !size.selected;
    this.changeDetectorRef.markForCheck();
  }

}

@Pipe({ name: "selected", pure: false })
export class SelectedPipe implements PipeTransform {
  transform(list: any[]) {
    return list.filter(o => o.selected);
  }
}
