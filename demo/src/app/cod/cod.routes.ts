/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {RouterModule, Routes} from "@angular/router";

import {Cod8834DemoComponent} from "./cod-88-34.component";
import {Cod7023DemoComponent} from "./cod-70-23.component";
import {CodDemoComponent} from "./cod.component";
import {CodHomeComponent} from "./home.component";

/**
 * @since 1.0.0
 */
const ROUTES: Routes = [
  {path: "", component: CodDemoComponent,
    children: [
      {path: "", redirectTo: "home", pathMatch: "full"},
      {path: "cod-70-23", component: Cod7023DemoComponent},
      {path: "cod-88-34", component: Cod8834DemoComponent},
      {path: "home", component: CodHomeComponent}
    ]
  }
];

export const COD_ROUTING = RouterModule.forChild(ROUTES);
