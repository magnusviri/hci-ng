/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";

import * as prism from "prismjs";

/**
 * @since 2.1.0
 */
@Component({
  selector: "hci-cfg-88-34",
  template: `
    <div class="d-flex flex-column y-auto flex-shrink-1 flex-grow-1 m-0">
      <div class="d-flex flex-column flex-shrink-0 m-3">
        <h3 class="mb-3">Configure on Demand</h3>
        <div class="d-flex mb-3">
          Here we use a sample configuration with three containers each with a variety of attributes for testing.
          This is using CCR, BMT, ACIS-001, Medical Event Transplant on 2017-09-05.
        </div>
        <div class="d-inline-block mb-3">
          <span class="font-weight-bold">Editing:</span> On the Transplant tab, click on the edit button.  Update some
          fields.  Click the + button above the grid.  Fill out some data and add the row.  When done, click Save.  This
          persists the data and the tab refreshes with the response from the service.
        </div>
        <div class="d-flex mb-3">
          <button type="button" class="btn btn-outline-primary" [ngbPopover]="configCcr" popoverTitle="Config" placement="right">Show Config</button>
          <ng-template #configCcr>
            <div [innerHTML]="ccrExampleHtml"></div>
          </ng-template>
        </div>
        <div class="d-flex flex-grow-1 flex-column mb-0">
          <h4 class="mb-0">Flex (Guided by Absolute Positioning)</h4>
        </div>
      </div>
      <hci-attribute-configuration id="attr-cfg-88-34"
                                   [idAttributeValueSet]="536753"
                                   [accordionNav]="accordionNav"
                                   class="flex-column flex-shrink-0 flex-grow-1">
      </hci-attribute-configuration>
    </div>
    <hci-accordion-nav #accordionNav
                       accordionId="attr-cfg-88-34"
                       class="mt-auto mb-0">
    </hci-accordion-nav>
  `
})
export class Cod8834DemoComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  ccrExampleHtml: SafeHtml;
  ccrExample: string = `
    <hci-attribute-configuration id="attr-cfg-88-34"
                                   [idFilter1]="34"
                                   [idAttributeValueSet]="536753"
                                   [accordionNav]="accordionNav"
                                   class="flex-column flex-shrink-0 flex-grow-1">
    </hci-attribute-configuration>
    <hci-accordion-nav #accordionNav
                       accordionId="attr-cfg-88-34"
                       class="mt-auto mb-0">
    </hci-accordion-nav>
  `;

  ngOnInit() {
    this.ccrExampleHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">";
    this.ccrExampleHtml += prism.highlight(this.ccrExample, prism.languages["html"]);
    this.ccrExampleHtml += "</code></pre>";
  }

}
