import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {CodModule, AttributeService} from "@huntsman-cancer-institute/cod";
import {NavigationModule} from "@huntsman-cancer-institute/navigation";
import {MiscModule} from "@huntsman-cancer-institute/misc";

import {CodDemoComponent} from "./cod.component";
import {COD_ROUTING} from "./cod.routes";
import {Cod7023DemoComponent} from "./cod-70-23.component";
import {Cod8834DemoComponent} from "./cod-88-34.component";
import {CodHomeComponent} from "./home.component";

/**
 * @since 2.1.0
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    NavigationModule,
    CodModule.forRoot(),
    MiscModule,
    COD_ROUTING
  ],
  declarations: [
    CodDemoComponent,
    CodHomeComponent,
    Cod7023DemoComponent,
    Cod8834DemoComponent
  ]
})
export class CodDemoModule {}
