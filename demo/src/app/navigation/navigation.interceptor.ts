import {Injectable, isDevMode} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";

import {Observable, of, throwError} from "rxjs";
import {catchError, delay, mergeMap} from "rxjs/operators";
import {CoolLocalStorage} from "@angular-cool/storage";

import {HciDataDto, HciFilterDto, HciGridDto, HciSortDto} from "hci-ng-grid-dto";

import {DemoInterceptor} from "../demo.interceptor";

/**
 *
 */
@Injectable()
export class NavigationInterceptor extends DemoInterceptor implements HttpInterceptor {

  origData: Object[] = [];
  origBigData: Object[] = [];
  origRandomData: Object[] = [];

  constructor(private coolLocalStorage: CoolLocalStorage) {
    super();

    let firstNames: string[] = ["Alice", "Bob", "Charlie", "Doug", "Elane", "Frank", "Jane", "Mary", "John", "Melissa", "Rick", "Sally", "Sam", "Tim", "Uma", "Stacy", "Ian", "Jim", "Abe", "Bart", "Lisa", "Mike", "Chris", "Kristi", "Nathan", "Jason", "Maria", "Darcie"];
    let middleNames: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W"];
    let lastNames: string[] = ["Black", "Brown", "Crowe", "Gunn", "Kanth", "Jones", "Smith", "White", "Lewis", "Marx", "Greene", "Garcia"];

    for (let i = 0; i < lastNames.length; i++) {
      for (let j = i; j < 9; j++) {
        this.origData.push({id: this.origData.length + 1, lastName: lastNames[i], firstName: firstNames[j]});
      }
    }
    for (let i = 0; i < lastNames.length; i++) {
      for (let j = i; j < firstNames.length; j++) {
        this.origBigData.push({id: this.origBigData.length + 1, lastName: lastNames[i], firstName: firstNames[j], middleName: "A"});
      }
    }
    for (let i = 0; i < lastNames.length; i++) {
      for (let j = i; j < firstNames.length - Math.floor(Math.random() * 5); j++) {
        for (let k = 0; k < Math.floor(Math.random() * 10 + 5); k++) {
          this.origRandomData.push({id: this.origRandomData.length + 1, lastName: lastNames[i], firstName: firstNames[j], middleName: middleNames[Math.floor(Math.random() * middleNames.length)]});
        }
      }
    }
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isDevMode()) {
      console.debug("NavigationInterceptor.intercept: " + request.url);
    }

    let delayTime: number = Math.floor(Math.random() * 200 + 100);

    return of(null)
      .pipe(mergeMap(() => {
        if (request.url.match(".*/api/search-sidebar-data")) {
          return of(new HttpResponse<any>({status: 200, body: this.getData(this.origData, <HciGridDto>request.body)}));
        } else if (request.url.match(".*/api/search-sidebar-big-data")) {
          return of(new HttpResponse<any>({status: 200, body: this.getData(this.origBigData, <HciGridDto>request.body)}));
        } else if (request.url.match(".*/api/search-sidebar-random-data")) {
          return of(new HttpResponse<any>({status: 200, body: this.getData(this.origRandomData, <HciGridDto>request.body)}));
        } else {
          delayTime = 0;
        }

        return next.handle(request)
          .pipe(
            catchError(response => {
              if (response instanceof HttpErrorResponse) {
                console.warn("Http error", response);
              }
              return throwError(response);
            }));
      }))
      .pipe(delay(delayTime));
  }

  getData(origData: Object[], gridDto: HciGridDto): HciDataDto {
    if (isDevMode()) {
      console.debug("NavigationInterceptor.getData");
      console.debug(gridDto);
    }

    try {
      let preData: any[] = [];
      for (let row of origData) {
        preData.push([row, 1]);
      }

      preData = this.filter(preData, gridDto.getFilters());
      this.sort(preData, gridDto.getSorts());

      if (gridDto.getGrouping().getFields().length > 0 && gridDto.getGrouping().getGroupQuery()) {
        let grpData = this.group(preData, gridDto.getGrouping().getFields());
        preData = [];
        for (let grp of Object.keys(grpData)) {
          preData.push(grpData[grp]);
        }
      }

      gridDto.getPaging().setDataSize(preData.length);

      let data: any[] = [];

      if (gridDto.paging.getPageSize() > 0) {
        for (var i = gridDto.paging.getPage() * gridDto.paging.getPageSize(); i < Math.min(preData.length, (gridDto.paging.getPage() + 1) * gridDto.paging.getPageSize()); i++) {
          data.push(preData[i]);
        }
      } else {
        data = preData;
      }

      let rows: Object[] = [];
      let dataCounts: number[];
      if (gridDto.getGrouping().getGroupQuery() !== undefined && gridDto.getGrouping().getGroupQuery()) {
        dataCounts = [];
      }

      for (let row of data) {
        rows.push(row[0]);
        if (dataCounts) {
          dataCounts.push(row[1]);
        }
      }

      console.debug(rows);
      console.debug(dataCounts);

      return new HciDataDto(rows, gridDto, dataCounts);
    } catch (error) {
      console.error(error);
    }
  }

  group(preData: any[], groupFields: string[]): Object {
    let data: Object = {};

    for (let row of preData) {
      let key: string;
      for (let groupField of groupFields) {
        key = (key) ? key + ":" + row[0][groupField] : row[0][groupField];
      }
      if (data[key]) {
        data[key][1] = data[key][1] + 1;
      } else {
        data[key] = row;
      }
    }

    return data;
  }

  filter(preData: any[], filters: HciFilterDto[]): Object[] {
    let data: any[] = [];

    for (let row of preData) {
      let include: boolean = true;

      for (let filter of filters) {
        if (!filter.valid || !filter.value) {
          continue;
        } else if (filter.getField().indexOf(" or ") !== -1) {
          let innerInclude: boolean = false;

          for (let field of filter.getField().split(" or ")) {
            if (row[0][field] && row[0][field].toLowerCase().indexOf(filter.getValue().toLowerCase()) !== -1) {
              innerInclude = true;
            }
          }

          if (!innerInclude) {
            include = false;
          }
        } else if (!row[0][filter.field]) {
          include = false;
          break;
        } else if (filter.operator === "E" && row[0][filter.field].toLowerCase() !== filter.value.toLowerCase()) {
          include = false;
          break;
        } else if (filter.operator === "L" && row[0][filter.field].toLowerCase().indexOf(filter.value.toLowerCase()) === -1) {
          include = false;
          break;
        }
      }

      if (include) {
        data.push(row);
      }
    }

    return data;
  }

  sort(preData: any[], sorts: HciSortDto[]): void {
    if (!sorts || sorts.length === 0) {
      return;
    }

    preData.sort((a: any[], b: any[]) => {
      let v: number = 0;

      for (let sort of sorts) {
        if (sort.asc) {
          if (a[0][sort.field] < b[0][sort.field]) {
            v = -1;
            break;
          } else if (a[0][sort.field] > b[0][sort.field]) {
            v = 1;
            break;
          }
        } else {
          if (a[0][sort.field] > b[0][sort.field]) {
            v = -1;
            break;
          } else if (a[0][sort.field] < b[0][sort.field]) {
            v = 1;
            break;
          }
        }
      }

      return v;
    });
  }
}
