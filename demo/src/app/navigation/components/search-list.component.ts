import {AfterViewInit, Component, HostBinding} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";

import {BehaviorSubject, of} from "rxjs";
import {delay} from "rxjs/operators";
import * as prism from "prismjs";

@Component({
  selector: "hci-navigation-components-search-list",
  template: `
    <div class="outlet-column y-auto m-3">
      <h3 class="mb-4">Search List Demo</h3>
      <div class="d-flex flex-shrink-0 mb-4">
        <div class="d-flex flex-column">
          <h4>Search List in Dropdown</h4>
          <div ngbDropdown class="dropdown" #searchDropdown1="ngbDropdown">
            <button id="dropdown" class="btn btn-primary dropdown-toggle" ngbDropdownToggle>Search List</button>
            <div aria-labelledby="dropdown" ngbDropdownMenu class="dropdown-menu">
              <hci-search-list-controller #controller1="searchListControllerComponent"
                                          [showTitle]="false"
                                          [filterByAllowedFields]="['firstName', 'lastName']"
                                          [sortByAllowedFields]="['firstName']"></hci-search-list-controller>
              <hci-search-list navId="search-dropdown1"
                               [controller]="controller1"
                               [dropdown]="searchDropdown1"
                               [data]="data"
                               [itemTemplate]="rowTemplate"
                               (rowClick)="rowEvent = $event"
                               (dataChange)="dataEvent = $event"></hci-search-list>
            </div>
          </div>
          <button mat-button [matMenuTriggerFor]="searchConfig1" class="mt-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
          <mat-menu #searchConfig1="matMenu" class="prism" xPosition="after" style="min-width: 500px;">
            <div [innerHTML]="config1Html"></div>
          </mat-menu>
        </div>

        <div class="d-flex flex-column ml-5">
          <h4>Search List Subject in Dropdown</h4>
          <div ngbDropdown class="dropdown" #searchDropdown2="ngbDropdown">
            <button id="dropdown" class="btn btn-primary dropdown-toggle" ngbDropdownToggle>Search List</button>
            <div aria-labelledby="dropdown" ngbDropdownMenu class="dropdown-menu">
              <hci-search-list navId="search-dropdown2"
                               [dataSubject]="dataSubject"
                               [loadingSubjects]="loadingSubjects"
                               [dropdown]="searchDropdown2"
                               [itemTemplate]="rowTemplate"
                               (rowClick)="rowEvent = $event"
                               (dataChange)="dataEvent = $event"></hci-search-list>
            </div>
          </div>
          <button mat-button [matMenuTriggerFor]="searchConfig2" class="mt-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
          <mat-menu #searchConfig2="matMenu" class="prism" xPosition="after" style="min-width: 500px;">
            <div [innerHTML]="config2Html"></div>
          </mat-menu>
        </div>

        <div class="d-flex flex-column ml-5">
          <h4>Search List Subject with options in Dropdown</h4>
          <div ngbDropdown class="dropdown" #searchDropdown2="ngbDropdown">
            <button id="dropdown" class="btn btn-primary dropdown-toggle" ngbDropdownToggle>Search List</button>
            <div aria-labelledby="dropdown" ngbDropdownMenu class="dropdown-menu">
            <hci-search-list-controller #controller1="searchListControllerComponent"
                                          [showQuickSearchOptions]="true"
                                          [searchTypeArray]="['firstName', 'lastName']"
                                          [sortByAllowedFields]="['firstName']"></hci-search-list-controller>
              <hci-search-list navId="search-dropdown-with-options"
                               [dataSubject]="dataSubject"
                               [loadingSubjects]="loadingSubjects"
                               [dropdown]="searchDropdown2"
                               [itemTemplate]="rowTemplate"
                               (rowClick)="rowEvent = $event"
                               (dataChange)="dataEvent = $event"></hci-search-list>
            </div>
          </div>
          <button mat-button [matMenuTriggerFor]="searchConfig2" class="mt-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
          <mat-menu #searchConfig2="matMenu" class="prism" xPosition="after" style="min-width: 500px;">
            <div [innerHTML]="config4Html"></div>
          </mat-menu>
        </div>

        <div class="d-flex flex-column ml-5">
          <h4>Grouped Search List Subject in Dropdown</h4>
          <div ngbDropdown class="dropdown" #searchDropdown3="ngbDropdown">
            <button id="dropdown" class="btn btn-primary dropdown-toggle" ngbDropdownToggle>Search List</button>
            <div aria-labelledby="dropdown" ngbDropdownMenu class="dropdown-menu">
              <hci-search-list navId="search-dropdown2"
                               [dataSubject]="dataSubject"
                               [loadingSubjects]="loadingSubjects"
                               [dropdown]="searchDropdown3"
                               [itemTemplate]="rowTemplate"
                               [inlineExpand]="true"
                               [groupingFields]="['lastName']"
                               (groupClick)="groupEvent = $event"
                               (rowClick)="rowEvent = $event"
                               (dataChange)="dataEvent = $event"></hci-search-list>
            </div>
          </div>
          <button mat-button [matMenuTriggerFor]="searchConfig3" class="mt-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
          <mat-menu #searchConfig3="matMenu" class="prism" xPosition="after" style="min-width: 500px;">
            <div [innerHTML]="config3Html"></div>
          </mat-menu>
        </div>
      </div>

      <div>
        Selected Row: {{selectedRow?.firstName}}
      </div>
      <div>
        Group Event: {{groupEvent | json}}
      </div>
      <div>
        Row Event: {{rowEvent | json}}
      </div>
      <div>
        Data Event: {{dataEvent | json}}
      </div>
    </div>
    
    <ng-template #rowTemplate let-row="row">
      <div class="pl-3" (click)="selectedRow = row">
        {{row.firstName}} {{row.lastName}}
      </div>
    </ng-template>
  `
})
export class ComponentsSearchListDemo implements AfterViewInit {

  @HostBinding("class") classlist: string = "outlet-row";

  groupEvent: any;
  rowEvent: any;
  dataEvent: any;
  selectedRow: any;

  data: any[] = [
    {id: 12, firstName: "Mary", lastName: "Jones"},
    {id: 1, firstName: "Alice", lastName: "Jones"},
    {id: 2, firstName: "Bob", lastName: "Jones"},
    {id: 3, firstName: "Charlie", lastName: "Jones"},
    {id: 4, firstName: "Doug", lastName: "Jones"},
    {id: 5, firstName: "Ellen", lastName: "Jones"},
    {id: 6, firstName: "Frank", lastName: "Smith"},
    {id: 7, firstName: "Geri", lastName: "Smith"},
    {id: 8, firstName: "Harry", lastName: "Smith"},
    {id: 9, firstName: "Ian", lastName: "Smith"},
    {id: 10, firstName: "Jane", lastName: "Smith"},
    {id: 11, firstName: "Liz", lastName: "Smith"}
  ];

  loadingSubjects: BehaviorSubject<boolean>[] = [new BehaviorSubject<boolean>(false)];
  dataSubject: BehaviorSubject<Object[]> = new BehaviorSubject<Object[]>(undefined);

  config1Html: SafeHtml;
  config1String: string = `
    <div ngbDropdown class="dropdown" #searchDropdown1="ngbDropdown">
            <button id="dropdown" class="btn btn-primary dropdown-toggle" ngbDropdownToggle>Search List</button>
            <div aria-labelledby="dropdown" ngbDropdownMenu class="dropdown-menu">
              <hci-search-list-controller #controller1="searchListControllerComponent"
                                          [filterByAllowedFields]="['firstName']"
                                          [sortByAllowedFields]="['firstName']"></hci-search-list-controller>
              <hci-search-list navId="search-dropdown1"
                               [controller]="controller1"
                               [data]="data"
                               [itemTemplate]="rowTemplate"></hci-search-list>
            </div>
          </div>
  `;

  config2Html: SafeHtml;
  config2String: string = `
    <div ngbDropdown class="dropdown" #searchDropdown2="ngbDropdown">
            <button id="dropdown" class="btn btn-primary dropdown-toggle" ngbDropdownToggle>Search List</button>
            <div aria-labelledby="dropdown" ngbDropdownMenu class="dropdown-menu">
              <hci-search-list navId="search-dropdown2"
                               [dataSubject]="dataSubject"
                               [loadingSubjects]="loadingSubjects"
                               [itemTemplate]="rowTemplate"
                               [sortByAllowedFields]="['firstName']"
                               (dataChange)="dataEvent = $event"></hci-search-list>
            </div>
          </div>
  `;

  config3Html: SafeHtml;
  config3String: string = `
    <div aria-labelledby="dropdown" ngbDropdownMenu class="dropdown-menu">
              <hci-search-list navId="search-dropdown2"
                               [dataSubject]="dataSubject"
                               [loadingSubjects]="loadingSubjects"
                               [itemTemplate]="rowTemplate"
                               [inlineExpand]="true"
                               [groupingFields]="['lastName']"
                               [sortByAllowedFields]="['firstName']"
                               (dataChange)="dataEvent = $event"></hci-search-list>
            </div>
  `;

  config4Html: SafeHtml;
  config4String: string = `
    <div ngbDropdown class="dropdown" #searchDropdown1="ngbDropdown">
            <button id="dropdown" class="btn btn-primary dropdown-toggle" ngbDropdownToggle>Search List</button>
            <div aria-labelledby="dropdown" ngbDropdownMenu class="dropdown-menu">
              <hci-search-list-controller #controller1="searchListControllerComponent"
                                          [showQuickSearchOptions]: "true",
                                          [searchTypeArray]: "['MRN', 'FIRST NAME']",
                                          [disableLocalFiltering]: "true"
                                          </hci-search-list-controller>
              <hci-search-list navId="search-dropdown1"
                               [controller]="controller1"
                               [data]="data"
                               [itemTemplate]="rowTemplate"></hci-search-list>
            </div>
          </div>
  `;

  ngOnInit(): void {
    this.config1Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.config1String, prism.languages["html"]) + "</code></pre>";
    this.config2Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.config2String, prism.languages["html"]) + "</code></pre>";
    this.config3Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.config3String, prism.languages["html"]) + "</code></pre>";
    this.config4Html = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.config4String, prism.languages["html"]) + "</code></pre>";
  }

  ngAfterViewInit() {
    this.loadingSubjects[0].next(true);
    of(undefined).pipe(delay(2000)).subscribe(() => {
      this.dataSubject.next(this.data);
      this.loadingSubjects[0].next(false);
    });
  }
}
