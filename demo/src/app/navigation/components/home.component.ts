/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";

import {LiNavComponent, NavComponent, NavigationGlobalService, AppHeaderComponent} from "@huntsman-cancer-institute/navigation";

import {NavigationDemoService} from "../navigation.service";

/**
 * Demonstrate components in the navigation module.
 *
 * @since 11.0.0
 */
@Component({
  selector: "hci-navigation-components-home",
  template: `
    <h3 class="mb-4">Navigation Components Demo</h3>
    <mat-card [routerLink]="['..', 'search-list']">
      <mat-card-title>
        Search List
      </mat-card-title>
      <mat-card-content>
        A search list is likely to be used in a sidebar or dropdown.  It displays a list of rows or groups of rows based upon
        an array of data or an external call using the grid DTO.
      </mat-card-content>
    </mat-card>
  `
})
export class ComponentsHomeDemo {

  @HostBinding("class") classlist: string = "outlet-column m-3 y-auto";

}
