/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {RouterModule, Routes} from "@angular/router";

import {NavigationComponent} from "./navigation.component";
import {BasicSidebarComponent} from "./basic-sidebar/basic-sidebar.component";
import {NavigationHomeComponent} from "./home/navigation-home.component";
import {SearchSidebarDataComponent} from "./search-sidebar/search-sidebar-data.component";
import {SearchSidebarDataCallComponent} from "./search-sidebar/search-sidebar-data-call.component";
import {SearchSidebarDataCallGroupingComponent} from "./search-sidebar/search-sidebar-data-call-grouping.component";
import {SelectedNameComponent} from "./search-sidebar/selected-name.component";
import {ComponentsDemo} from "./components/components.component";
import {ComponentsSearchListDemo} from "./components/search-list.component";
import {ComponentsHomeDemo} from "./components/home.component";

/**
 * @since 1.0.0
 */
const ROUTES: Routes = [
  {path: "", component: NavigationComponent,
    children: [
      {path: "", redirectTo: "home", pathMatch: "full"},
      {path: "basic-sidebar",  component: BasicSidebarComponent},
      {path: "components",  component: ComponentsDemo, children: [
        {path: "", redirectTo: "home", pathMatch: "full"},
        {path: "home", component: ComponentsHomeDemo},
        {path: "search-list", component: ComponentsSearchListDemo}
      ]},
      {path: "search-sidebar-data",  component: SearchSidebarDataComponent},
      {path: "search-sidebar-data-call",  component: SearchSidebarDataCallComponent},
      {path: "search-sidebar-data-call-grouping",  component: SearchSidebarDataCallGroupingComponent, children: [
          {path: "selected/:id/:last/:first/:middle", component: SelectedNameComponent},
        ]
      },
      {path: "home", component: NavigationHomeComponent}
    ]
  }
];

export const NAVIGATION_ROUTING = RouterModule.forChild(ROUTES);
