/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding} from "@angular/core";

import {LiNavComponent, NavComponent, NavigationGlobalService, AppHeaderComponent} from "@huntsman-cancer-institute/navigation";

import {NavigationDemoService} from "../navigation.service";

/**
 * Demonstrate aspects of navigation such as sidebar, sub-header, custom configuration, and dynamic control.
 *
 * @since 1.2.0
 */
@Component({
  selector: "hci-navigation-home-demo",
  template: `
    <div class="d-flex flex-column m-3">
      <h3 class="mb-4">Navigation</h3>
      <div class="mb-4">
        <h4>Toggle</h4>
        <div class="mb-2">
          The header component has a hidden property that can be used to hide or show the header.  This toggle button
          updates the header configuration by negating its current hidden value.
        </div>
        <div class="d-flex">
          <button class="btn btn-primary mr-2" (click)="toggleSubHeader()">Toggle Sub-header</button>
        </div>
      </div>
      <div class="mb-4">
        <h4>Roles</h4>
        <div class="mb-2">
          The sub header configuration contains components with different roles.  If a role is not specified, the component
          is always shown, if NOT_AUTHC, it is only shown if there are no roles, if AUTHC, shown if there are any roles,
          and if a specific role is specified, the component is only shown if that specific role exists.  Below we have
          components that use AUTHC, NOT_AUTHC, USER and ADMIN.
        </div>
        <div class="d-flex">
          <button class="btn btn-primary mr-2" (click)="logout()">Logout</button>
          <button class="btn btn-primary mr-2" (click)="login('USER')">Login User</button>
          <button class="btn btn-primary mr-2" (click)="login('ADMIN')">Login Admin</button>
        </div>
      </div>
      <div class="mb-4">
        <h4>Dynamic Configuration</h4>
        <div class="mb-2">
          The Options menu has several items.  These items are assigned functions that are triggered when clicked.  The
          function in this case changes the title of the parent to the title of the clicked component.  This is an example
          of programmatically altering the components.
        </div>
        <div class="mb-2">
          New components can be added or inserted into other components.  Click below to see.  We get the "nav-sub-header"
          component, then get the "options" dropdown, then insert a new Option before the "reset" component.
        </div>
        <div class="d-flex">
          <button class="btn btn-primary mr-2" (click)="addOption()">Add Option to the Options Menu</button>
          <button class="btn btn-primary mr-2" (click)="removeOptions()">Delete Added Options</button>
        </div>
      </div>
    </div>
  `
})
export class NavigationHomeComponent {

  @HostBinding("class") classlist: string = "outlet-column y-auto";

  nOptions: number = 3;

  constructor(private navigationDemoService: NavigationDemoService, private navigationGlobalService: NavigationGlobalService) {}

  addOption() {
    this.nOptions++;

    this.navigationGlobalService
      .getNavigationComponent("nav-sub-header")
      .getComponent("options")
      .insertBefore(
        "reset",
        {
          type: LiNavComponent,
          id: "option-" + this.nOptions,
          title: "Option " + this.nOptions,
          aClass: "dropdown-item",
          liClick: (component: NavComponent) => {
            component.getParent().setTitle(component.title);
          }
        });
  }

  removeOptions() {
    for (var i = this.nOptions; i > 3; i--) {
      this.navigationGlobalService
        .getNavigationComponent("nav-sub-header")
        .getComponent("options")
        .removeById("option-" + i);
    }

    this.nOptions = 3;
  }

  toggleSubHeader() {
    let subHeader: AppHeaderComponent = <AppHeaderComponent>this.navigationGlobalService.getNavigationComponent("nav-sub-header");
    subHeader.updateConfig({hidden: !subHeader.hidden});
  }

  logout() {
    this.navigationDemoService.logout();
  }

  login(role: string) {
    this.navigationDemoService.login(role);
  }
}
