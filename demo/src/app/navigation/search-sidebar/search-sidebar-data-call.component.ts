import {ChangeDetectorRef, Component, HostBinding, TemplateRef, ViewChild} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";
import {HttpClient} from "@angular/common/http";

import * as prism from "prismjs";

import {RowGroup, SidebarComponent, SearchListComponent} from "@huntsman-cancer-institute/navigation";
import {HciDataDto, HciGridDto} from "hci-ng-grid-dto";

@Component({
  selector: "hci-search-sidebar",
  template: `
    <hci-sidebar></hci-sidebar>
    <div class="outlet-column y-auto">
      <div class="flex-column m-3">
        <h3>Search Sidebar Demo</h3>
        <div class="flex-shrink-0 flex-column">
          <div class="mb-3">
            The search list component loaded in the sidebar uses templates to determine the layout of a group header and
            each item.
          </div>
          <div class="mb-3">
            <button mat-button [matMenuTriggerFor]="searchConfig1" class="mt-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
            <mat-menu #searchConfig1="matMenu" class="prism" xPosition="after" style="min-width: 500px;">
              <div [innerHTML]="config1Html"></div>
            </mat-menu>
          </div>
          <div class="mb-3">
            <h4>Item Template</h4>
            <div [innerHTML]="itemHtml" class="code"></div>
          </div>
        </div>
      </div>
    </div>
    
    <ng-template #demoItemTemplate let-row="row">
      <div class="search-item pl-4" style="font-size: 1rem;">
        <div>{{row.firstName}}, {{row.lastName}}</div>
      </div>
    </ng-template>
  `
})
export class SearchSidebarDataCallComponent {

  @HostBinding("class") classList: string = "outlet-row";

  overflow: boolean = false;

  itemHtml: SafeHtml;
  itemExample: string = `
    <ng-template #demoItemTemplate let-row="row">
      <div class="search-item pl-4" style="font-size: 1rem;">
        <div>{{row.firstName}}, {{row.lastName}}</div>
      </div>
    </ng-template>
  `;

  config1Html: SafeHtml;
  config1String: string = `
    @ViewChild("demoItemTemplate", { read: TemplateRef, static: true }) private demoItemTemplate: any;
  
    this.sidebar.setConfig({
      sizebarSize: 260,
      children: [
        {
          type: SearchListComponent,
          itemTemplate: this.demoItemTemplate,
          id: "search-list",
          title: "Demographics",
          dataCall: (gridDto: HciGridDto) => {
            return this.http.post<HciDataDto>("/api/search-sidebar-data", gridDto);
          },
          pageSize: 5,
          rowId: "id",
          onRowClick: (event: MouseEvent, cmpt: SearchListComponent, rowGroup: RowGroup, row: any) => {
            cmpt.defaultRowClick(event, cmpt, rowGroup, row);
            cmpt.deselectAllRows();
            cmpt.activateRowById(row.id);
          }
        }
      ]
    });
  `;

  @ViewChild(SidebarComponent, {static: true}) private sidebar: SidebarComponent;

  @ViewChild("demoItemTemplate", { read: TemplateRef, static: true }) private demoItemTemplate: any;

  constructor(private changeDetectorRef: ChangeDetectorRef,
              private http: HttpClient) {}

  ngAfterViewInit() {
    this.sidebar.setConfig({
      sizebarSize: 260,
      children: [
        {
          type: SearchListComponent,
          itemTemplate: this.demoItemTemplate,
          id: "search-list",
          title: "Demographics",
          dataCall: (gridDto: HciGridDto) => {
            return this.http.post<HciDataDto>("/api/search-sidebar-data", gridDto);
          },
          pageSize: 5,
          rowId: "id",
          onRowClick: (event: MouseEvent, cmpt: SearchListComponent, rowGroup: RowGroup, row: any) => {
            cmpt.defaultRowClick(event, cmpt, rowGroup, row);
            cmpt.deselectAllRows();
            cmpt.activateRowById(row.id);
          }
        }
      ]
    });

    this.itemHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.itemExample, prism.languages["html"]) + "</code></pre>";
    this.config1Html = "<pre class=\"language-js\"><code #code class=\"language-js\">" + prism.highlight(this.config1String, prism.languages["js"]) + "</code></pre>";
    this.changeDetectorRef.detectChanges();
  }
}
