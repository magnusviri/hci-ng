import {ChangeDetectorRef, Component, EventEmitter, HostBinding, TemplateRef, ViewChild} from "@angular/core";
import {SafeHtml} from "@angular/platform-browser";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";

import * as prism from "prismjs";

import {SidebarComponent} from "@huntsman-cancer-institute/navigation";

import {HciDataDto, HciGridDto} from "hci-ng-grid-dto";

@Component({
  selector: "hci-search-sidebar-with-options",
  template: `
    <hci-sidebar></hci-sidebar>
    <div class="outlet-column y-auto">
      <div class="flex-column m-3">
        <h3>Search Sidebar Demo</h3>
        <div class="flex-shrink-0 flex-column">
          <div class="mb-3">
            The search list component loaded in the sidebar uses templates to determine the layout of a group header and
            each item.
          </div>
          <router-outlet></router-outlet>
          <div class="mb-3">
            <button mat-button [matMenuTriggerFor]="searchConfig1" class="mt-3 btn btn-outline-primary" style="max-width: 125px;">Show Config</button>
            <mat-menu #searchConfig1="matMenu" class="prism" xPosition="after" style="min-width: 500px;">
              <div [innerHTML]="config1Html"></div>
            </mat-menu>
          </div>
          <div class="mb-3">
            <h4>Item Template</h4>
            <div [innerHTML]="itemHtml" class="code"></div>
          </div>
        </div>
      </div>
    </div>
    
    <ng-template #demoItemTemplate let-row="row">
      {{row.firstName}} {{row.middleName}}, {{row.lastName}}
    </ng-template>
  `
})
export class SearchSidebarWithSearchOptions {

  @HostBinding("class") classList: string = "outlet-row";

  searchListChange: EventEmitter<string> = new EventEmitter<string>();

  overflow: boolean = false;

  itemHtml: SafeHtml;
  itemExample: string = `
    <ng-template #demoItemTemplate let-row="row">
      {{row.firstName}} {{row.middleName}}, {{row.lastName}}
    </ng-template>
  `;

  config1Html: SafeHtml;
  config1String: string = `
    @ViewChild("demoItemTemplate", { read: TemplateRef, static: true }) demoItemTemplate: any;

    this.sidebar.setConfig({
      sizebarSize: 260,
      children: [
        {
          type: "SearchListControllerComponent",
          title: "Demographics",
          fields: [
            {field: "firstName", display: "First Name"},
            {field: "middleName", display: "Middle Name"},
            {field: "lastName", display: "Last Name"}
          ],
          showQuickSearchOptions: true,
          searchTypeArray: ["MRN", "FIRST NAME", "LAST NAME", "PERSON ID" , "DOB"],
          selectedSearchType: this.selectedSearchType,
          disableLocalFiltering: true

        },
        {
          type: "SearchListComponent",
          itemTemplate: this.demoItemTemplate,
          id: "search-list-small",
          title: "Demo Small",
          inlineExpand: true,
          dataCall: (gridDto: HciGridDto) => {
            return this.http.post<HciDataDto>("/api/search-sidebar-big-data", gridDto);
          },
          route: (rowGroup: any, row: any) => {
            return ["selected", row.id, row.lastName, row.firstName, row.middleName]
          },
          pageSize: 4
        },
        {
          type: "SearchListComponent",
          itemTemplate: this.demoItemTemplate,
          id: "search-list-big",
          title: "Demo Big",
          dataCall: (gridDto: HciGridDto) => {
            return this.http.post<HciDataDto>("/api/search-sidebar-random-data", gridDto);
          },
          route: (rowGroup: any, row: any) => {
            return ["selected", row.id, row.lastName, row.firstName, row.middleName]
          },
          pageSize: 4,
          rowId: "id",
          rowClass: "test"
        }
      ]
    });
  `;

  @ViewChild(SidebarComponent, {static: true}) sidebar: SidebarComponent;

  @ViewChild("demoItemTemplate", { read: TemplateRef, static: true }) demoItemTemplate: any;

  selectedSearchType = "MRN";

  constructor(private changeDetectorRef: ChangeDetectorRef,
              private router: Router,
              private route: ActivatedRoute,
              private http: HttpClient) {}

  ngAfterViewInit() {
    this.sidebar.setConfig({
      sizebarSize: 260,
      children: [
        {
          type: "SearchListControllerComponent",
          title: "Demographics",
          fields: [
            {field: "firstName", display: "First Name"},
            {field: "middleName", display: "Middle Name"},
            {field: "lastName", display: "Last Name"}
          ],
          searchListChange: this.searchListChange,
          showQuickSearchOptions: true,
          searchTypeArray: ["MRN", "FIRST NAME", "LAST NAME", "PERSON ID" , "DOB"],
          selectedSearchType: this.selectedSearchType,
          disableLocalFiltering: true
        },
        {
          type: "SearchListComponent",
          itemTemplate: this.demoItemTemplate,
          id: "search-list-small",
          title: "Demo Small",
          inlineExpand: true,
          dataCall: (gridDto: HciGridDto) => {
            return this.http.post<HciDataDto>("/api/search-sidebar-big-data", gridDto);
          },
          route: (rowGroup: any, row: any) => {
            return ["selected", row.id, row.lastName, row.firstName, row.middleName]
          },
          pageSize: 4,
        },
        {
          type: "SearchListComponent",
          itemTemplate: this.demoItemTemplate,
          id: "search-list-big",
          title: "Demo Big",
          dataCall: (gridDto: HciGridDto) => {
            return this.http.post<HciDataDto>("/api/search-sidebar-random-data", gridDto);
          },
          route: (rowGroup: any, row: any) => {
            return ["selected", row.id, row.lastName, row.firstName, row.middleName]
          },
          pageSize: 4,
          rowId: "id",
          rowClass: "test"
        }
      ]
    });

    this.searchListChange.subscribe((searchList: string) => {
      console.info("SearchListChange: " + searchList);
    });

    this.itemHtml = "<pre class=\"language-html\"><code #code class=\"language-html\">" + prism.highlight(this.itemExample, prism.languages["html"]) + "</code></pre>";
    this.config1Html = "<pre class=\"language-js\"><code #code class=\"language-js\">" + prism.highlight(this.config1String, prism.languages["js"]) + "</code></pre>";
    this.changeDetectorRef.detectChanges();
  }

}
