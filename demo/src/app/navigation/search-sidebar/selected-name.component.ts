import {ActivatedRoute} from "@angular/router";
import {Component} from "@angular/core";

@Component({
  selector: "hci-demo-selected-name",
  template: `
    <div class="m-3 p-3 header">Selected name: {{name}}</div>
  `
})
export class SelectedNameComponent {

  name: string;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.name = params.get("last") + ", " + params.get("first") + " " + params.get("middle");
    });
  }
}
