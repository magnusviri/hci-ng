
export class DemoInterceptor {

  delayTime: number = 0;
  delayMin: number = 100;
  delayRange: number = 200;

  constructor() {
    this.delayTime = Math.floor(Math.random() * this.delayRange + this.delayMin);
  }
}
