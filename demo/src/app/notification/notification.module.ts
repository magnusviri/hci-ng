import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {NavigationModule} from "@huntsman-cancer-institute/navigation";
import {NotificationModule} from "@huntsman-cancer-institute/notification";

import {NotificationDemoComponent} from "./notification.component";

/**
 * @since 1.2.0
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    NavigationModule,
    NotificationModule,
    RouterModule.forChild([
      {path: "", component: NotificationDemoComponent}
    ])
  ],
  declarations: [
    NotificationDemoComponent
  ]
})
export class NotificationDemoModule {}
