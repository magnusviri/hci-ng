import {Component} from "@angular/core";

@Component({
  selector: "hci-reporting-framework-demo",
  template: `
    <hci-reporting-framework
      manageReportUrl="/artadmin/legacy/api/hci.artadmin.controller.ManageReports"
      commandPrefix="/artadmin/legacy/api/jsp/hci.artadmin.controller."
      dictAPILeadIn="/artadmin/api/dictionaries"
      reportsByType="global"
    >
      <!--reportsByType is optional-->
      <!--commandPrefix="/artadmin/legacy/api/jsp/hci.artadmin.controller." is another way-->
    </hci-reporting-framework>
  `
})
export class ReportingFrameworkDemoComponent { }
