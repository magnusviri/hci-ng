/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, ComponentFactoryResolver, isDevMode, Type, ViewChild, ViewContainerRef} from "@angular/core";

import {Subject} from "rxjs";

import {DashboardService, WidgetComponent, Widget, WidgetAttributeValue, WidgetAttributeChoice, WidgetAttributeContainer} from "@huntsman-cancer-institute/dashboard";
import {ComponentInjector, ComponentType} from "@huntsman-cancer-institute/utils";

/**
 * @since 1.0.0
 */
@ComponentType("SimpleContainerWidget")
@Component({
  selector: "simple-container-widget",
  template: `
    <h4 *ngIf="showTitle">{{widgetInstance?.widget?.title}}</h4>
    <ng-template #childWidgetRef></ng-template>
  `
})
export class SimpleContainerWidget extends WidgetComponent {

  private childWidgetRef: ViewContainerRef;
  private childWidgetName: string;
  private childWidgetComponent: WidgetComponent;

  constructor(private resolver: ComponentFactoryResolver, dashboardService: DashboardService) {
    super(dashboardService);
  }

  ngOnInit() {
    this.dashboardService.getWidget("SimpleContainerWidget").subscribe((widget: Widget) => {
      this.widget = widget;
    });
  }

  @ViewChild("childWidgetRef", { read: ViewContainerRef, static: true }) set childWidgetRefSetter(childWidgetRef: ViewContainerRef) {
    if (isDevMode()) {
      console.debug("SimpleContainerWidget.childWidgetRefSetter: " + this.childWidgetName);
    }
    this.childWidgetRef = childWidgetRef;
    this.setChildWidgetComponent();
  }

  setChildWidgetComponent(): void {
    if (!this.childWidgetRef || !this.childWidgetName) {
      return;
    }

    this.childWidgetRef.clear();

    this.childWidgetComponent = ComponentInjector.getComponent(this.resolver, this.childWidgetRef, this.childWidgetName);

    this.dashboardService.getWidget(this.childWidgetName).subscribe((widget: Widget) => {
      this.childWidgetComponent.setWidget(widget);
    });

    this.childWidgetComponent.attributeContainers.subscribe((attributeContainers: Map<string, WidgetAttributeContainer>) => {
      this.attributeContainers.next(this.getAttributeContainers());
    });

    this.childWidgetComponent.rendered.subscribe((rendered: boolean) => {
      this.rendered.next(rendered);
    });

    this.configChild();
    this.childWidgetComponent.config(this.widgetInstance.attributeValueSet.attributeValues);
    this.childWidgetComponent.render();
  }

  config(attributeValues: WidgetAttributeValue[]) {
    super.config(attributeValues);

    if (this.widgetInstance) {
      // Set values if childWidget
      for (var ac = 0; ac < this.widgetInstance.widget.attributeContainers.length; ac++) {
        for (var a = 0; a < this.widgetInstance.widget.attributeContainers[ac].attributes.length; a++) {
          if (this.widgetInstance.widget.attributeContainers[ac].attributes[a].name !== "childWidget") {
            continue;
          }
          this.setValues(this.widgetInstance.widget.attributeContainers[ac].attributes[a].name,
            this.widgetInstance.attributeValueSet.attributeValues.filter((value: WidgetAttributeValue) => {
              return this.widgetInstance.widget.attributeContainers[ac].attributes[a].idWidgetAttribute === value.idWidgetAttribute;
            })
          );
          this.setValues(this.widgetInstance.widget.attributeContainers[ac].attributes[a].name,
            this.widgetInstance.widget.attributeValueSet.attributeValues.filter((value: WidgetAttributeValue) => {
              return this.widgetInstance.widget.attributeContainers[ac].attributes[a].idWidgetAttribute === value.idWidgetAttribute;
            })
          );
        }
      }

      // Set values if NOT renderer
      for (var ac = 0; ac < this.widgetInstance.widget.attributeContainers.length; ac++) {
        for (var a = 0; a < this.widgetInstance.widget.attributeContainers[ac].attributes.length; a++) {
          if (this.widgetInstance.widget.attributeContainers[ac].attributes[a].name === "childWidget") {
            continue;
          }
          this.setValues(this.widgetInstance.widget.attributeContainers[ac].attributes[a].name,
            this.widgetInstance.attributeValueSet.attributeValues.filter((value: WidgetAttributeValue) => {
              return this.widgetInstance.widget.attributeContainers[ac].attributes[a].idWidgetAttribute === value.idWidgetAttribute;
            })
          );
          this.setValues(this.widgetInstance.widget.attributeContainers[ac].attributes[a].name,
            this.widgetInstance.widget.attributeValueSet.attributeValues.filter((value: WidgetAttributeValue) => {
              return this.widgetInstance.widget.attributeContainers[ac].attributes[a].idWidgetAttribute === value.idWidgetAttribute;
            })
          );
        }
      }
    }
  }

  configChild() {
    for (var ac = 0; ac < this.widgetInstance.widget.attributeContainers.length; ac++) {
      for (var a = 0; a < this.widgetInstance.widget.attributeContainers[ac].attributes.length; a++) {
        if (this.widgetInstance.widget.attributeContainers[ac].attributes[a].name === "childWidget") {
          continue;
        }
        this.childWidgetComponent.setValues(this.widgetInstance.widget.attributeContainers[ac].attributes[a].name,
          this.widgetInstance.attributeValueSet.attributeValues.filter((value: WidgetAttributeValue) => {
            return this.widgetInstance.widget.attributeContainers[ac].attributes[a].idWidgetAttribute === value.idWidgetAttribute;
          })
        );
        this.childWidgetComponent.setValues(this.widgetInstance.widget.attributeContainers[ac].attributes[a].name,
          this.widgetInstance.widget.attributeValueSet.attributeValues.filter((value: WidgetAttributeValue) => {
            return this.widgetInstance.widget.attributeContainers[ac].attributes[a].idWidgetAttribute === value.idWidgetAttribute;
          })
        );
      }
    }
  }

  setValues(name: string, attributeValues: WidgetAttributeValue[]): boolean {
    super.setValues(name, attributeValues);

    if (attributeValues.length === 0) {
      return;
    } else if (name === "childWidget") {
      this.childWidgetName = attributeValues[0].valueString;
      this.setChildWidgetComponent();
    } else if (this.childWidgetComponent) {
      this.childWidgetComponent.setValues(name, attributeValues);
    }
    return true;
  }

  getWidgetChoices(name: string): Subject<WidgetAttributeChoice[]> {
    return this.childWidgetComponent.getWidgetChoices(name);
  }

  getAttributeContainers(): Map<string, WidgetAttributeContainer> {
    if (this.childWidgetComponent) {
      return Widget.merge(super.getAttributeContainers(), this.childWidgetComponent.getAttributeContainers());
    } else {
      return super.getAttributeContainers();
    }
  }

}
