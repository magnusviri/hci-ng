import {Component, ElementRef} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {ModalDismissReasons} from "@ng-bootstrap/ng-bootstrap";
import * as d3 from "d3";
import {ScaleOrdinal} from "d3-scale";

import {DashboardService, WidgetComponent, Widget, WidgetAttributeValue} from "@huntsman-cancer-institute/dashboard";
import {ComponentType} from "@huntsman-cancer-institute/utils";

@ComponentType("PieChartWidget")
@Component({
  selector: "pie-chart",
  template: "<ng-template></ng-template>",
  styles: [ `

    /deep/ path.slice {
      stroke-width:2px;
    }

    /deep/ polyline {
      opacity: .3;
      stroke: black;
      stroke-width: 2px;
      fill: none;
    }

    /deep/ .labelValue {
      font-size: 60%;
      opacity: .5;

    }
    /deep/ .toolTip {
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      position: absolute;
      display: none;
      width: auto;
      height: auto;
      background: none repeat scroll 0 0 white;
      border: 0 none;
      border-radius: 8px 8px 8px 8px;
      box-shadow: -3px 3px 15px #888888;
      color: black;
      font: 12px sans-serif;
      padding: 5px;
      text-align: center;
    }
    /deep/ text {
      font: 12px sans-serif;
    }

    /deep/ .arc-path {
      stroke: white;
      stroke-width: 2px;
    }
    
  ` ]
})
export class PieChartWidget extends WidgetComponent {

  protected htmlElement: HTMLElement;
  protected svgParent: any;
  protected svg: any;
  protected g: any;

  private width: number;
  private height: number;
  private radius: number;

  private x: any;
  private y: any;
  private tooltip : any;

  private data: Object[];
  private keys : any[];
  private selectedData : any;

  private startColor: string = "#eeeeff";
  private stopColor: string = "#9999ff";
  private updateChartData : any;
  private handleDataChange : any;
  private handleDataChangeAfterService: any;
  private showDialog: any;
  private getDismissReason: any;

  private datasource: string;

  constructor(private element: ElementRef, private http: HttpClient, dashboardService: DashboardService) {
    super(dashboardService);
    this.htmlElement = this.element.nativeElement;
    this.svgParent = d3.select(this.htmlElement);
    this.dashboardService.getWidget("PieChartWidget").subscribe((widget: Widget) => {
      this.widget = widget;
    });
  }

  ngOnInit() {
    this.rendered.next(false);
    window.onresize = (e) => {
      this.initSvg();
    };
  }

  setValues(name: string, attributeValues: WidgetAttributeValue[]): boolean {
    console.info("PieChartRenderer.setValue " + name);
    if (attributeValues.length === 0) {
      return;
    } else if (name === "datasource") {
      if (this.rendered.getValue() && this.datasource != attributeValues[0].valueString) {
        this.datasource = attributeValues[0].valueString;
        this.render();
      } else {
        this.datasource = attributeValues[0].valueString;
      }
    } else if (name === "startColor") {
      this.startColor = attributeValues[0].valueString;

      if (this.rendered.getValue()) {
        this.initSvg();
      }
    } else if (name === "stopColor") {
      this.stopColor = attributeValues[0].valueString;

      if (this.rendered.getValue()) {
        this.initSvg();
      }
    }
    return false;
  }

  render() {
    this.http.get(this.datasource)
      .subscribe(
        response => {
          this.data = JSON.parse(JSON.stringify(response));
          this.initSvg();
          this.dashboardService.setWidgetRendered(true);
        },
        error => {
          console.error(error);
        }
      );
  }

  private initSvg() {
    var self = this;
    var instanceId = Math.floor(Math.random() * 100000);
    var margin = {
      top: 5,
      right: 15,
      bottom: 50,
      left: 20
    };
    self.width = this.getParentDimension(this.htmlElement, "width") - margin.right - margin.left;
    self.height = this.width / 1.5 ;

    self.svgParent.html("");

    // Update data for the chart
    self.updateChartData = function () {
      //if (!self.svg) {
        this.svg = this.svgParent
          .classed("widget-svg-container", true)
          .append("svg")
          .attr("preserveAspectRatio", "xMinYMin meet")
          .attr("viewBox", "0 0 " + this.width + " " + this.height)
          .append("g")
          .classed("widget-svg-content-responsive", true);
      //}
      self.svg.selectAll(".arctext").remove();
      self.svg.selectAll(".pie-widget-total-label").remove();

      var gradientColor = d3.scaleLinear().domain([0, this.selectedData.length]).range([this.startColor, this.stopColor]);

      var pie = d3.pie()(self.selectedData.map(function (d: any) {
        return d.population;
      }));
      var path = d3.arc()
        .outerRadius(radius)
        .innerRadius(radius / 2);
      var outsideLabel = d3.arc()
        .outerRadius(radius * 0.9)
        .innerRadius(radius * 0.9);
      var middleLabel = d3.arc()
        .outerRadius(radius * 0.75)
        .innerRadius(radius * 0.75);
      var insideLabel = d3.arc()
        .outerRadius(radius * 0.6)
        .innerRadius(radius * 0.6);
      var g = self.svg
        .attr("width", self.width + margin.left + margin.right)
        .attr("height", self.height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + self.width / 2 + "," + self.height / 2 + ")");
      var defs = g.append("defs");
      var lightGradients = defs.selectAll(".arc")
        .data(pie)
        .enter()
        .append("radialGradient")
        .attr("id", function (d: any) {
          return "lightGradient-" + instanceId + "-" + d.index;
        })
        .attr("gradientUnits", "userSpaceOnUse")
        .attr("cx", "0")
        .attr("cy", "0")
        .attr("r", "120%");
      var darkGradients = defs.selectAll(".arc")
        .data(pie)
        .enter()
        .append("radialGradient")
        .attr("id", function (d: any) {
          return "darkGradient-" + instanceId + "-" + d.index;
        })
        .attr("gradientUnits", "userSpaceOnUse")
        .attr("cx", "0")
        .attr("cy", "0")
        .attr("r", "120%");
      lightGradients.append("stop")
        .attr("offset", "0%")
        .attr("style", function (d: any) {
          return "stop-color: " + d3.color(gradientColor(d.index)) + ";";
        });
      lightGradients.append("stop")
        .attr("offset", "100%")
        .attr("style", function (d: any) {
          return "stop-color: black;";
        });
      darkGradients.append("stop")
        .attr("offset", "0%")
        .attr("style", function (d: any) {
          return "stop-color: " + d3.color(gradientColor(d.index)).darker(0.5) + ";";
        });
      darkGradients.append("stop")
        .attr("offset", "100%")
        .attr("style", function (d: any) {
          return "stop-color: black;";
        });
      self.tooltip = d3.select("body")
        .append("div")
        .attr("class", "pie-widget-tooltip")
        .style("opacity", 0);
      var arc = g.selectAll(".arc")
        .data(pie)
        .enter()
        .append("g")
        .attr("class", "arc");
      var arcpath = arc.append("path")
        .attr("id", function (d: any) {
          return d.index;
        })
        .attr("d", path)
        .attr("stroke", "white")
        .attr("stroke-width", "2px")
        .attr("fill", function (d: any) {
          return "url(#lightGradient-" + instanceId + "-" + d.index + ")";
        })
        .on("click", function () {
          self.showDialog();
        })
        .on("mouseover", function (d: any) {
          d3.select(this).style("fill", function () {
            return "url(#darkGradient-" + instanceId + "-" + d3.select(this).attr("id") + ")";
          });
          self.tooltip.transition()
            .duration(200)
            .style("opacity", 0.9);
          self.tooltip.html(getPatientsLabel(d.index))
            .style("left", (d3.event.pageX) + "px")
            .style("top", (d3.event.pageY - 28) + "px");
        })
        .on("mouseout", function () {
          d3.select(this).style("fill", function () {
            return "url(#lightGradient-" + instanceId + "-" + d3.select(this).attr("id") + ")";
          });
          self.tooltip.transition()
            .duration(200)
            .style("opacity", 0.0);
        })
        .transition()
        .duration(function(d:any, i:any) {
          return i * 800;
        })
        .attrTween('d', function(d:any) {
          var i = d3.interpolate(d.startAngle + 0.1, d.endAngle);
          return function (t: any) {
            d.endAngle = i(t);
            return path(d);
          }
        });
      function arcTween(a: any) {
        var i = d3.interpolate(this._current, a);
        this._current = i(0);
        return function(t: any) {
          return path(i(t));
        };
      }
      var gtext = self.svg
        .append("g")
        .attr("transform", "translate(" + self.width / 2 + "," + self.height / 2 + ")");
      var arctext = gtext.selectAll(".arctext")
        .data(pie)
        .enter()
        .append("g")
        .attr("class", "arctext");
      arctext.append("text")
        .transition()
        .duration(750)
        .attr("transform", function (d: any) {
          return "translate(" + middleLabel.centroid(d) + ")";
        })
        .attr("dy", "-0.75em")
        .attr("font-family", "sans-serif")
        .attr("font-size", "10px")
        .attr("class", "sponsor-pie-widget-label")
        .text(function (d: any) {
          if (d.endAngle - d.startAngle < 0.3) {
            return "";
          } else {
            return getStudyLabel(d.index);
          }
        });
      var n = 0;
      for (var i = 0; i < self.selectedData.length; i++) {
        n = n + this.selectedData[i]["population"];
      }
      var total = self.svg
        .append("g")
        .attr("transform", "translate(" + self.width / 2 + "," + self.height / 2 + ")")
      total.append("text")
        .attr("class", "pie-widget-total-label")
        .text("Total\r\n" + n);
    }.bind(self);

    self.showDialog = function() {
      //this.router.navigateByUrl('/user');
    }.bind(self);

    self.getDismissReason = function(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return  `with: ${reason}`;
      }
    }.bind(self);

    // Handle drop-down data change
    self.handleDataChangeAfterService = function(key: string) {
      self.selectedData = self.data;
      self.updateChartData();
    }.bind(self);

    var radius = Math.min(this.width, this.height) / 2;

    var getStudyLabel = function (i: any) {
      return this.selectedData[i]["age"];
    }.bind(this);
    var getPatientsLabel = function (i: any) {
      return "<div>Age: " + this.selectedData[i]["age"] + "</div><div>" + this.selectedData[i]["population"] + "</div>";
    }.bind(this);

    self.handleDataChangeAfterService("Init");

    self.rendered.next(true);
  }
}
