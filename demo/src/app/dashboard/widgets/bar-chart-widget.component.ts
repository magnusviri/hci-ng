import {Component, ElementRef, isDevMode} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {Subject} from "rxjs";
import * as d3 from "d3";

import {DashboardService, WidgetComponent, Widget, WidgetAttributeChoice, WidgetAttributeValue} from "@huntsman-cancer-institute/dashboard";
import {ComponentType} from "@huntsman-cancer-institute/utils";

export interface AccrualByMDG {
  id: number;
  mdgName: string;
  openCount: number;
  closeCount: number;
  pendingCount: number;
}

@ComponentType("BarChartWidget")
@Component({
  selector: "bar-chart",
  template: "<ng-template></ng-template>",
  styles: [ `

    /deep/ .pie-widget-label {
      text-anchor: middle;
      font-size: large;
      font-weight: bold;
    }

    /deep/ .pie-widget-total-label {
      text-anchor: middle;
      font-size: larger;
      font-weight: bold;
    }

    /deep/ div.pie-widget-tooltip {
      position: absolute;
      text-align: center;
      width: 100px;
      height: 50px;
      padding: 4px;
      font: 16px sans-serif;
      background: #dddddd;
      border: black 1px solid;
      border-radius: 8px;
      pointer-events: none;
      font-weight: bold;
    }

    /deep/ svg {
      width: 100%;
      height: 100%;
    }

    /deep/ path.slice{
      stroke-width: 2px;
    }

    /deep/ polyline {
      opacity: .3;
      stroke: black;
      stroke-width: 2px;
      fill: none;
    }

    /deep/ .labelValue {
      font-size: 60%;
      opacity: .5;

    }

    /deep/ .bar {
      stroke: white;
      stroke-width: 2px;
    }

    /deep/ text {
      font: 12px sans-serif;
    }

  ` ]
})
export class BarChartWidget extends WidgetComponent {

  protected g: any;
  protected svg: any;
  protected htmlElement: HTMLElement;
  protected svgParent: any;

  private width: number;
  private height: number;
  private maxHeight: number;
  private data: AccrualByMDG[];
  private origData: AccrualByMDG[];

  private dataFilter: WidgetAttributeValue[] = [];

  private x: any;
  private y: any;
  private tooltip : any;

  private sortTimeout : any;

  // Attributes
  private datasource: string;
  private widgetChoices: Map<string, WidgetAttributeChoice[]> = new Map<string, WidgetAttributeChoice[]>();

  constructor(private element: ElementRef, private http: HttpClient, dashboardService: DashboardService) {
    super(dashboardService);
    this.htmlElement = this.element.nativeElement;
    this.svgParent = d3.select(this.htmlElement);
    this.dashboardService.getWidget("BarChartWidget").subscribe((widget: Widget) => {
      this.widget = widget;
    });
  }

  ngOnInit() {
    window.onresize = (e) => {
      this.initSvg();
    };
  }

  setValues(name: string, attributeValues: WidgetAttributeValue[]): boolean {
    if (super.setValues(name, attributeValues)) {
      return true;
    } else if (!attributeValues || attributeValues.length === 0) {
      return false;
    } else if (name === "datasource") {
      if (this.rendered.getValue() && this.datasource !== attributeValues[0].valueString) {
        this.datasource = attributeValues[0].valueString;
        this.render();
      } else if (this.datasource !== attributeValues[0].valueString) {
        this.datasource = attributeValues[0].valueString;
      }
    } else if (name === "mdgName") {
      this.dataFilter = attributeValues;
      if (this.rendered.getValue()) {
        this.initSvg();
      }
    }
    return false;
  }

  render() {
    this.http.get(this.datasource)
      .subscribe(
        response => {
          this.origData = <AccrualByMDG[]>response;
          this.data = this.origData;
          this.generateWidgetChoices();
          this.initSvg();
          this.dashboardService.setWidgetRendered(true);
        },
        error => {
          console.error(error);
        }
      );
  }

  generateWidgetChoices() {
    if (this.widgetChoices.size > 0) {
      return;
    }

    let choices: WidgetAttributeChoice[] = [];

    for (let o of this.data) {
      let choice: WidgetAttributeChoice = new WidgetAttributeChoice();
      choice.value = o["mdgName"];
      choice.display = o["mdgName"];
      choices.push(choice);
    }

    this.widgetChoices.set("mdgName", choices);
  }

  getWidgetChoices(name: string): Subject<WidgetAttributeChoice[]> {
    let widgetChoices: Subject<WidgetAttributeChoice[]> = new Subject<WidgetAttributeChoice[]>();
    widgetChoices.next(this.widgetChoices.get(name));
    return widgetChoices;
  }

  private initSvg() {
    this.svgParent.html("");

    if (!this.origData) {
      return;
    }

    if (this.dataFilter.length > 0) {
      this.data = this.origData.filter((o: AccrualByMDG) => {
        for (let filter of this.dataFilter) {
          if (o["mdgName"] === filter.valueString) {
            return true;
          }
        }
        return false;
      });
    } else {
      this.data = this.origData;
    }

    var margin = {
      top: 10,
      right: 15,
      bottom: 50,
      left: 30
    };

    this.width = this.getParentDimension(this.htmlElement, "width") - margin.right - margin.left;
    this.height = this.width / 1.5;

    this.svg = this.svgParent
      .classed("widget-svg-container", true)
      .append("svg")
      .attr("preserveAspectRatio", "none")
      .attr("viewBox", "0 0 " + this.width + " " + this.height);

    var svgg = this.svg
      .append("g")
      .classed("widget-svg-content-responsive", true)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var x0 = d3.scaleBand()
      .rangeRound([0, this.width])
      .paddingInner(0.1);

    var x1 = d3.scaleBand();

    var y = d3.scaleLinear()
      .range([this.height - margin.top - margin.bottom, 0]);

    var color = d3.scaleOrdinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

    var xAxis = d3.axisBottom(x0).ticks(6, d3.format(",d"));
    var yAxis = d3.axisLeft(y);

    var ageNames = d3.keys(this.data[0]).filter(function(key) {
      if (key !== "mdgName" && key !== "id" && key !== "ages") {
        return key;
      }
    });

    this.data.forEach(function(d: any) {
      d.ages = ageNames.map(function(name) { return {name: name, value: +d[name]}; });
    });

    x0.domain(this.data.map(function(d) { return d.mdgName; }));
    x1.domain(ageNames).rangeRound([0, x0.bandwidth()]);

    y.domain([0, d3.max(this.data, function(d: any) {
      return <number>d3.max(d.ages, function(d: any) {
        return <number>d.value;
      });
    })]);

    var state = svgg.selectAll(".groups")
      .data(this.data)
      .enter().append("g")
      .attr("class", "groups")
      .attr("transform", function(d: any) { return "translate(" + x0(d.mdgName) + ",0)"; });

    // Storing height in a local variable so that it is available in the callback
    var height = this.height;

    state.selectAll("rect")
      .data(function(d: any) { return d.ages; })
      .enter().append("rect")
      .attr("class", "bar")
      .attr("width", x1.bandwidth())
      .attr("x", function(d: any) { return x1(d.name); })
      .attr("y", this.height - margin.top - margin.bottom)
      .attr("height", 0)
      .transition()
      .duration(1000)
      .ease(d3.easeQuadInOut)
      .attr("height", function(d: any) {
        return Math.max(0, (Number(height - margin.top - margin.bottom) - Number(y(d.value))));
      })
      .attr("y", function(d: any) { return y(d.value); })
      .style("fill", function(d: any) { return color(d.name); });

    // Set the legend
    var legend = svgg.selectAll(".legend")
      .data(ageNames.slice().reverse())
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d: any, i: any) { return "translate(0," + i * 20 + ")"; });

    legend.append("rect")
      .attr("x", this.width - margin.left - margin.right - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

    legend.append("text")
      .attr("x", this.width - margin.left - margin.right - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function(d: any) { return d; });

    svgg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Accruals");

    var xAxisG = svgg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + (this.height - margin.bottom - margin.top)  + ")")
      .call(xAxis);

    xAxisG
      .call(xAxis)
      .selectAll("text")
      .attr("dx", "-.8em")
      .attr("dy", "1em")
      .attr("transform", "rotate(-20)" );

    this.rendered.next(true);
  }

}
