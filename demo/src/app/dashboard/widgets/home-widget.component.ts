/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component} from "@angular/core";

import {DashboardService, WidgetComponent} from "@huntsman-cancer-institute/dashboard";
import {ComponentType} from "@huntsman-cancer-institute/utils";

/**
 * @since 1.0.0
 */
@ComponentType("HomeWidget")
@Component({
  selector: "home-widget",
  template: `
    <h4>{{widgetInstance?.widget?.title}}</h4>
    <p>
      Widgets can be created in any project and dynamically added into the dashboard.  It is up the implementing application
      to provide the widgets available to the user.
      </p>
      <h6>Other Dashboard Demos</h6>
      <ul>
        <li>
          <a routerLink="../many-widgets">A dashboard pre configured with more widgets.</a>
        </li>
    </ul>
  `
})
export class HomeWidget extends WidgetComponent {

  constructor(dashboardService: DashboardService) {
    super(dashboardService);
  }

  ngOnInit() {
    this.rendered.next(true);
  }

}
