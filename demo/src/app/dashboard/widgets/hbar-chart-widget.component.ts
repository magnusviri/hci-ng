import {Component, ElementRef} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {Subject} from "rxjs";
import * as d3 from "d3";

import {DashboardService, WidgetComponent, Widget, WidgetAttributeChoice, WidgetAttributeValue} from "@huntsman-cancer-institute/dashboard";
import {ComponentType} from "@huntsman-cancer-institute/utils";

export interface AccrualByMDG {
  id: number,
  mdgName: string;
  openCount: number,
  closeCount: number,
  pendingCount: number
}

@ComponentType("HBarChartWidget")
@Component({
  selector: "hbar-chart",
  template: "<ng-template></ng-template>",
  styles: [ `

    .pie-widget-label {
      text-anchor: middle;
      font-size: large;
      font-weight: bold;
    }

    .pie-widget-total-label {
      text-anchor: middle;
      font-size: larger;
      font-weight: bold;
    }

    div.pie-widget-tooltip {
      position: absolute;
      text-align: center;
      width: 100px;
      height: 50px;
      padding: 4px;
      font: 16px sans-serif;
      background: #dddddd;
      border: black 1px solid;
      border-radius: 8px;
      pointer-events: none;
      font-weight: bold;
    }

    path.slice{
      stroke-width: 2px;
    }

    polyline {
      opacity: .3;
      stroke: black;
      stroke-width: 2px;
      fill: none;
    }

    .labelValue {
      font-size: 60%;
      opacity: .5;

    }

    .bar {
      stroke: white;
      stroke-width: 2px;
    }
    
    text {
      font: 12px sans-serif;
    }

  ` ]
})
export class HBarChartWidget extends WidgetComponent {

  protected g: any;
  protected svg: any;
  protected htmlElement: HTMLElement;
  protected svgParent: any;

  private width: number;
  private height: number;
  private data: AccrualByMDG[];
  private origData: AccrualByMDG[];

  private dataFilter: WidgetAttributeValue[] = [];

  private x: any;
  private y: any;
  private tooltip : any;

  private sortTimeout : any;

  // Attributes
  private datasource: string;
  private widgetChoices: Map<string, WidgetAttributeChoice[]> = new Map<string, WidgetAttributeChoice[]>();

  constructor(private element: ElementRef, private http: HttpClient, dashboardService: DashboardService) {
    super(dashboardService);
    this.htmlElement = this.element.nativeElement;
    this.svgParent = d3.select(this.htmlElement);
    this.dashboardService.getWidget("HBarChartWidget").subscribe((widget: Widget) => {
      this.widget = widget;
    });
  }

  ngOnInit() {
    window.onresize = (e) => {
      this.initSvg();
    };
  }

  setValues(name: string, attributeValues: WidgetAttributeValue[]): boolean {
    if (attributeValues.length === 0) {
      return;
    } else if (name === "datasource") {
      if (this.rendered.getValue() && this.datasource != attributeValues[0].valueString) {
        this.datasource = attributeValues[0].valueString;
        this.render();
      } else {
        this.datasource = attributeValues[0].valueString;
      }
    } else if (name === "mdgName") {
      this.dataFilter = attributeValues;
      if (this.rendered.getValue()) {
        this.initSvg();
      }
    }
    return false;
  }

  render() {
    this.http.get(this.datasource)
      .subscribe(
        response => {
          this.origData = JSON.parse(JSON.stringify(response));
          this.data = this.origData;
          this.generateWidgetChoices();
          this.initSvg();
          this.dashboardService.setWidgetRendered(true);
        },
        error => {
          console.error(error);
        }
      );
  }

  generateWidgetChoices() {
    if (this.widgetChoices.size > 0) {
      return;
    }

    let choices: WidgetAttributeChoice[] = [];

    for (let o of this.data) {
      let choice: WidgetAttributeChoice = new WidgetAttributeChoice();
      choice.value = o["mdgName"];
      choice.display = o["mdgName"];
      choices.push(choice);
    }

    this.widgetChoices.set("mdgName", choices);
  }

  getWidgetChoices(name: string): Subject<WidgetAttributeChoice[]> {
    let widgetChoices: Subject<WidgetAttributeChoice[]> = new Subject<WidgetAttributeChoice[]>();
    widgetChoices.next(this.widgetChoices.get(name));
    return widgetChoices;
  }

  private initSvg() {
    this.svgParent.html("");

    if (!this.origData) {
      return;
    }

    if (this.dataFilter.length > 0) {
      this.data = this.origData.filter((o: AccrualByMDG) => {
        for (let filter of this.dataFilter) {
          if (o["mdgName"] === filter.valueString) {
            return true;
          }
        }
        return false;
      });
    } else {
      this.data = this.origData;
    }

    var margin = {
      top: 5,
      right: 15,
      bottom: 40,
      left: 60
    };

    this.width = this.getParentDimension(this.htmlElement, "width") - margin.right - margin.left;
    this.height = this.width / 1.5;

    this.svg = this.svgParent
      .classed("widget-svg-container", true)
      .append("svg")
      .attr("preserveAspectRatio", "none")
      .attr("viewBox", "0 0 " + this.width + " " + this.height);

    var svgg = this.svg
      .append("g")
      .classed("widget-svg-content-responsive", true)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var y0 = d3.scaleBand()
      .rangeRound([0, this.height - margin.bottom])
      .paddingInner(0.1);

    var y1 = d3.scaleBand();

    var x = d3.scaleLinear()
      .range([this.width, 0]);

    var color = d3.scaleOrdinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

    var yAxis = d3.axisLeft(y0).ticks(6, d3.format(",d"));
    var xAxis = d3.axisBottom(x);

    var ageNames = d3.keys(this.data[0]).filter(function(key) {
      if (key !== "mdgName" && key!== "id" && key!== "ages"){
        return key;
      }
    });

    this.data.forEach(function(d: any) {
      d.ages = ageNames.map(function(name) { return {name: name, value: +d[name]}; });
    });

    y0.domain(this.data.map(function(d) { return d.mdgName; }));
    y1.domain(ageNames).rangeRound([0, y0.bandwidth()]);

    x.domain([0, d3.max(this.data, function(d: any) {
      return <number>d3.max(d.ages, function(d: any) {
        return <number>d.value;
      });
    })]);

    var xAxisG = svgg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + (this.height - (margin.bottom - margin.top  ) )  + ")")
      .call(xAxis)
      .text("Accruals");

    svgg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end");

    xAxisG
      .call(xAxis)
      .selectAll("text")
      .attr("dx", "-.8em")
      .attr("dy", "1em")
      .attr("transform", "rotate(-20)" );


    var state = svgg.selectAll(".groups")
      .data(this.data)
      .enter().append("g")
      .attr("class", "groups")
      .attr("transform", function(d: any) { return "translate(2," + y0(d.mdgName) + ")"; });

    // Storing height in a local variable so that it is available in the callback
    var height = this.height;
    var width = this.width;

    state.selectAll("rect")
      .data(function(d: any) { return d.ages; })
      .enter().append("rect")
      .attr("class", "bar")
      .attr("height", y1.bandwidth())
      .attr("y", function(d: any) { return y1(d.name); })
      .attr("x", function(d: any) { return 0; })

      .attr("width", 0)
      .transition()
      .duration(1000)
      .ease(d3.easeQuadInOut)
      .attr("width", function(d: any) {
        return Math.max(0, Number(x(d.value)));
      })
      .style("fill", function(d: any) { return color(d.name); });

    /*
      .attr("width", function(d: any) {
        return (Number(width - margin.right - margin.left) - Number(x(d.value)));
      })
      .style("fill", function(d: any) { return color(d.name); });*/

    // Set the legend
    var legend = svgg.selectAll(".legend")
      .data(ageNames.slice().reverse())
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d: any, i: any) { return "translate(0," + i * 20 + ")"; });

    legend.append("rect")
      .attr("x", this.width - margin.left - margin.right - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

    legend.append("text")
      .attr("x", this.width - margin.left - margin.right -24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function(d: any) { return d; });

    this.rendered.next(true);
  }

}
