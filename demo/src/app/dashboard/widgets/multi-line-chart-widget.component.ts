import {Component, ElementRef} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import * as d3 from "d3";

import {DashboardService, WidgetComponent, Widget, WidgetAttributeValue} from "@huntsman-cancer-institute/dashboard";
import {ComponentType} from "@huntsman-cancer-institute/utils";

@ComponentType("MultiLineChartWidget")
@Component({
  selector: "multi-line-chart",
  template: "<ng-template></ng-template>",
  styles: [ `

    /deep/ path.slice {
      stroke-width:2px;
    }

    /deep/ polyline {
      opacity: .3;
      stroke: black;
      stroke-width: 2px;
      fill: none;
    }
    /deep/ .legend {
      padding: 5px;
      font: 10px sans-serif;
      background: yellow;
      box-shadow: 2px 2px 1px #888;
    }
    /deep/ .labelValue
    {
      font-size: 60%;
      opacity: .5;

    }
    /deep/ .pie-widget-label {
      text-anchor: middle;
      font-size: large;
      font-weight: bold;
    }

    /deep/ .pie-widget-total-label {
      text-anchor: middle;
      font-size: larger;
      font-weight: bold;
    }

    /deep/ div.pie-widget-tooltip {
      position: absolute;
      text-align: center;
      width: 100px;
      height: 50px;
      padding: 4px;
      font: 16px sans-serif;
      background: #dddddd;
      border: black 1px solid;
      border-radius: 8px;
      pointer-events: none;
      font-weight: bold;
    }

    /deep/ text {
      font: 12px sans-serif;
    }

    /deep/ .legend rect {
      fill:white;
      stroke:black;
      opacity:0.8;
    }

    /deep/ .line {
      fill: none;
      stroke-width: 4px;
    }

  ` ]
})
export class MultiLineChartWidget extends WidgetComponent {

  protected htmlElement: HTMLElement;
  protected svgParent: any;
  protected svg: any;
  protected g: any;

  data: any;
  resultString: any;
  width: number;
  height: number;
  x: any;
  y: any;
  z: any;
  line: any;

  private tooltip : any;

  // Attributes
  private datasource: string;
  private xAxisPosition: string = "bottom";
  private xAxisTicks: number = 5;
  private yAxisPosition: string = "left";
  private yAxisTicks: number = 5;

  constructor(private element: ElementRef, private http: HttpClient, dashboardService: DashboardService) {
    super(dashboardService);
    this.htmlElement = this.element.nativeElement;
    this.svgParent = d3.select(this.htmlElement);
    this.dashboardService.getWidget("MultiLineChartWidget").subscribe((widget: Widget) => {
      this.widget = widget;
    });
  }

  ngOnInit() {
    window.onresize = (e) => {
      this.render();
    };
  }

  config(attributeValues: WidgetAttributeValue[]) {
    this.rendered.next(false);

    if (this.widget) {
      for (let attributeContainer of this.widget.attributeContainers) {
        for (let attribute of attributeContainer.attributes) {
          this.setValues(attribute.name,
            // Set the widget's own attribute values
            this.widget.attributeValueSet.attributeValues.filter((value: WidgetAttributeValue) => {
              return value.idWidgetAttribute === attribute.idWidgetAttribute;
            })
          );

          // Set user defined attribute values
          this.setValues(attribute.name,
            attributeValues.filter((value: WidgetAttributeValue) => {
              return value.idWidgetAttribute === attribute.idWidgetAttribute;
            })
          );
        }
      }
    }
  }

  setValues(name: string, attributeValues: WidgetAttributeValue[]): boolean {
    if (attributeValues.length === 0) {
      return;
    }

    if (name === "datasource") {
      if (this.rendered && this.datasource != attributeValues[0].valueString) {
        this.datasource = attributeValues[0].valueString;
        this.tryDatasource();
      } else {
        this.datasource = attributeValues[0].valueString;
      }
    } else if (name === "xAxisPosition") {
      if (this.rendered && this.xAxisPosition != attributeValues[0].valueString) {
        this.xAxisPosition = attributeValues[0].valueString;
        this.render();
      } else {
        this.xAxisPosition = attributeValues[0].valueString;
      }
    } else if (name === "xAxisTicks") {
      if (this.rendered && this.xAxisTicks != attributeValues[0].valueInteger) {
        this.xAxisTicks = attributeValues[0].valueInteger;
        this.render();
      } else {
        this.xAxisTicks = attributeValues[0].valueInteger;
      }
    } else if (name === "yAxisPosition") {
      if (this.rendered && this.yAxisPosition != attributeValues[0].valueString) {
        this.yAxisPosition = attributeValues[0].valueString;
        this.render();
      } else {
        this.yAxisPosition = attributeValues[0].valueString;
      }
    } else if (name === "yAxisTicks") {
      if (this.rendered && this.yAxisTicks != attributeValues[0].valueInteger) {
        this.yAxisTicks = attributeValues[0].valueInteger;
        this.render();
      } else {
        this.yAxisTicks = attributeValues[0].valueInteger;
      }
    }
    return false;
  }

  tryDatasource() {
    this.http.get(this.datasource)
      .subscribe(
        response => {
          this.data = JSON.parse(JSON.stringify(response));
          this.render();
        },
        error => {
          console.error(error);
        }
      );
  }

  tryRender() {
    if (this.rendered) {
      this.render();
    }
  }

  render() {
    this.svgParent.html("");

    if (!this.data) {
      this.tryDatasource();
      return;
    }

    this.g = undefined;
    this.svg = undefined;
    this.x = undefined;
    this.y = undefined;
    this.z = undefined;

    let dates: any = this.data.map((v: any) => v["values"].map((v: any) => v["date"]))[0];

    var margin = {
      top: 5,
      right: 20,
      bottom: 40,
      left: 40
    };

    if (this.xAxisPosition === "top") {
      margin.top = 20;
      margin.bottom = 5;
    }

    if (this.yAxisPosition === "right") {
      margin.left = 30;
      margin.right = 40;
    }

    var tooltip = d3.select("body")
      .append("div")
      .attr("class", "pie-widget-tooltip")
      .style("opacity", 0);

    this.width = this.getParentDimension(this.htmlElement, "width") - margin.left - margin.right;
    //this.height = this.getParentDimension(this.htmlElement, "height") - margin.top - margin.bottom;
    this.height = this.width / 2;

    // Create the SVG container and set the origin.
    this.svg = this.svgParent
      .classed("widget-svg-container", true)
      .append("svg")
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 " + this.width + " " + this.height);

    this.x = d3.scaleTime().range([0, this.width - margin.left - margin.right]);
    this.y = d3.scaleLinear().range([this.height - margin.top - margin.bottom, 0]);

    this.z = d3.scaleOrdinal(d3.schemeCategory10);

    this.line = d3.line()
      .curve(d3.curveBasis)
      .x((d: any) => this.x(d["date"]))
      .y((d: any) => this.y(d["money"]));

    this.x.domain(d3.extent(dates, (d: Date) => d));

    this.y.domain([
      d3.min(this.data, function (c) {
        return d3.min(c["values"], function (d) {
          return d["money"];
        });
      }),
      d3.max(this.data, function (c) {
        return d3.max(c["values"], function (d) {
          return d["money"];
        });
      })
    ]);

    this.z.domain(dates.map(function (c: any) {
      return c["id"];
    }));

    // Define Axes
    var xAxis;
    if (this.xAxisPosition === "bottom") {
      xAxis = d3.axisBottom(this.x).ticks(this.xAxisTicks, d3.timeFormat("%Y-%m-%d"));
    } else {
      xAxis = d3.axisTop(this.x).ticks(this.xAxisTicks, d3.timeFormat("%Y-%m-%d"));
    }

    var yAxis;
    if (this.yAxisPosition === "left") {
      yAxis = d3.axisLeft(this.y).ticks(this.yAxisTicks);
    } else {
      yAxis = d3.axisRight(this.y).ticks(this.yAxisTicks);
    }

    var legendRectSize = 18;
    var legendSpacing = 4;

    var color = d3.scaleOrdinal(d3.schemeCategory20b);

    var color_hash = {
      0: ["apple", "green"],
      1: ["mango", "orange"],
      2: ["cherry", "red"]
    }

    this.g = this.svg
    //Since we need a responsive design, we will not set the width and height attr on g
    //.attr("width", this.width + margin.left + margin.right)
    //.attr("height", this.height + margin.top + margin.bottom)
      .append("g")
      .classed("widget-svg-content-responsive", true)
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Add the x-axis.
    if (this.xAxisPosition === "bottom") {
      this.g.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + (this.height - (margin.bottom - margin.top) ) + ")")
        .call(xAxis);
    } else {
      this.g.append("g")
        .attr("class", "x axis")
        .call(xAxis);
    }

    // Add the y-axis.
    if (this.yAxisPosition === "left") {
      this.g.append("g")
        .attr("class", "y axis")
        .call(yAxis);
    } else {
      this.g.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + (this.width - margin.left - margin.right) + ", 0)")
        .call(yAxis);
    }

    let city = this.g.selectAll(".city")
      .data(this.data)
      .enter().append("g")
      .attr("class", "city");

    city.append("path")
      .attr("class", "line")
      .attr("d", (d: any) => this.line(d["values"]))
      .style("stroke", (d: any) => this.z(d["id"]))
      .on("mouseover", (d: any, i: any) => {

        tooltip.transition()
          .duration(200)
          .style("opacity", 0.9);
      })
      .on("mouseout", function() {

        tooltip.transition()
          .duration(200)
          .style("opacity", 0.0);
      });

    city.append("text")
      .datum(function (d: any) {
        return {id: d.id, value: d.values[d.values.length - 1]};
      })
      .attr("transform", (d: any) => "translate(" + this.x(d["value"]["date"]) + "," + this.y(d["value"]["money"]) + ")")
      .attr("x", 3)
      .attr("dy", "0.35em")
      .style("font", "10px sans-serif")
      .text(function (d: any) {
        return d.id;
      });

    var legend = this.g.selectAll("g")
      .data(this.data)
      .enter()
      .append("g")
      .attr("class", "legend");

    legend.append("rect")
      .attr("x", this.width - 20)
      .attr("y", function(d: any, i: any) {
        return i * 20;
      })
      .attr("width", 10)
      .attr("height", 10)
      .style("fill", function(d: any) {
        return color(d.name);
      });

    legend.append("text")
      .attr("x", this.width - 8)
      .attr("y", function(d:any, i: any) {
        return (i * 20) + 9;
      })
      .text(function(d: any) {
        return d.id;
      });

    this.rendered.next(true);
  }

}
