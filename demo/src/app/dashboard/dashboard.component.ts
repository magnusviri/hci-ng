/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, ViewChild} from "@angular/core";

import {AppHeaderComponent, UlNavComponent, LiNavComponent} from "@huntsman-cancer-institute/navigation";

/**
 * @since 9.0.0
 */
@Component({
  selector: "hci-dashboard-demo",
  template: `
    <hci-app-header #subHeader></hci-app-header>
    <router-outlet></router-outlet>
  `
})
export class DashboardDemoComponent {

  @HostBinding("class") classlist: string = "outlet-column";

  @ViewChild("subHeader", {static: true}) subHeader: AppHeaderComponent;

  ngOnInit() {
    this.subHeader.setConfig({
      id: "dashboard-sub-header",
      navbarClasses: "sub-header",
      children: [
        {
          type: UlNavComponent, ulClass: "nav-container",
          children: [
            {type: LiNavComponent, liClass: "nav-item", title: "Home", route: "home"},
            {type: LiNavComponent, liClass: "nav-item", title: "Simple", route: "simple"},
            {type: LiNavComponent, liClass: "nav-item", title: "Empty", route: "empty"},
            {type: LiNavComponent, liClass: "nav-item", title: "Many Widgets", route: "many-widgets"},
            {type: LiNavComponent, liClass: "nav-item", title: "Multi Dashboard", route: "multi-dashboard"},
            {type: LiNavComponent, liClass: "nav-item", title: "Complex Widgets", route: "complex-widgets"}
          ]
        }
      ]
    });
  }

}
