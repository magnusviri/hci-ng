/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ColorPickerModule, ColorPickerService} from "ngx-color-picker";
import {DragulaModule} from "ng2-dragula";
import {CoolLocalStorage, CoolStorageModule} from "@angular-cool/storage";

import {ATTRIBUTE_ENDPOINT, AttributeService} from "@huntsman-cancer-institute/cod";
import {DashboardGlobalService} from "@huntsman-cancer-institute/dashboard";
import {DictionaryServiceModule, DICTIONARY_ENDPOINT} from "@huntsman-cancer-institute/dictionary-service";
import {CrudModule} from "@huntsman-cancer-institute/crud";
import {NavigationModule} from "@huntsman-cancer-institute/navigation";
import {AUTHENTICATION_SERVER_URL, AUTHENTICATION_DIRECT_ENDPOINT, AUTHENTICATION_TOKEN_KEY, AUTHENTICATION_ROUTE,
  AUTHENTICATION_LOGOUT_PATH, AUTHENTICATION_TOKEN_ENDPOINT, AuthenticationModule} from "@huntsman-cancer-institute/authentication";
import {AUTHENTICATED_USER_ENDPOINT} from "@huntsman-cancer-institute/user";
import {NotificationModule} from "@huntsman-cancer-institute/notification";

import {DemoComponent} from "./demo.component";
import {ROUTES} from "./demo.routes";
import {InputInterceptor} from "./input/input.interceptor";
import {DictionaryInterceptor} from "./dictionary/dictionary.interceptor";
import {AuthInterceptor} from "./auth/auth.interceptor";
import {CrudInterceptor} from "./crud/crud.interceptor";
import {HelpInterceptor} from "./help/help.interceptor";
import {DashboardInterceptor} from "./dashboard/dashboard.interceptor";
import {DocumentationComponent} from "./documentation/documentation.component";
import {HomeComponent} from "./home/home.component";
import {CodInterceptor} from "./cod/cod.interceptor";
import {NavigationInterceptor} from "./navigation/navigation.interceptor";
import {ReportingFrameworkInterceptor} from "./reporting/reporting-framework.interceptor";
import {HelpDemoModule} from "./help/help.module";

/**
 * @since 1.0.0
 */
@NgModule({
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {useHash: true}),
    NgbModule,
    DragulaModule.forRoot(),
    ColorPickerModule,
    CoolStorageModule,
    AuthenticationModule.forRoot(),
    CrudModule.forRoot({
      crudEndpoint: "/api/crud/"
    }),
    DictionaryServiceModule.forRoot({
      dictionaryEndpoint: "/api/dictionaries/"
    }),
    NavigationModule.forRoot(),
    NotificationModule.forRoot({
      popupRemoveDelay: 5000
    })
  ],
  providers: [
    {provide: AUTHENTICATION_TOKEN_ENDPOINT, useValue: "https://localhost/auth/api/token"},
    {provide: AUTHENTICATION_SERVER_URL, useValue: "https://localhost/"},
    {provide: AUTHENTICATION_DIRECT_ENDPOINT, useValue: "https://localhost/login"},
    {provide: AUTHENTICATION_LOGOUT_PATH, useValue: "https://localhost/auth/logout"},
    {provide: AUTHENTICATION_ROUTE, useValue: "https://localhost/authenticate"},
    {provide: AUTHENTICATION_TOKEN_KEY, useValue: "core-jwt"},
    {provide: DICTIONARY_ENDPOINT, useValue: "/api/dictionaries/"},
    {provide: AUTHENTICATION_TOKEN_KEY, useValue: "jwt"},
    {provide: AUTHENTICATION_ROUTE, useValue: "/auth"},
    {provide: AUTHENTICATION_LOGOUT_PATH, useValue: "/auth"},
    {provide: AUTHENTICATED_USER_ENDPOINT, useValue: "/auth"},
    {provide: ATTRIBUTE_ENDPOINT, useValue: "/api/attr/"},
    {provide: HTTP_INTERCEPTORS, useClass: ReportingFrameworkInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: DashboardInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: CodInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: InputInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: DictionaryInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: CrudInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: HelpInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: NavigationInterceptor, multi: true},
    DashboardGlobalService,
    ColorPickerService,
    CoolLocalStorage,
    AttributeService
  ],
  declarations: [
    DemoComponent,
    HomeComponent,
    DocumentationComponent
  ],
  bootstrap: [
    DemoComponent
  ]
})
export class DemoModule {}
