/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, HostBinding, ViewChild} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {CoolLocalStorage} from "@angular-cool/storage";

import {AuthenticationService} from "@huntsman-cancer-institute/authentication";
import {AppHeaderComponent, AppFooterComponent, UlNavComponent, LiNavComponent, LiDdNavComponent} from "@huntsman-cancer-institute/navigation";

import {JwtUtil} from "./auth/jwt";

/**
 * The top level demo application component.
 *
 * @since 1.0.0
 */
@Component({
  selector: "demo",
  template: `
    <hci-notification-popup></hci-notification-popup>
    <hci-app-header></hci-app-header>
    <main class="outlet-column">
      <router-outlet></router-outlet>
    </main>
    <hci-app-footer id="core-footer"></hci-app-footer>
  `
})
export class DemoComponent {

  @HostBinding("class") classList: String = "app-root risr-material";

  @ViewChild(AppHeaderComponent, {static: true}) header: AppHeaderComponent;
  @ViewChild(AppFooterComponent, {static: true}) footer: AppFooterComponent;

  version: string = "11.x";

  constructor(private authenticationService: AuthenticationService, private http: HttpClient, private localStorageService: CoolLocalStorage) {}

  ngOnInit() {
    new JwtUtil(this.authenticationService, this.http, this.localStorageService).authenticate();

    this.header.setConfig({
      id: "header",
      children: [
        {
          type: UlNavComponent, rootClass: "flex-grow-1", ulClass: "nav-container",
          children: [
            {type: LiNavComponent, iClass: "hci fa-core fa-2x", aClass: "nav-link", route: "/home"},
            {type: LiDdNavComponent, liClass: "nav-container", iClass: "fas fa-bars fa-lg", aClass: "nav-link no-caret", ulClass: "dropdown-menu",
              children: [
                {type: LiNavComponent, title: "Dictionary Editor", aClass: "dropdown-item", route: "/dictionary-editor"},
                {type: LiNavComponent, title: "Configure on Demand", aClass: "dropdown-item", route: "/cod"},
                {type: LiNavComponent, title: "Configure on Demand Editor", aClass: "dropdown-item", route: "/cod-editor"},
                {type: LiNavComponent, title: "CRUD", aClass: "dropdown-item", route: "/crud"},
                {type: LiNavComponent, title: "Dashboard", aClass: "dropdown-item", route: "/dashboard"},
                {type: LiNavComponent, title: "Help", aClass: "dropdown-item", route: "/help"},
                {type: LiNavComponent, title: "Icon", aClass: "dropdown-item", route: "/icon"},
                {type: LiNavComponent, title: "Input", aClass: "dropdown-item", route: "/input"},
                {type: LiNavComponent, title: "Misc", aClass: "dropdown-item", route: "/misc"},
                {type: LiNavComponent, title: "Navigation and Layout", aClass: "dropdown-item", route: "/navigation"},
                {type: LiNavComponent, title: "Notification", aClass: "dropdown-item", route: "/notification"},
                {type: LiNavComponent, title: "Reporting Framework", aClass: "dropdown-item", route: "/reporting-framework"}
              ]
            },
            {type: LiNavComponent, title: "HCI Angular Demo", rootClass: "d-flex flex-grow-1 font-lg", liClass: "ml-auto mr-auto", aClass: "nav-link"}
          ]
        },
        {
          type: UlNavComponent, id: "header-right-container", container: "rightContainer", ulClass: "navbar-nav", children: [
            {type: LiNavComponent, iClass: "fas fa-file-alt fa-2x", aClass: "nav-link", route: "/documentation"}
          ]
        }
      ]
    });

    this.footer.setConfig({
      id: "footer",
      children: [
        {type: UlNavComponent, ulClass: "nav-container",
          children: [
            {type: LiNavComponent, id: "version", title: "version " + this.version, aClass: ""},
            {type: LiNavComponent, title: "©2018 Huntsman Cancer Institute", aClass: ""}
          ]
        }
      ]
    });
  }
}
