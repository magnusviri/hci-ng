import {Injectable, isDevMode} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";

import {Observable, of, throwError} from "rxjs";
import {catchError, delay, mergeMap} from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isDevMode()) {
      console.debug("AuthInterceptor.intercept: " + request.url);
    }

    let delayTime: number = Math.floor(Math.random() * 500 + 250);

    return of(null)
      .pipe(mergeMap(() => {
        if (request.url === "https://localhost/login") {
          if (isDevMode()) {
            console.info("GET https://localhost/login");
          }
          return of(new HttpResponse<any>({ status: 201, body: {} }));
        } else {
          delayTime = 0;
        }

        return next.handle(request)
          .pipe(catchError(response => {
            if (response instanceof HttpErrorResponse) {
              console.warn("Http error", response);
            }

            return throwError(response);
          }));
      }))
      .pipe(delay(delayTime));
  }

}
