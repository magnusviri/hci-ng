/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import "@huntsman-cancer-institute/style/assets/fonts/fontawesome/js/all.min.js";

import "@huntsman-cancer-institute/icon";
