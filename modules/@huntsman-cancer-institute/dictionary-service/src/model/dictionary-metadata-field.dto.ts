export interface DictionaryMetadataField {
  name: string;
  type: string;
  protectionType: string;
  displayName: string;
  description: string;
  displayOrder: number;
  filterDictionaryClass: string;
  min: number;
  max: number;
  integer: boolean;
  fraction: boolean;
  id: boolean;
  code: boolean;
  notNull: boolean;
  dictionaryTooltip: boolean;
  dictionaryDisplay: boolean;
  filterable: boolean;
}
