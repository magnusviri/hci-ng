export interface DropdownEntry {
  id: number;
  display: string;
}
