# Dictionary Service

Provides the singleton service for an application and its components to fetch dictionaries.
