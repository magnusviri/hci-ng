/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the notification package.
 *
 * @since 1.0.0
 */
export {NotificationModule} from "./src/notification.module";
export {NotificationPopupComponent} from "./src/components/notification-popup.component";
export {NotificationItemComponent} from "./src/components/notification-item.component";
export {NotificationDropdown} from "./src/components/notification-dropdown.component";
export {NotificationService, NOTIFICATION_CONFIG} from "./src/notification.service";
export {Notification} from "./src/model/notification.entity";

