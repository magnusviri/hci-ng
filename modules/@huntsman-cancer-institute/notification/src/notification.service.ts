import {Inject, Injectable, InjectionToken, isDevMode} from "@angular/core";

import {Subject} from "rxjs";

import {Notification} from "./model/notification.entity";

export let NOTIFICATION_CONFIG = new InjectionToken<any>("notificationConfig");

@Injectable()
export class NotificationService {

  id: number = 0;

  public popupRemoveDelay: number = 5000;

  private notifications: Notification[] = [];
  private notificationsSubject: Subject<Notification[]> = new Subject<Notification[]>();

  constructor(@Inject(NOTIFICATION_CONFIG) private notificationConfig: any) {
    this.setConfig(this.notificationConfig);
  }

  getNotificationsSubject(): Subject<Notification[]> {
    return this.notificationsSubject;
  }

  addNotification(notification: Notification) {
    if (isDevMode()) {
      console.debug("addNotification");
    }
    if (!notification.idNotification) {
      notification.idNotification = this.id++;
    }
    if (!notification.type) {
      notification.type = "INFO";
    }

    this.notifications.push(notification);
    this.notificationsSubject.next(this.notifications);
  }

  hide(notification: Notification) {
    let i: number;
    for (i = 0; i < this.notifications.length; i++) {
      if (this.notifications[i].idNotification === notification.idNotification) {
        break;
      }
    }
    if (this.notifications[i].status !== "NEW") {
      return;
    }
    if (notification.type === "INFO") {
      notification.status = "Read";
    } else {
      notification.status = "Unread";
    }
    this.notifications[i] = notification;
    console.debug(notification);
    this.notificationsSubject.next(this.notifications);
  }

  acknowledge(notification: Notification) {
    notification.status = "Read";
    this.notificationsSubject.next(this.notifications);
  }

  discard(notification: Notification) {
    notification.status = "Closed";
    let i: number;
    for (i = 0; i < this.notifications.length; i++) {
      if (this.notifications[i].idNotification === notification.idNotification) {
        break;
      }
    }
    this.notifications.splice(i, 1);
    this.notificationsSubject.next(this.notifications);
  }

  clearAll() {
    this.notifications = [];
    this.notificationsSubject.next(this.notifications);
  }

  clearRead() {
    this.notifications = this.notifications.filter((o: Notification) => {
      return o.status !== "Read";
    });
    this.notificationsSubject.next(this.notifications);
  }

  clearUnread() {
    this.notifications = this.notifications.filter((o: Notification) => {
      return o.status !== "Unread";
    });
    this.notificationsSubject.next(this.notifications);
  }

  setConfig(config: any) {
    if (config.popupRemoveDelay) {
      this.popupRemoveDelay = config.popupRemoveDelay;
    }
  }

}
