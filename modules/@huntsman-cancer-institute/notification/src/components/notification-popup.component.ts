/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import { Component, OnInit, OnDestroy } from "@angular/core";
import {animate, query, stagger, state, style, transition, trigger} from "@angular/animations";

import { Subscription } from "rxjs";

import {NotificationService} from "../notification.service";
import {Notification} from "../model/notification.entity";

@Component({
  selector: "hci-notification-popup",
  template: `
    <div class="notification-popup" (click)="$event.stopPropagation()">
      <ng-container *ngFor="let notification of notifications | notificationFilter:'NEW'">
        <hci-notification-item [notification]="notification" [popup]="true"></hci-notification-item>
      </ng-container>
    </div>
  `,
  animations: [
    trigger("notificationInOut", [
      transition("* => *", [
        query(".notification-popup-item:enter", [
          animate(1000, style({ opacity: 1 }))
        ], {optional: true}),
        query(".notification-popup-item:leave", [
          animate(1000, style({ opacity: 0 }))
        ], {optional: true})
      ])
    ])
  ]
})
export class NotificationPopupComponent implements OnInit, OnDestroy {

  notifications: Notification[] = [];

  notificationsSubscription: Subscription;

  constructor(private notificationService: NotificationService) {}

  ngOnInit(): void {
    this.notificationsSubscription = this.notificationService.getNotificationsSubject().subscribe((notifications: Notification[]) => {
      this.notifications = notifications;
    });
  }

  ngOnDestroy(): void {
    this.notificationsSubscription.unsubscribe();
  }
}
