import {Component} from "@angular/core";
import {Param} from "../model/param.model";


@Component({
    selector: "hci-parameter",
    template: `

      <!--Text Input-->
      <ng-container *ngIf="param.type === 'input'">
        <div style="display: block;">
          <div class="row">
            <label class="data-label">{{param.caption}}:</label>
            <input type="text" class="form-control input-field"
                   [id]="param.name"
                   [name]="param.name"
                   [(ngModel)]="param.selectedValue"
            >
          </div>
        </div>
      </ng-container>

      <!--Date-->
      <ng-container *ngIf="param.type === 'date'">
        <div style="display: block;">
          <div class="row">
            <label class="data-label">{{param.caption}}:</label>
            <report-datepicker class="date-input"
                               placeholder=""
                               id="{{param.name}}"
                               [(value)]="param.selectedValue"
            >
            </report-datepicker>
          </div>
        </div>
      </ng-container>

      <!--Select with dictionary-->
      <ng-container *ngIf="param.type === 'select' && param.className && param.className != ''">
        <div style="display: block;">
          <div class="row">
            <label class="data-label">{{param.caption}}:</label>
            <hci-native-select class="report-select"
                               [id]="param.name"
                               [name]="param.name"
                               [idKey]="param.valueXPath"
                               [displayKey]="param.displayXPath"
                               [sortKey]="param.sortField"
                               [sortNumeric]="param.sortNumeric"
                               [(ngModel)]="param.selectedValue"
                               [url]="param.calculateDictionaryURL()"
            ></hci-native-select>
          </div>
        </div>
      </ng-container>

      <!--Select with manual options-->
      <ng-container *ngIf="param.type === 'select' && (!param.className || param.className === '')">
        <div style="display: block;">
          <div class="row">
            <label class="data-label">{{param.caption}}:</label>
            <hci-native-select class="report-select"
                               [id]="param.name"
                               [name]="param.name"
                               [(ngModel)]="param.selectedValue"
                               [sortKey]="param.sortField"
                               [sortNumeric]="param.sortNumeric"
                               [entries] = "entries"
            ></hci-native-select>
          </div>
        </div>
      </ng-container>

      <!--Multi-select with dictionary-->
      <ng-container *ngIf="param.type === 'multiselect' && param.className && param.className != ''">
        <div style="display: block;">
          <div class="row">
            <label class="data-label">{{param.caption}}:</label>
            <hci-md-multi-select class="multi-report-select"
              [id]="param.name"
              [name]="param.name"
              [idKey]="param.valueXPath"
              [displayKey]="param.displayXPath"
              [sortKey]="param.sortField"
              [sortNumeric]="param.sortNumeric"
              [(ngModel)]="param.selectedValueList"
              [url]="param.calculateDictionaryURL()"
            >
            </hci-md-multi-select>
          </div>
        </div>
      </ng-container>

      <!--Multi-select with manual options-->
      <ng-container *ngIf="param.type === 'multiselect' && (!param.className || param.className === '')">
        <div style="display: block;">
          <div class="row">
            <label class="data-label">{{param.caption}}:</label>
            <hci-md-multi-select class="multi-report-select"
              [id]="param.name"
              [name]="param.name"
              [entries] = "entries"
              [sortKey]="param.sortField"
              [sortNumeric]="param.sortNumeric"
              [(ngModel)]="param.selectedValueList"
            >
            </hci-md-multi-select>
          </div>
        </div>
      </ng-container>

      <!--Radio buttons-->
      <ng-container *ngIf="param.type === 'radio'">
        <div style="display: block;">
          <div class="row">
            <label class="data-label">{{param.caption}}:</label>
            <div class="radio-pair" *ngFor="let o of param.options">
              <input type="radio"
                     style="margin-right: 3px;"
                     [value]="o.value"
                     [id]="param.name + '-' + o.value"
                     [(ngModel)]="param.selectedValue"
                     [name]="param.name">
              <label for="{{param.name + '-' + o.value}}">{{o.display}}</label>
            </div>
          </div>
        </div>
      </ng-container>

    `,
    styles: [`

      .data-label{
        width: 270px;
        margin-right: 30px;
        display: flex;
        align-items: center;
        justify-content: flex-end;
      }

      .date-input{
        width: 45%;
      }

      .report-select{
        flex-grow: unset !important;
        width: 45%;
    }

    .multi-report-select{
        flex-grow: unset !important;
        width: 45%;
    }

    .radio-pair{
        display: flex;
        align-items: center;
        padding-right: 5px;
        margin-right: 15px;
    }

    .input-field{
        width: 45%
    }
    `]
})
export class ParameterComponent {

    param: Param;
    entries: any[];

    setParam(p: Param) {
      this.param = p;

      if(this.param.type === "radio") { //if radio, auto select first option
        let o = this.param.options[0];
        this.param.selectedValue = o.value;
      }
    }

}
