import {Param} from "./param.model";
import {Format} from "./format.model";

export class Report {

    public name: string;
    public title: string;
    public description: string;
    public className: string;
    public fileName: string;
    public type: string;
    public tab: string;
    public datakey: string;
    public help: string;

    public params: Param[] = [];
    public formats: Format[] = [];
}
