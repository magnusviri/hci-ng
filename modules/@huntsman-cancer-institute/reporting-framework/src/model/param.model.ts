import {Option} from "./option.model";

export class Param {

    public caption: string;
    public name: string;
    public type: string;
    public className: string;
    public valueXPath: string;
    public displayXPath: string;
    public activeOnly: boolean = true;
    public displayOrder: number;
    public sortField: string;
    public sortNumeric: boolean = false;

    public options: Option[] = [];

    public selectedValue: any;
    public selectedValueList: any[] = []; //for multi-select
    public dictAPILeadIn: string;

    public calculateDictionaryURL(): string {
      var out: string = "";

      if(this.activeOnly) {
        out = this.dictAPILeadIn + "/" + this.className + "/active-entries";
      }else {
        out = this.dictAPILeadIn + "/" + this.className + "/entries";
      }
      return out;
    }
}
