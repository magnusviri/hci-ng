(function () {
  "use strict";

  const demoPath = "demo/node_modules/@hci/style";
  var cleanCSS = require("gulp-clean-css");
  const del = require("del");
  const fs = require("fs");
  var gulp = require("gulp");
  var sass = require("gulp-sass");
  var path = require("path");
  var rename = require("gulp-rename");

  gulp.task("clean", function(done)  {
    done();
    return del.sync([
      "**.js", "!gulpfile.js"
    ]);

  });

  function buildScss() {
    return gulp.src("src/**/*.scss")
      .pipe(sass())
      .pipe(cleanCSS({}))
      .pipe(rename({
        extname: ".min.css"
      }))
      .pipe(gulp.dest("./dist"));
  }

  gulp.task("build", gulp.series(buildScss), function(done) {
    console.log("Started");
    done();
  });

}());

