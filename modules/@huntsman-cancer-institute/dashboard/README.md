# Angular Dashboard

The dashboard is intended to be used as an application's landing page.  The dashboard acts
as a container for widgets which are laid out using a Bootstrap's card-group class and each
widget is wrapped as a card.  Each widget is independent and can be as simple containing route
shortcuts or pull data from a backend service and display that data via D3 charts.
