/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, EventEmitter, Input, isDevMode, OnChanges, Output, SimpleChange, ViewEncapsulation} from "@angular/core";

import {Subscription} from "rxjs";

import {WidgetAttributeValue, WidgetAttribute, WidgetAttributeChoice, WidgetInstance} from "../model/model";
import {DashboardService} from "../services/dashboard.service";
import {WidgetComponent} from "../widget/widget.component";

/**
 * @since 1.0.0
 */
@Component({
  selector: "widget-attribute",
  template: `
    <ng-container *ngIf="init && attributeValues?.length > 0">
      <div *ngIf="attribute.dataType === 'string'" class="form-group">
        <label for="attr-string" class="bold">{{attribute.name}}</label>
        <input id="attr-string"
               class="form-control"
               [ngModel]="attributeValues[0].valueString"
               (keypress)="onChangeString($event)" />
      </div>
      <div *ngIf="attribute.dataType === 'integer'" class="form-group">
        <label for="attr-integer" class="bold">{{attribute.name}}</label>
        <input id="attr-integer"
               class="form-control"
               [ngModel]="attributeValues[0].valueInteger"
               (keypress)="onChangeInteger($event)" />
      </div>
      <div *ngIf="attribute.dataType === 'color'" class="form-group">
        <label for="attr-color" class="bold">{{attribute.display}}</label>
        <input id="attr-color"
               class="form-control"
               [colorPicker]="attributeValues[0].valueString"
               (colorPickerChange)="onChangeColor($event)"
               [style.background]="attributeValues[0].valueString"
               [cpPositionRelativeToArrow]="true"
               [cpCancelButton]="true"
               [cpOKButtonClass]="'btn btn-primary btn-xs'"
               [cpPosition]="bottom"/>
      </div>
      <div *ngIf="attribute.dataType === 'choice'" class="form-group">
        <label for="attr-choice" class="bold">{{attribute.display}}</label>
        <select id="attr-choice"
                class="form-control"
                [ngModel]="attributeValues[0].valueString"
                (ngModelChange)="onChoiceChange($event)" >
          <option *ngFor="let choice of attribute.widgetAttributeChoices"
                  [ngValue]="choice.value"
                  [selected]="(attributeValues[0].valueString === choice.value) ? true : undefined">
            {{choice.display}}</option>
        </select>
      </div>
      <div *ngIf="attribute.dataType === 'checkbox'" class="form-group row">
        <label for="attr-checkbox" class="col-6 bold">{{attribute.display}}</label>
        <input id="attr-checkbox" class="col-6 form-check-input centered" type="checkbox" [checked]="attributeValues[0].valueBoolean" (change)="onCheckboxChange($event)" />
      </div>
      <div *ngIf="attribute.dataType === 'graphical-choice'" class="form-group">
        <label for="attr-choice" class="bold">{{attribute.display}}</label>
        <div class="card-group">
          <ng-template ngFor let-choice [ngForOf]="attribute.widgetAttributeChoices">
            <span class="graphical-choice"
                  [ngClass]="attributeValues[0].valueString === choice.value ? 'selected' : ''"
                  (click)="setGraphicalChoice(choice)">
              <ng-container *ngIf="choice.display">
                <img *ngIf="choice.display.indexOf('.svg') > 0"
                     [src]="choice.display" />
                <ng-container *ngIf="choice.display.indexOf('.svg') === -1">
                  <i class="{{choice.display}} fa-2x"></i>
                </ng-container>
              </ng-container>
              <ng-container *ngIf="!choice.display">
                <i class="fas fa-question-circle fa-2x"></i>
              </ng-container>
            </span>
          </ng-template>
        </div>
      </div>
      <div *ngIf="attribute.dataType === 'widget-choice'" class="form-group">
        <div ngbDropdown [autoClose]="true" class="d-inline-block">
          <button class="btn btn-outline-primary" id="attr-widget-choice" ngbDropdownToggle>{{attribute.display}}</button>
          <div ngbDropdownMenu class="dropdown-menu" aria-labelledby="attr-widget-choice">
            <ng-template ngFor let-choice [ngForOf]="attribute.widgetAttributeChoices">
              <a class="dropdown-item bold" (click)="setWidgetChoice(choice)">{{choice.display}}</a>
            </ng-template>
          </div>
        </div>
      </div>
      <div *ngIf="attribute.dataType === 'widget-multi-choice'" class="form-group">
        <div ngbDropdown [autoClose]="true" class="d-inline-block">
          <button class="btn btn-outline-primary" id="attr-widget-choice" ngbDropdownToggle>{{attribute.display}}</button>
          <div ngbDropdownMenu class="dropdown-menu" aria-labelledby="attr-widget-choice">
            <ng-template ngFor let-choice [ngForOf]="attribute.widgetAttributeChoices">
              <div>
                <label for="attr-checkbox" class="bold">{{choice.display}}</label>
                <input id="attr-checkbox"
                       class="form-control checkbox"
                       type="checkbox" [checked]="isMultiSelected(choice)" (change)="setWidgetMultiChoice(choice)" />
              </div>
            </ng-template>
          </div>
        </div>
      </div>
    </ng-container>
  `,
  styles: [`
    .color-picker {
      left: inherit !important;
      top: inherit !important;
    }
    div.color-picker {
      left: inherit !important;
      top: inherit !important;
    }
    .form-control.checkbox {
      width: inherit;
      float: left;
      margin-top: 5px;
      margin-left: 4px;
      margin-right: 4px;
    }
    .graphical-choice {
      width: 64px;
      padding: 5px;
    }
    .graphical-choice.selected {
      border: red 1px solid !important;
      border-radius: 10px !important;
    }
    label.bold {
      font-weight: bold;
    }
    .form-control.centered {
      margin-top: auto;
      margin-bottom: auto;
    }
    .form-check-input {
      height: 1rem;
    }
  `],
  encapsulation: ViewEncapsulation.None
})
export class WidgetAttributeComponent implements OnChanges {

  @Input() idWidgetAttributeValueSet: number;
  @Input() attribute: WidgetAttribute;
  @Input() widgetComponent: WidgetComponent;

  @Output() changed: EventEmitter<boolean> = new EventEmitter<boolean>();

  public init: boolean = false;
  public attributeValues: WidgetAttributeValue[] = [];

  private widgetAttributeUpdatedSubscription: Subscription;
  private widgetAttributeChoicesSubscription: Subscription;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.init = false;

    this.dashboardService.configurationWidgetInstanceSubject.subscribe((widgetInstance: WidgetInstance) => {
      if (widgetInstance) {
        this.refresh();
      }
    });

    this.widgetAttributeUpdatedSubscription = this.dashboardService.getWidgetAttributeUpdatedSubject().subscribe((idWidgetAttribute: number) => {
      this.refresh();
    });

    this.refresh();
  }

  ngOnDestroy() {
    if (this.widgetAttributeUpdatedSubscription) {
      this.widgetAttributeUpdatedSubscription.unsubscribe();
    }
    if (this.widgetAttributeChoicesSubscription) {
      this.widgetAttributeChoicesSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: {[propName: string]: SimpleChange}) {
    if (this.init && changes["attribute"]) {
      this.refresh();
    }
  }

  refresh() {
    if (isDevMode()) {
      console.debug("refresh: " + this.attribute.name);
    }

    this.attributeValues = this.dashboardService.getConfigurationAttributeValues(this.attribute.idWidgetAttribute);

    if (this.attribute.dataType === "widget-choice" || this.attribute.dataType === "widget-multi-choice") {
      this.widgetAttributeChoicesSubscription = this.widgetComponent.getWidgetChoices(this.attribute.name).subscribe((widgetAttributeChoices: WidgetAttributeChoice[]) => {
        this.attribute.widgetAttributeChoices = widgetAttributeChoices;
        this.init = true;
      });
    } else {
      this.init = true;
    }
  }

  onCheckboxChange(value: boolean) {
    // TODO: Implement
  }

  onChangeColor(color: string) {
    if (this.init && this.attributeValues[0].valueString !== color) {
      this.attributeValues[0].valueString = color;
      this.attributeValues = this.dashboardService.setConfigurationAttributeValues(this.attribute.idWidgetAttribute, this.attributeValues);
      this.updateConfigurationWidgetValues(this.attributeValues);
    }
  }

  onChoiceChange(value: string) {
    if (this.init && this.attributeValues[0].valueString !== value) {
      this.attributeValues[0].valueString = value;
      this.attributeValues = this.dashboardService.setConfigurationAttributeValues(this.attribute.idWidgetAttribute, this.attributeValues);
      this.updateConfigurationWidgetValues(this.attributeValues);
    }
  }

  onChangeString(event: KeyboardEvent) {
    if (this.init) {
      if (event.keyCode === 13 && this.attributeValues[0].valueString !== event.target["value"]) {
        this.attributeValues[0].valueString = event.target["value"];
        this.attributeValues = this.dashboardService.setConfigurationAttributeValues(this.attribute.idWidgetAttribute, this.attributeValues);
        this.updateConfigurationWidgetValues(this.attributeValues);
      }
    }
  }

  onChangeInteger(event: KeyboardEvent) {
    if (this.init) {
      if (event.keyCode === 13 && this.attributeValues[0].valueInteger !== event.target["value"]) {
        this.attributeValues[0].valueInteger = event.target["value"];
        this.attributeValues = this.dashboardService.setConfigurationAttributeValues(this.attribute.idWidgetAttribute, this.attributeValues);
        this.updateConfigurationWidgetValues(this.attributeValues);
      }
    }
  }

  setGraphicalChoice(widgetAttributeChoice: WidgetAttributeChoice) {
    if (this.init && this.attributeValues[0].valueString !== widgetAttributeChoice.value) {
      this.attributeValues[0].valueString = widgetAttributeChoice.value;
      this.attributeValues = this.dashboardService.setConfigurationAttributeValues(this.attribute.idWidgetAttribute, this.attributeValues);
      this.updateConfigurationWidgetValues(this.attributeValues);
    }
  }

  isMultiSelected(widgetAttributeChoice: WidgetAttributeChoice) {
    for (var i = 0; i < this.attributeValues.length; i++) {
      if (this.attributeValues[i].valueString === widgetAttributeChoice.value) {
        return true;
      }
    }
    return false;
  }

  setWidgetChoice(widgetAttributeChoice: WidgetAttributeChoice) {
    if (isDevMode()) {
      console.debug("setWidgetChoice: " + widgetAttributeChoice.idWidgetAttributeChoice);
    }

    if (this.init && this.attributeValues[0].valueString !== widgetAttributeChoice.value) {
      this.attributeValues[0].valueString = widgetAttributeChoice.value;
      this.attributeValues = this.dashboardService.setConfigurationAttributeValues(this.attribute.idWidgetAttribute, this.attributeValues);
      this.updateConfigurationWidgetValues(this.attributeValues);
      this.changed.emit(true);

      this.dashboardService.setWidgetAttributeUpdatedSubject(this.attribute.idWidgetAttribute);
    }
  }

  setWidgetMultiChoice(widgetAttributeChoice: WidgetAttributeChoice) {
    if (this.init) {
      let exists: boolean = false;
      for (var i = 0; i < this.attributeValues.length; i++) {
        if (this.attributeValues[i].valueString === widgetAttributeChoice.value) {
          exists = true;
          this.attributeValues.splice(i, 1);
          break;
        }
      }
      if (!exists) {
        let value: WidgetAttributeValue = new WidgetAttributeValue();
        value.idWidgetAttributeValue = this.dashboardService.getIdNew();
        value.idWidgetAttribute = this.attribute.idWidgetAttribute;
        value.valueString = widgetAttributeChoice.value;
        this.attributeValues.push(value);
      }

      this.attributeValues = this.dashboardService.setConfigurationAttributeValues(this.attribute.idWidgetAttribute, this.attributeValues);
      this.updateConfigurationWidgetValues(this.attributeValues);
      this.changed.emit(true);
    }
  }

  updateConfigurationWidgetValues(attributeValues: WidgetAttributeValue[]) {
    if (isDevMode()) {
      console.debug("updateConfigurationWidgetValues");
    }

    // If setValues returns true, re-config the widget.
    if (this.widgetComponent.setValues(this.attribute.name, attributeValues)) {
      this.widgetComponent.config(this.dashboardService.getConfigWidgetComponent().widgetInstance.attributeValueSet.attributeValues);
    }
  }
}
