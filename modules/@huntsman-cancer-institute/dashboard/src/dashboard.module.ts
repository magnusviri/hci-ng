/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {ANALYZE_FOR_ENTRY_COMPONENTS, ModuleWithProviders, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {ColorPickerModule, ColorPickerService} from "ngx-color-picker";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {DragulaModule, DragulaService} from "ng2-dragula";

import {DashboardComponent} from "./dashboard.component";
import {WidgetAttributeComponent} from "./configure/widget-attribute.component";
import {WidgetConfigureComponent} from "./configure/widget-configure.component";
import {WidgetAddComponent} from "./configure/widget-add.component";
import {WidgetContainerComponent} from "./widget/widget-container.component";
import {WidgetHostComponent} from "./widget/widget-host.component";
import {WidgetComponent} from "./widget/widget.component";
import {HasEditableAttributesPipe} from "./pipes/has-editable-attributes.pipe";

/**
 * A feature module for the dashboard layout and dashboard widgets.
 *
 * @since 1.0.0
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ColorPickerModule,
    DragulaModule,
    NgbModule
  ],
  declarations: [
    DashboardComponent,
    WidgetContainerComponent,
    WidgetHostComponent,
    WidgetComponent,
    WidgetAddComponent,
    WidgetConfigureComponent,
    WidgetAttributeComponent,
    HasEditableAttributesPipe
  ],
  providers: [
    ColorPickerService
  ],
  exports: [
    DashboardComponent,
    WidgetContainerComponent,
    WidgetHostComponent,
    WidgetComponent,
    WidgetAddComponent,
    WidgetConfigureComponent,
    WidgetAttributeComponent
  ]
})
export class DashboardModule {

  static withComponents(components: any[]): ModuleWithProviders<DashboardModule> {
    return {
      ngModule: DashboardModule,
      providers: [
        {
          provide: ANALYZE_FOR_ENTRY_COMPONENTS,
          useValue: components,
          multi: true
        }
      ]
    };
  }
}
