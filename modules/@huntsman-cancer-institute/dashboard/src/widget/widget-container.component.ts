/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, ElementRef, Input, HostListener, HostBinding, isDevMode} from "@angular/core";

import {DragulaService} from "ng2-dragula";

import {Dashboard, WidgetInstance} from "../model/model";
import {DashboardService} from "../services/dashboard.service";

/**
 * Dashboard (within a card-group) iterates over configured widgets to produce each one in a card.  The widgets can
 * always get access to any configuration held by the dashboard service, but they are intended to be largely
 * independent.
 *
 * @since 1.0.0
 */
@Component({
  selector: "widget-container",
  template: `
    <div [id]="'widget-container-' + dashboardIndex"
         [dragula]="'widget-container-' + dashboardIndex"
         class="d-flex flex-grow-1 flex-wrap y-auto p-3 pb-5">
      <ng-container *ngIf="dashboard?.widgetInstances?.length === 0">
        <div class="card one-third">
          <div class="widget-host card-padding">
            <div style="text-align: center; height: 200px; display: inline-grid; width: 100%;">
              <button class="btn btn-secondary"
                      style="margin: auto;"
                      (click)="addWidget()">
                <i class="fas fa-plus fa-4x"></i>
                <br />
                <h4>Add Widget</h4>
              </button>
            </div>
          </div>
        </div>
      </ng-container>
      
      <ng-template ngFor let-widgetInstance [ngForOf]="dashboard.widgetInstances" let-widgetIndex="index">
        <widget-host [id]="widgetInstance.idWidgetInstance"
                     [widgetInstance]="widgetInstance"
                     class="card"></widget-host>
      </ng-template>
    </div>
  `,
  styles: [`
    .gu-mirror{position:fixed!important;margin:0!important;z-index:9999!important;opacity:.8;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";filter:alpha(opacity=80)}.gu-hide{display:none!important}.gu-unselectable{-webkit-user-select:none!important;-moz-user-select:none!important;-ms-user-select:none!important;user-select:none!important}.gu-transit{opacity:.2;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=20)";filter:alpha(opacity=20)}
  `]
})
export class WidgetContainerComponent {

  @HostBinding("class") classList: string = "outlet-row";

  @Input()
  public dashboard: Dashboard;

  @Input()
  public dashboardIndex: number;

  public editMode: boolean = false;

  configMode: boolean = false;
  widgetContainerWidth: number = 0;
  widgetContainerHeight: number = 0;

  constructor(private elementRef: ElementRef, private dashboardService: DashboardService, private dragulaService: DragulaService) {}

  ngOnInit() {
    this.dashboardService.getEditMode().subscribe((editMode: boolean) => {
      this.editMode = editMode;

      // Upon switch editMode, destroy or re-add dragula to container.
      var bag = this.dragulaService.find("widget-container-" + this.dashboardIndex);
      if (bag !== undefined) {
        this.dragulaService.destroy("widget-container-" + this.dashboardIndex);
      }
      this.dragulaService.createGroup("widget-container-" + this.dashboardIndex, {
        moves: function (el: any, container: any, handle: any) {
          return editMode;
        }
      });
      this.dragulaService.find("widget-container-" + this.dashboardIndex)
        .drake
        .containers
        .push(this.elementRef.nativeElement.querySelector("#widget-container-" + this.dashboardIndex));

      /**
       * Upon drop, persist the new sortOrder to the backend.
       */
      this.dragulaService.drop("widget-container-" + this.dashboardIndex).subscribe((value: any) => {
        let n: number = 1;

        for (let el of this.elementRef.nativeElement.querySelectorAll("widget-host")) {
          this.dashboardService.saveWidgetSortOrder(this.dashboardIndex, +el.getAttribute("id"), n++);
        }
      });
    });

    this.dashboardService.configurationWidgetInstanceSubject.subscribe((widgetInstance: WidgetInstance) => {
      if (widgetInstance) {
        this.updateSize();
        this.configMode = true;
      } else {
        this.configMode = false;
      }
    });
  }

  @HostListener("window:resize", ["$event"])
  onResize(event: Event) {
    this.updateSize();
  }

  updateSize() {
    this.widgetContainerWidth = this.elementRef.nativeElement.offsetWidth;
    this.widgetContainerHeight = this.elementRef.nativeElement.offsetHeight;
  }

  addWidget() {
    this.dashboardService.addWidgetSubject.next(true);
  }

}
