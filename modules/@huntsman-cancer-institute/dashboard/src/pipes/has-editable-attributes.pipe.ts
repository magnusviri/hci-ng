import {Pipe} from "@angular/core";

import {WidgetAttributeContainer} from "../model/widget-attribute-container.entity";

@Pipe({
  name: "hasEditableAttributes",
  pure: true
})
export class HasEditableAttributesPipe {

  transform(list: WidgetAttributeContainer[]): any {
    return list.filter((wac: WidgetAttributeContainer) => {
      if (wac.attributes && wac.attributes.length > 0) {
        for (let attribute of wac.attributes) {
          if (attribute.userEditable) {
            return true;
          }
        }
      }

      return false;
    });
  }
}
