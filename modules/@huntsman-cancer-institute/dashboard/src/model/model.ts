/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for dashboard models.
 *
 * @since 3.0.0
 */
export {Dashboard} from "./dashboard.entity";
export {UserProfile} from "./user-profile.entity";
export {Widget} from "./widget.entity";
export {WidgetAttribute} from "./widget-attribute.entity";
export {WidgetAttributeChoice} from "./widget-attribute-choice.entity";
export {WidgetAttributeContainer} from "./widget-attribute-container.entity";
export {WidgetAttributeValue} from "./widget-attribute-value.entity";
export {WidgetAttributeValueSet} from "./widget-attribute-value-set.entity";
export {WidgetCategory} from "./widget-category.entity";
export {WidgetInstance} from "./widget-instance.entity";
