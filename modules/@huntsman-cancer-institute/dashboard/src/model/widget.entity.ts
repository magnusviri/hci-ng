import {WidgetAttributeContainer} from "./widget-attribute-container.entity";
import {WidgetAttributeValueSet} from "./widget-attribute-value-set.entity";
import {WidgetCategory} from "./widget-category.entity";

export class Widget {
  public idWidget: number;
  public instantiable: boolean;
  public parentWidget: Widget;
  public name: string;
  public component: string;
  public title: string;
  public icon: string;
  public description: string;
  public keywords: string;
  public widgetCategories: WidgetCategory[];
  public attributeContainers: WidgetAttributeContainer[];
  public attributeContainerMap: Map<string, WidgetAttributeContainer> = new Map<string, WidgetAttributeContainer>();
  public attributeValueSet: WidgetAttributeValueSet;

  static merge(attributeContainerMapParent: Map<string, WidgetAttributeContainer>, attributeContainerMapChild: Map<string, WidgetAttributeContainer>): Map<string, WidgetAttributeContainer> {
    let attributeContainerMap: Map<string, WidgetAttributeContainer> = new Map<string, WidgetAttributeContainer>();

    if (attributeContainerMapParent.size > 0) {
      for (let attributeContainer of attributeContainerMapParent.values()) {
        attributeContainerMap.set(attributeContainer.name, attributeContainer);
      }
    }

    for (let attributeContainer of attributeContainerMapChild.values()) {
      if (attributeContainerMap.has(attributeContainer.name)) {
        attributeContainer.attributeMap = WidgetAttributeContainer.merge(attributeContainerMap.get(attributeContainer.name).attributeMap, attributeContainer.attributeMap);
      }

      attributeContainerMap.set(attributeContainer.name, attributeContainer);
    }

    for (let attributeContainer of attributeContainerMap.values()) {
      attributeContainer.attributes = Array.from(attributeContainer.attributeMap.values());
      attributeContainerMap.set(attributeContainer.name, attributeContainer);
    }

    return attributeContainerMap;
  }

  deserialize(object): Widget {
    this.idWidget = object.idWidget;
    this.instantiable = object.instantiable;
    this.name = object.name;
    this.title = object.title;
    this.component = object.component;
    this.icon = object.icon;
    this.description = object.description;
    this.keywords = object.keywords;

    this.attributeContainerMap = new Map<string, WidgetAttributeContainer>();

    if (object.parentWidget) {
      this.parentWidget = new Widget().deserialize(object.parentWidget);
    }

    if (object.widgetCategories) {
      this.widgetCategories = new WidgetCategory().deserializeArray(object.widgetCategories);
    }

    if (object.attributeContainers) {
      this.attributeContainers = [];
      for (var i = 0; i < object.attributeContainers.length; i++) {
        let attributeContainer: WidgetAttributeContainer = new WidgetAttributeContainer().deserialize(object.attributeContainers[i]);
        this.attributeContainers.push(attributeContainer);
        this.attributeContainerMap.set(attributeContainer.name, attributeContainer);
      }
    }
    if (this.parentWidget) {
      this.attributeContainerMap = Widget.merge(this.attributeContainerMap, this.parentWidget.attributeContainerMap);
    }

    if (object.attributeValueSet) {
      this.attributeValueSet = new WidgetAttributeValueSet().deserialize(object.attributeValueSet);
    }

    return this;
  }

  deserializeArray(list: Object[]): Widget[] {
    let widgets: Widget[] = [];
    for (var i = 0; i < list.length; i++) {
      widgets.push(new Widget().deserialize(list[i]));
    }
    return widgets;
  }

  /**
   * Convert the json array of containers in to a map based on the attribute container name.
   */
  generateAttributeContainerMap() {
    this.attributeContainerMap = new Map<string, WidgetAttributeContainer>();
    for (let attributeContainer of this.attributeContainers) {
      this.attributeContainerMap.set(attributeContainer.name, attributeContainer);
    }
  }

  hasAttributes() {
    if (this.attributeContainers === undefined) {
      return false;
    } else {
      for (let attributeContainer of this.attributeContainers) {
        if (attributeContainer.attributes && attributeContainer.attributes.length > 0) {
          return true;
        }
      }

      return false;
    }
  }

}
