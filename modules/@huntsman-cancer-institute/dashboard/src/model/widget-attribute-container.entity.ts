import {WidgetAttribute} from "./widget-attribute.entity";

export class WidgetAttributeContainer {
  public idWidgetAttributeContainer: number;
  public name: string;
  public sortOrder: number;
  public hidden: boolean;
  public attributes: WidgetAttribute[];
  public attributeMap: Map<string, WidgetAttribute> = new Map<string, WidgetAttribute>();

  static merge(attributeMapParent: Map<string, WidgetAttribute>, attributeMapChild: Map<string, WidgetAttribute>): Map<string, WidgetAttribute> {
    let attributeMap: Map<string, WidgetAttribute> = new Map<string, WidgetAttribute>();

    for (let attribute of attributeMapParent.values()) {
      attributeMap.set(attribute.name, attribute);
    }

    for (let attribute of attributeMapChild.values()) {
      attributeMap.set(attribute.name, attribute);
    }

    return attributeMap;
  }

  deserialize(object): WidgetAttributeContainer {
    this.idWidgetAttributeContainer = object.idWidgetAttributeContainer;
    this.name = object.name;
    this.sortOrder = object.sortOrder;
    this.hidden = object.hidden;
    this.attributeMap = new Map<string, WidgetAttribute>();

    if (object.attributes) {
      this.attributes = [];
      for (var i = 0; i < object.attributes.length; i++) {
        let attribute: WidgetAttribute = new WidgetAttribute().deserialize(object.attributes[i]);
        this.attributes.push(attribute);
        this.attributeMap.set(attribute.name, attribute);
      }
    }

    return this;
  }

  deserializeArray(list: Object[]): WidgetAttributeContainer[] {
    let attributeContainers: WidgetAttributeContainer[] = [];
    for (var i = 0; i < list.length; i++) {
      attributeContainers.push(new WidgetAttributeContainer().deserialize(list[i]));
    }
    return attributeContainers;
  }

  generateAttributeMap() {
    this.attributeMap = new Map<string, WidgetAttribute>();
    for (let attribute of this.attributes) {
      this.attributeMap.set(attribute.name, attribute);
    }
  }

}
