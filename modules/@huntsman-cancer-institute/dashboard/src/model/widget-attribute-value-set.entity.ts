import {WidgetAttributeValue} from "./widget-attribute-value.entity";

export class WidgetAttributeValueSet {
  public idWidgetAttributeValueSet: number = undefined;
  public attributeValues: WidgetAttributeValue[] = [];

  deserialize(object): WidgetAttributeValueSet {
    this.idWidgetAttributeValueSet = object.idWidgetAttributeValueSet;

    if (object.attributeValues) {
      this.attributeValues = [];
      for (var i = 0; i < object.attributeValues.length; i++) {
        this.attributeValues.push(new WidgetAttributeValue().deserialize(object.attributeValues[i]));
      }
    }

    return this;
  }

  idReset() {
    if (this.idWidgetAttributeValueSet < 0) {
      this.idWidgetAttributeValueSet = undefined;
    }

    if (this.attributeValues) {
      for (let attributeValue of this.attributeValues) {
        if (attributeValue.idWidgetAttributeValue < 0) {
          attributeValue.idWidgetAttributeValue = undefined;
        }
      }
    }
  }
}
