/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {Component, ViewContainerRef, DebugElement} from "@angular/core";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {By} from "@angular/platform-browser";

import {} from "jasmine";
import {of} from "rxjs";

import {RoleCheckUnlessNullDirective} from "./role-check-unless-null.directive";
import {UserService} from "../user.service";
import {RoleEntity} from "./role.entity";
import {UserEntity} from "../user.entity";

/**
 * A suite of test for the {@link RoleCheckUnlessNullDirective}
 *
 * @since 1.0.0
 */
describe("RoleCheckUnlessNullDirective Tests", () => {
  @Component({
    selector: "role-check-unless-null-test",
    template: `
      <div>
        <a id="bacon-link" *hciHasRoleUnlessNull="'BACON'">Bacon</a>
        <a id="coffee-link" *hciHasRoleUnlessNull="'COFFEE'">Bacon</a>
        <a id="hammer-link" *hciHasRoleUnlessNull="'HAMMER'">Hammer</a>
        <a id="null-link" *hciHasRoleUnlessNull="_nullRole">Coffee</a>
        <a id="undef-link" *hciHasRoleUnlessNull="_unDef">Coffee</a>
      </div>
    `
  })
  class TestRoleCheckUnlessNullComponent {
    _nullRole: string = null;
    _unDef: any;
  }

  let fixture: ComponentFixture<TestRoleCheckUnlessNullComponent>;
  let debugElement: DebugElement;
  let authUser: UserEntity;

  class MockUserService {
    getAuthenticatedUser() {
      return of(authUser);
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ViewContainerRef
      ],
      declarations: [RoleCheckUnlessNullDirective, TestRoleCheckUnlessNullComponent]
    });

    TestBed.overrideDirective(RoleCheckUnlessNullDirective, {
      set: {
        providers: [
          {
            provide: UserService,
            useValue: new MockUserService()
          }
        ]
      }
    });

  });

  it("Should be able to create an embedded view from the template with bacon-link, coffee-link, null-link and undef-link " +
    "links when user has roles: BACON, COFFEE  and should not include a hammer-link",
    () => {
      authUser = new UserEntity("99", "squirrelbrother", [
        new RoleEntity("BACON"),
        new RoleEntity("COFFEE")
      ]);

      fixture = TestBed.createComponent(TestRoleCheckUnlessNullComponent);
      fixture.detectChanges();
      debugElement = fixture.debugElement;

      expect(debugElement.queryAll(By.css("a")).length).toBe(4, "Expected to find four links defined.");
      let links: string[] = ["bacon-link", "coffee-link", "null-link", "undef-link"];
      links.forEach((linkId) => {
        expect(debugElement.query(By.css("#" + linkId))).toBeDefined(`Expected link with id ${linkId} to be defined`);
      });
      expect(debugElement.query(By.css("#hammer-link"))).toBeNull("Expected link with id hammer-link to be null");
    });
});
