import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { SelectRenderer } from "./select.renderer";
import {DateRenderer} from "./date.renderer";
@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
    ],
    declarations: [
		  SelectRenderer,
      DateRenderer
    ],
    exports: [
      SelectRenderer,
      DateRenderer
    ]
})
export class AgGridRendererModule { }
