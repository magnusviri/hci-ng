# Angular Navigation Library

This module provides all the basic building blocks for creating configurable navigation tools.  In particular, this library
is the basis for the @huntsman-cancer-institute/app-header and @huntsman-cancer-institute/sidebar modules.

## Usage

The project contains a set of different types of components all of which extend the NavComponent.  Any implementation
will create a component which extends NavComponent.  From this, children may be added to any number of containers within
that base component.  For example, the app-header extends NavComponent and defines a left and right container.  The sidebar
extends NavComponent and defines a single container.  Also, the base component should provide the NavigationService.  This
service keeps track of all the rendered components and allows you to reference them easily.

Some of the provided components in this library are designed to be children and do not have a container.  These include
things such as the LiNavComponent.  That is supposed to be contained within a dropdown component or unordered list.  The
LiDdNavComponent and UlNavComponent both define a single container.

Any container can be populated with any type of NavComponent, but the html has to make sense for rendering to look right.
So a ul would probably contain a set of lis.

Custom components can be created by any implementing application.  One example is a dropdown, that instead of a list of
li components, should contain a larger panel for custom searching.  On example would be the projects search in GitLab.

### SearchListComponent Configuration

    {
      type: SearchListComponent,
      itemTemplate: this.demoItemTemplate,
      id: "search-list",
      title: "Demographics",
      dataCall: (gridDto: HciGridDto) => {
        return this.http.post<HciDataDto>("/api/search-sidebar-big-data", gridDto);
      },
      pageSize: 4,
      groupPageSize: 5,
      groupCacheNumPages: 2,
      filterByAllowedFields: ["firstName", "middleName", "lastName"],
      sortByAllowedFields: ["firstName", "lastName"],
      groupingFields: ["lastName"],
      fields: [
        {field: "firstName", display: "First Name"},
        {field: "middleName", display: "Middle Name"},
        {field: "lastName", display: "Last Name"}
      ],
      onGroupClose: () => {
        this.router.navigate(["."], {relativeTo: this.route});
      }
    }
