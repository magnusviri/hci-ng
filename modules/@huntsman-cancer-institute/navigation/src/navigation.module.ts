/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {ModuleWithProviders, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {TreeModule} from "@circlon/angular-tree-component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatSelectModule} from "@angular/material/select";
import {NgbModule, NgbNavModule} from "@ng-bootstrap/ng-bootstrap";
import {MiscModule} from "@huntsman-cancer-institute/misc";
import {UlNavComponent} from "./components/ul-nav.component";
import {LiNavComponent} from "./components/li-nav.component";
import {LiDdNavComponent} from "./components/li-dd-nav.component";
import {DivNavComponent} from "./components/div-nav.component";
import {BrandNavComponent} from "./components/brand-nav.component";
import {LiExpNavComponent} from "./components/li-exp-nav.component";
import {BadgeComponent} from "./components/badge.component";
import {TemplateComponent} from "./components/template.component";
import {SearchListComponent} from "./components/search-list/search-list.component";
import {NavigationGlobalService} from "./services/navigation-global.service";
import {TopMarginDirective} from "./directives/top-margin.directive";
import {AppHeaderComponent} from "./impl/app-header.component";
import {AppFooterComponent} from "./impl/app-footer.component";
import {SidebarComponent} from "./impl/sidebar.component";
import {SearchListControllerComponent} from "./components/search-list/search-list-controller.component";
import {SearchTreeComponent} from "./components/search-tree/search-tree.component";
import {SearchTreeControllerComponent} from "./components/search-tree/search-tree-controller.component";
import { MatNativeDateModule } from "@angular/material/core";
import { MatCheckboxModule } from '@angular/material/checkbox';
import {NavComponent} from "./components/nav.component";


/**
 * The main @huntsman-cancer-institute/navigation module.  The module exports all components that an implementation might use
 * and the forRoot() provides a global service for an implementing application.
 *
 * @since 1.0.0
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    MiscModule,
    NgbModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
	MatCheckboxModule,
    MatSelectModule,
    TreeModule,
    NgbNavModule
  ],
  providers: [
    MatDatepickerModule,
  ],
  declarations: [
    NavComponent,
    AppHeaderComponent,
    AppFooterComponent,
    SidebarComponent,
    UlNavComponent,
    LiNavComponent,
    LiDdNavComponent,
    DivNavComponent,
    BrandNavComponent,
    LiExpNavComponent,
    BadgeComponent,
    TemplateComponent,
    SearchListControllerComponent,
    SearchListComponent,
    SearchTreeControllerComponent,
    SearchTreeComponent,
    TopMarginDirective
  ],
	entryComponents: [
    UlNavComponent,
    LiNavComponent,
    LiDdNavComponent,
    DivNavComponent,
    BrandNavComponent,
    LiExpNavComponent,
    BadgeComponent,
    TemplateComponent,
    SearchListControllerComponent,
    SearchListComponent,
    SearchTreeControllerComponent,
    SearchTreeComponent
	],
  exports: [
    AppHeaderComponent,
    AppFooterComponent,
    SidebarComponent,
    UlNavComponent,
    LiNavComponent,
    LiDdNavComponent,
    BrandNavComponent,
    LiExpNavComponent,
    DivNavComponent,
    BadgeComponent,
    TemplateComponent,
    SearchListControllerComponent,
    SearchListComponent,
    SearchTreeControllerComponent,
    SearchTreeComponent,
    TopMarginDirective
  ]
})
export class NavigationModule {
  static forRoot(): ModuleWithProviders<NavigationModule> {
    return {
      providers: [
        NavigationGlobalService
      ],
      ngModule: NavigationModule
    };
  }
}
