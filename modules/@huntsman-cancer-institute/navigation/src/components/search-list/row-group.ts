export class RowGroup {

  rgKey: string;
  rgRows: Object[];
  rgCount: number = 0;
  rgExpanded: boolean = false;
  rgPageMin: number;
  rgPageMax: number;

  constructor(rgKey: string, firstRow: Object, rgCount: number = 0) {
    this.rgKey = rgKey;
    this.rgRows = [firstRow];
    this.rgCount = rgCount;
  }
}
