import {
  ChangeDetectorRef, Component, ComponentFactoryResolver, ElementRef, EventEmitter, Input,
  isDevMode, Optional, Output, QueryList, Renderer2, SimpleChange, SimpleChanges, TemplateRef, ViewChild, ViewChildren
} from "@angular/core";

import {BehaviorSubject, Observable, of, Subject} from "rxjs";
import {delay, finalize} from "rxjs/operators";
import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";

//import {TreeComponent, ITreeOptions} from "angular-tree-component";

import {HciDataDto, HciFilterDto, HciGridDto, HciGroupingDto, HciPagingDto, HciSortDto} from "hci-ng-grid-dto";
import {ComponentType} from "@huntsman-cancer-institute/utils";

import {NavComponent} from "../nav.component";
import {NavigationService} from "../../services/navigation.service";
import {NavigationGlobalService} from "../../services/navigation-global.service";
import {SearchTreeControllerComponent} from "./search-tree-controller.component";
import {TreeComponent, ITreeOptions, TreeModel} from "@circlon/angular-tree-component";
import {ITreeNode} from "@circlon/angular-tree-component/lib/defs/api";


@ComponentType("SearchTreeComponent")
@Component({
  selector: "hci-search-tree",
  template: `
    <div class="d-flex scrollable treeBox">
      <div class="d-flex flex-column treeBox">
        <hci-busy [busySubjects]="loadingSubjects" parentSelector=".scrollable" [icon]="busyIcon"></hci-busy>
        <tree-root #searchTreeControl [nodes]="viewData" [options]="searchTreeOptions" (activate)="onNodeClick($event)" (updateData)="onUpdateData($event)"
                   (moveNode)="onMoveNode($event)" class="searchTree">
          <ng-template #treeNodeTemplate let-node let-index="index">
            <ng-container *ngTemplateOutlet="itemTemplate; context: {node: node}"> </ng-container>
          </ng-template>
        </tree-root>
      </div>
    </div>

    <!-- Paging footer -->
    <div *ngIf="paging.pageSize > 0" class="d-flex flex-shrink-0 paging-footer justify-content-between pl-3 pr-3" style="min-height: 1rem; align-items: center;">
      <ng-container *ngIf="!selectedRowGroup">
        <div (click)="doPageFirst()" class="d-flex mt-1 mb-1" [class.disabled]="paging.page === 0"><i class="fas fa-fast-backward fa-xs"></i></div>
        <div (click)="doPagePrevious()" class="d-flex mt-1 mb-1" [class.disabled]="paging.page === 0"><i class="fas fa-backward fa-xs"></i></div>
        <div style="font-size: 0.8rem;">{{paging.page + 1}} of {{paging.numPages}}</div>
        <div (click)="doPageNext()" class="d-flex mt-1 mb-1" [class.disabled]="paging.page === paging.numPages - 1"><i class="fas fa-forward fa-xs"></i></div>
        <div (click)="doPageLast()" class="d-flex mt-1 mb-1" [class.disabled]="paging.page === paging.numPages - 1"><i class="fas fa-fast-forward fa-xs"></i></div>
      </ng-container>
    </div>

    <ng-template #defaultTemplate let-data="node.data">
      <span>{{ data.name }}</span>
    </ng-template>
  `,
  styles: [`
    :host {
      min-height: 0px;
      display: flex;
      flex-direction: column;
      flex-grow: 1;
      flex-shrink: 1;
     }

     .treeBox {
       min-height: 0px;
       display: flex;
       flex-direction: column;
       flex-grow: 1;
       flex-shrink: 1;
     }

     .searchTree {
       height: 100%;
     }
    }
  `]
})
export class SearchTreeComponent extends NavComponent {

  @ViewChild("searchTreeControl", {static: false}) treeComponent: TreeComponent;
  @ViewChild("defaultTemplate", {read: TemplateRef, static: true}) defaultTemplate: TemplateRef<any>;

  // The data that is actually rendered.
  viewData: any[];

  // A reference to the search list controller if applicable.
  @Input() controller: SearchTreeControllerComponent;

  // An array of data.
  @Input("data") boundData: Object[];

  // A subject that represents an array of data.
  @Input("dataSubject") dataSubject: BehaviorSubject<Object[]>;

  // A function which takes in a HciGridDto and returns a HciDataDto which wraps an array of data.
  @Input("dataCall") externalDataCall: (dto: HciGridDto) => Observable<HciDataDto>;

  @Input("searchTreeOptions") searchTreeOptions: any;

  @Input() itemTemplate: TemplateRef<any>;

  // An array of subjects with booleans to control the hci-busy overlaying the search-list.
  @Input() loadingSubjects: Subject<boolean>[];

  // An array of booleans to control the hci-busy overlaying the search-list.
  @Input() loading: boolean[] = [false];

  // If clicking a row changes a route, this function specifies the route to use.
  @Input() route: (rowGroup: any, row: any) => any[];

  // The page size of row groups or rows if no grouping.
  @Input() pageSize: number;

  // A reference to a parent dropdown if applicable.
  @Input() dropdown: NgbDropdown;

  //Which icon to use for the busy spinner
  @Input() busyIcon: string = "fas fa-sync fa-spin";
  //Either an array of node ids for expanding or a string 'all' for expanding whole tree
  @Input() expandTreeNodeIDs: string | any[];

  @Input() onClick: (cmpt: SearchTreeComponent, tree: TreeComponent, event: any) => void;

  @Input() onNodeMoved: (cmpt: SearchTreeComponent, tree: TreeComponent, event: any) => void;

  @Input() onDataUpdate: (cmpt: SearchTreeComponent, tree: TreeComponent, event: any) => void;

  @Output() nodeSelected: EventEmitter<any> = new EventEmitter<any>();
  @Output() dataChange: EventEmitter<any> = new EventEmitter<any>();
  
  @Input() sortChildren: boolean = true;


  // Grid DTO info needed to build external data requests.
  sorts: HciSortDto[] = [];
  filters: HciFilterDto[] = [];
  paging: HciPagingDto = new HciPagingDto(0, -1);
  pagingSubject: Subject<HciPagingDto> = new BehaviorSubject<HciPagingDto>(this.paging);

  defaultSearchTreeOptions: ITreeOptions = {
      idField: "id",
      displayField: "name",
      childrenField: "children",
      useVirtualScroll: true,
      nodeHeight: 22
    };

  // A dummy number that increments based on selection changes.  Forces routerLinkActive to recheck.
  rowRefresh: number = 0;

  busy: boolean = false;

  lastScrollTop: number = -1;
  lastScrollPosition: number = 1;
  setScrollTop: boolean = false;

  cmpt: SearchTreeComponent = this;

  lastEvent: any;

  constructor(elementRef: ElementRef,
              renderer: Renderer2,
              resolver: ComponentFactoryResolver,
              navigationGlobalService: NavigationGlobalService,
              @Optional() navigationService: NavigationService,
              changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);

    if (!navigationService) {
      this.navigationService = new NavigationService(navigationGlobalService);
    }

    if (this.navigationService.getId() === undefined) {
      this.isRoot = true;
      this.setConfig({});
    }
  }

  /**
   * On init, listen for loading subjects and listen for page changes.
   */
  ngOnInit(): void {
    super.ngOnInit();

    if (this.loadingSubjects) {
      this.loadingSubjects[0].subscribe((busy: boolean) => {
        this.busy = busy;
      });
    }

    this.pagingSubject.subscribe((paging: HciPagingDto) => {
      this.paging = paging;
      this.setData();
    });
  }

  /**
   * Called when inputs change.
   *
   * @param {SimpleChanges} changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);

    this.refreshTree();
  }

   ngAfterViewInit(): void {
     super.ngAfterViewInit();
     if (this.treeComponent.treeModel && this.expandTreeNodeIDs ) {
      setTimeout(() => {
       let treeModel: TreeModel = this.treeComponent.treeModel;
       if(Array.isArray(this.expandTreeNodeIDs)) {
         for(let nodeId of this.expandTreeNodeIDs) {
           let node: ITreeNode = treeModel.getNodeById(nodeId);
           if(node) {
             node.expand();
           }
         }
       } else if(typeof this.expandTreeNodeIDs === "string"
         && this.expandTreeNodeIDs === "all") {
         treeModel.expandAll();
       }
      });

     }
   }


  /**
   * Set data only when the component has fully initialized and is rendered.
   */
  checkInitialized() {
    if (this.afterViewInit) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.postInit();

      if (this.dataSubject) {
        //May as well make it spin while processing, as well as loading. Otherwise it looks frozen
        if (this.loadingSubjects) {
          this.loadingSubjects[0].next(true);
        }

        this.dataSubject
          .pipe(delay(0))
          .subscribe((data: Object[]) => {
            this.boundData = data;
            this.setData();

            //Now done loading
            if (this.loadingSubjects) {
              this.loadingSubjects[0].next(false);
            }
        });
      } else {
        this.setData();
      }
    }
  }

  postInit(): void {
    if (isDevMode()) {
      console.debug("SearchTreeComponent.postInit: " + this.id);
    }

    if (this.getParent()) {
      this.controller = <SearchTreeControllerComponent>this.getParent().getComponentByType("SearchTreeControllerComponent");
    }

    if (this.controller) {
      if (isDevMode()) {
        console.debug("SearchTreeComponent: using controller");
      }

      this.controller.addSearchList(this.title);

      this.controller.filtersSubject.subscribe((filters: HciFilterDto[]) => {
        this.filters = filters;
        this.setData();
        this.resetPaging();
      });

      this.controller.sortsSubject.subscribe((sorts: HciSortDto[]) => {
        this.sorts = sorts;
        this.setData();
      });

      this.controller.selectedSearchListSubject.subscribe((title: string) => {
        if (isDevMode()) {
          console.debug("selectedSearchListSubject.subscribe: " + this.title + ", " + title);
        }

        let visible: boolean = true;

        if (this.title && title && this.title !== title) {
          visible = false;
        }

        if (visible) {
          this.renderer.setStyle(this.elementRef.nativeElement, "display", "flex");
        } else {
          this.renderer.setStyle(this.elementRef.nativeElement, "display", "none");
        }

        this.refreshTree();
      });

      this.refreshTree();
    }
  }

  /**
   * This function would reset paging and fires
   */
  resetPaging(){
    this.paging.setPage(0);
    this.pagingSubject.next(this.paging);
  }

  /**
   * Updates the config with options and re-fetches data.
   *
   * @param config
   */
  updateConfig(config) {
    if (isDevMode()) {
      console.debug("updateConfig");
    }

    if (config) {
      this.config = Object.assign({}, this.config, config);

      if (config.route) {
        this.route = config.route;
      }
      if (config.pageSize) {
        this.pageSize = config.pageSize;
        this.paging.setPageSize(config.pageSize);
      }
      if (config.data) {
        this.boundData = config.data;
      }
      if (config.dataSubject) {
        this.dataSubject = config.dataSubject;
      }
      if (config.dataCall) {
        this.externalDataCall = config.dataCall;
      }
      if (config.searchTreeOptions) {
        this.searchTreeOptions = config.searchTreeOptions;
      } else {
        this.searchTreeOptions = this.defaultSearchTreeOptions;
      }
      if (config.itemTemplate) {
        this.itemTemplate = config.itemTemplate;
      }
      if (!this.itemTemplate) {
        this.itemTemplate = this.defaultTemplate;
      }

      if (config.loadingSubjects) {
        this.loadingSubjects = config.loadingSubjects;
      }
      if (config.onClick) {
        this.onClick = config.onClick;
      }
      if (config.onNodeMoved) {
        this.onNodeMoved = config.onNodeMoved;
      }
      if(config.onDataUpdate) {
        this.onDataUpdate = config.onDataUpdate;
      }
      if(config.expandTreeNodeIDs) {
        this.expandTreeNodeIDs = config.expandTreeNodeIDs;
      }
    }

    if (this.externalDataCall) {
      this.loadingSubjects = [new BehaviorSubject<boolean>(false)];
    }

    super.updateConfig(config);

    this.setData();
  }

  /**
   * Generates the data if an array, calls the data if external call.
   */
  setData(data?: Object[]): void {
    if (isDevMode()) {
      console.debug("setData");
    }

    if (data) {
      this.boundData = data;
    }


    if (this.externalDataCall) {
      if (!this.busy) {
        this.busy = true;
        this.loadingSubjects[0].next(true);

        this.externalDataCall(new HciGridDto(this.filters, this.sorts, this.paging, new HciGroupingDto()))
          .pipe(finalize(() => {
            this.loadingSubjects[0].next(false);
            this.busy = false;
          }))
          .subscribe((externalData: HciDataDto) => {
            this.generateData(externalData);
          });
      }
    } else {
      this.generateData();
    }
  }

  /**
   * Generates the data array that the interface uses to render (regardless of source).
   *
   * @param {HciDataDto} externalData
   */
  generateData(externalData?: HciDataDto): void {
    if (isDevMode()) {
      console.debug("generateData");
      console.debug(externalData);
    }

    if (externalData) {
      this.boundData = externalData.data;
      this.paging.setDataSize(externalData.gridDto.paging.dataSize);

      // Create row groups.
      this.viewData = [];

      for (let i = 0; i < this.boundData.length; i++) {
        this.viewData.push(this.boundData[i]);
      }
    } else {
      // Create and populate row groups.
      this.populateFromBoundData();
    }

    this.detectChanges();
    this.dataChange.emit(Object.assign({type: "none"}, this.lastEvent, {paging: this.paging, nViewData: this.viewData.length}));
  }

  /**
   * Generates the data if the data is an array (not external).
   */
  populateFromBoundData(): void {

    if (isDevMode()) {
      console.debug("populateFromBoundData");
      console.debug(this.boundData);
    }

    if (!this.boundData) {
      this.viewData = [];
      this.paging.setPage(0);
      return;
    }

    if (this.boundData.length === 0) {
      console.debug("Bound Data length found as 0");
      this.paging.setPage(0);
      this.paging.setNumPages(1);
      this.paging.setDataSize(0);
    }

    let preparedData = this.filterTreeIncludeChildren(this.boundData);

    if (this.sorts.length > 0) {
      this.sortData(preparedData);
    }

    this.paging.setDataSize(preparedData.length);


    if (this.paging.getPageSize() > 0) {
      this.viewData = [];
      for (var i = this.paging.getPage() * this.paging.getPageSize(); i < Math.min(preparedData.length, (this.paging.getPage() + 1) * this.paging.getPageSize()); i++) {
        this.viewData.push(preparedData[i]);
      }
    } else {
      this.paging.setDataSize(preparedData.length);
      this.viewData = preparedData;
    }

    if (isDevMode()) {
      console.debug("Filtered Tree Data:");
      console.debug(this.viewData);
    }

    this.refreshTree();
  }
  
  sortData(data: any) {
    data.sort((a: any, b: any) => {
      let v: number = 0;

      for (let sort of this.sorts) {
        if (sort.getAsc()) {
          if (this.upper(a[sort.getField()]) < this.upper(b[sort.getField()])) {
            v = -1;
          } else if (this.upper(a[sort.getField()]) > this.upper(b[sort.getField()])) {
            v = 1;
          }
        } else {
          if (this.upper(a[sort.getField()]) > this.upper(b[sort.getField()])) {
            v = -1;
          } else if (this.upper(a[sort.getField()]) < this.upper(b[sort.getField()])) {
            v = 1;
          }
        }

        if (v != 0) {
          return v;
        }
      }

      return v;
    });
    
    //Sort children
    if (this.sortChildren) {
      for (let child of data) {
        if (child[this.searchTreeOptions.childrenField]) {
          this.sortData(child[this.searchTreeOptions.childrenField]);
        }
      }
    }
  }

  upper(value: Object) {
    if (value && value != null) {
      return value.toString().toUpperCase();
    } else {
      return value;
    }
  }

  refreshTree () {
    this.treeComponent.treeModel.update();
    this.treeComponent.sizeChanged();
  }

  /**
   * Recursive tree search.  It will include children nodes if a parent node matches the search string.
   *
   * @param {any[]} data
   * @returns {any[]}
   */
  filterTreeIncludeChildren(data: any[]): any[] {
    if (isDevMode()) {
      console.debug("filterTreeIncludeChildren");
    }
    let filteredData: any[] = [];
    for (let child of data) {
      if (this.includeNode(child))  {
        filteredData.push(child);
      }
      else {
        if (child[this.searchTreeOptions.childrenField]) {
          let filteredChildren = this.filterTreeIncludeChildren(child[this.searchTreeOptions.childrenField]);
          if (filteredChildren.length > 0) {
            //Copy the node and replace its children with the filtered children
            let node: any = Object.assign({}, child);
            node[this.searchTreeOptions.childrenField] = filteredChildren;
            filteredData.push(node);
          }
        }
      }
    }
    return filteredData;
  }


  includeNode(row: any): boolean {
    let include: boolean = true;

    if (this.filters && this.filters.length > 0) {

      for (let filter of this.filters) {
        if (!filter.valid || !filter.getValue() || filter.getValue() === "") {
          continue;
        }

        if (filter.getField().indexOf(" or ") !== -1) {
          let innerInclude: boolean = false;

          for (let field of filter.getField().split(" or ")) {
            if (row[field] && row[field].toLowerCase().indexOf(filter.getValue().toLowerCase()) !== -1) {
              innerInclude = true;
            }
          }

          if (!innerInclude) {
            include = false;
          }
        } else {
          if (!row[filter.field]) {
            include = false;
            break;
          } else if (row[filter.field].toLowerCase().indexOf(filter.getValue().toLowerCase()) === -1) {
            include = false;
            break;
          }
        }
      }
    }

    return include;
  }
  onUpdateData(event: any): void {
    if (this.onDataUpdate) {
      this.onDataUpdate(this, this.treeComponent, event);
    }
  }

  onMoveNode(event: any): void {
    if (this.onNodeMoved) {
      this.onNodeMoved(this, this.treeComponent, event);
    }
  }

  onNodeClick(event: any): void {
    if (this.onClick) {
      this.onClick(this, this.treeComponent, event);
    }

    if (this.treeComponent.treeModel.activeNodes.length > 0) {
      this.nodeSelected.emit({
        event: event,
        id: this.treeComponent.treeModel.activeNodes[0].data.id,
        node: this.treeComponent.treeModel.activeNodes[0]
      });
    }
  }

  /**
   * Go to the first page.
   */
  public doPageFirst(): void {
    if (!this.busy) {
      this.paging.setPage(0);
      this.pagingSubject.next(this.paging);
    }
  }

  /**
   * Go to the previous page.
   */
  public doPagePrevious(): void {
    if (!this.busy) {
      if (this.paging.getPage() > 0) {
        this.paging.setPage(this.paging.getPage() - 1);
        this.pagingSubject.next(this.paging);
      }
    }
  }

  /**
   * Go to the next page.
   */
  public doPageNext(): void {
    if (!this.busy) {
      if (this.paging.getPage() < this.paging.getNumPages() - 1) {
        this.paging.setPage(this.paging.getPage() + 1);
        this.pagingSubject.next(this.paging);
      }
    }
  }

  /**
   * Go to the last page.
   */
  public doPageLast(): void {
    if (!this.busy) {
      this.paging.setPage(this.paging.getNumPages() - 1);
      this.pagingSubject.next(this.paging);
    }
  }


  //TODo : Handle the case where the user uses a custom groupTemplate
  /**
   * This function will cause SearchListComponent to navigate to a page and highlight the given group item, or
   * the row (in-case of non-grouped list)
   * This
   * @param groupId : This should contain a string to uniquely identify a particular group-display
   * @param rowId : This is the actual rowId for the row to be selected.
   */
  navigateToPageContainingElement(id: any){

    if (this.boundData.length === 0) {
      return;
    }

    this.boundData.forEach((row, index) => {
      if (row["id"] === id) {
        if (!this.busy) {
          let page = Math.floor(index / this.paging.getPageSize());
          if (this.paging.getNumPages() - 1 >= page) {
            this.paging.setPage(page);
            this.pagingSubject.next(this.paging);
          }
        }
      }
    });
  }
}
