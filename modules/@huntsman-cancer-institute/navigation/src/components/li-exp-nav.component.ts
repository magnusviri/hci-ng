import {
  ChangeDetectorRef, Component, ComponentFactoryResolver, ElementRef, Input, Renderer2, ViewChild, ViewContainerRef
} from "@angular/core";

import {ComponentType} from "@huntsman-cancer-institute/utils";

import {NavComponent} from "./nav.component";
import {LiNavComponent} from "./li-nav.component";
import {NavigationService} from "../services/navigation.service";

@ComponentType("LiExpNavComponent")
@Component({
  selector: "hci-li-exp",
  template: `
    <div class="hci-li-exp {{rootClass}}" (mouseover)="onMouseOver($event)" (mouseout)="onMouseOut($event)" [class.hover]="hover">
      <li [id]="id + '-li'" class="hci-li-exp-li {{liClass}}" (click)="handleLiClick($event)">
        <a *ngIf="!href && !route" class="hci-li-exp-a {{aClass}}">
          <i *ngIf="iClass && !iRight" class="hci-li-exp-i">
            <span class="{{iClass}}"></span>
            {{iText}}
          </i>
          <span class="hci-li-exp-span {{spanClass}}">{{title}}</span>
          <i *ngIf="iClass && iRight" class="hci-li-exp-i">
            <span class="{{iClass}}"></span>
            {{iText}}
          </i>
        </a>
        <a *ngIf="href" [href]="href" class="hci-li-exp-a {{aClass}}">
          <i *ngIf="iClass && !iRight" class="hci-li-exp-i">
            <span class="{{iClass}}"></span>
            {{iText}}
          </i>
          <span class="hci-li-exp-span {{spanClass}}">{{title}}</span>
          <i *ngIf="iClass && iRight" class="hci-li-exp-i">
            <span class="{{iClass}}"></span>
            {{iText}}
          </i>
        </a>
        <a *ngIf="route"
           routerLink="{{route}}"
           routerLinkActive="active-link"
           [queryParams]="queryParams"
           queryParamsHandling="{{queryParamsHandling}}"
           class="hci-li-exp-a {{aClass}}">
          <i *ngIf="iClass && !iRight" class="hci-li-exp-i">
            <span class="{{iClass}}"></span>
            {{iText}}
          </i>
          <span class="hci-li-exp-span {{spanClass}}">{{title}}</span>
          <i *ngIf="iClass && iRight" class="hci-li-exp-i">
            <span class="{{iClass}}"></span>
            {{iText}}
          </i>
        </a>
      </li>
      <ul [id]="id" class="hci-li-exp-ul {{ulClass}}" [class.expanded]="expanded" (click)="onUlClick($event)">
        <ng-container #containerRef></ng-container>
      </ul>
    </div>
  `
})
export class LiExpNavComponent extends LiNavComponent {

  @Input() liClass: string = "nav-item header";
  @Input() ulClass: string;
  @Input() hover: boolean = false;
  @Input() expandable: boolean = false;
  @Input() expanded: boolean = true;

  @ViewChild("containerRef", { read: ViewContainerRef, static: true }) private containerRef: any;

  private container: NavComponent[] = [];

  constructor(elementRef: ElementRef, renderer: Renderer2, resolver: ComponentFactoryResolver, navigationService: NavigationService, changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);
  }

  onMouseOver(event: MouseEvent) {
    if (this.hover) {
      this.expanded = true;
    }
  }

  onMouseOut(event: MouseEvent) {
    if (this.hover) {
      this.expanded = false;
    }
  }

  onUlClick(event: MouseEvent) {
    if (this.hover && this.expanded) {
      this.expanded = false;
    }
  }

  handleLiClick(event: MouseEvent) {
    if (!this.hover && this.expandable) {
      this.expanded = !this.expanded;
    } else if (this.hover && this.expanded) {
      this.expanded = false;
    }
  }

  getContainer(container: string): ViewContainerRef {
    return this.containerRef;
  }

  setConfig(config) {
    super.setConfig(config);
  }

  updateConfig(config) {
    super.updateConfig(config);

    if (!config) {
      return;
    }

    if (config.liClass) {
      this.liClass = config.liClass;
    }
    if (config.ulClass) {
      this.ulClass = config.ulClass;
    }
    if (config.hover !== undefined) {
      this.hover = config.hover;
    }
    if (config.expandable !== undefined) {
      this.expandable = config.expandable;
    }
    if (this.expandable || this.hover) {
      this.expanded = false;
    }
  }

}
