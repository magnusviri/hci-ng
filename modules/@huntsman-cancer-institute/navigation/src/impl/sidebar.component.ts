/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from "@angular/core";

import {NavigationService} from "../services/navigation.service";
import {NavComponent} from "../components/nav.component";
import {NavigationGlobalService} from "../services/navigation-global.service";

/**
 * TODO
 *
 * @since 1.0.0
 */
@Component(
  {
    selector: "hci-sidebar",
    template:
      `
        <div
          id="hci-sidebar-div"
          class="hci-sidebar {{sidebarClasses}}"
          [class.expanded]="sidebarExpanded"
          [style.width.px]="sidebarSize"
        >
          <div id="topSidebarContainer" class="top-sidebar-container d-flex">
            <ng-content></ng-content>
            <ng-container #topContainer></ng-container>

            <ng-container>
              <div class="bottom-of-sidebar mt-auto p-2"></div>
            </ng-container>

          </div>
        </div>

        <div
          *ngIf="draggable"
          class="sidebar-splitter"
          [style.left.px]="sidebarSize"
          [style.font-size.px]="sidebarFontSize"
          [style.width.px]="sidebarSplitterWidth"
          (mousedown)="mouseDown($event)"
          (dblclick)="negateSidebarExpanded()"
        >
          <i class="fas fa-2xl fa-grip-lines-vertical splitter-icon"></i>
        </div>
      `,
    providers: [
      NavigationService
    ]
  }
)
export class SidebarComponent extends NavComponent {

  @Input() hidden: boolean = false;
  @Input() output: string = "padding-left";
  @Input() sidebarClasses: string = "";
  @Input() showCollapse: boolean = true;
  @Input() draggable: boolean = true;
  @Input() sidebarSplitterWidth: number = 10;
  @Input() collapsedSize: number = 160;
  @Input() minWidth: number = 160;
  @Input() maxWidth: number = 400;
  @Input() sidebarSize: number = 220;
  @Input() sidebarFontSize: number = 20;
  @Input() sidebarExpanded: boolean = true;

  @Output() sidebarSizeChange = new EventEmitter<number>();

  topContainer: ViewContainerRef;

  navigationGlobalService: NavigationGlobalService;

  private dragging: boolean = false;
  private sidebarSizeMemory: number = 220;

  constructor(
    elementRef: ElementRef,
    renderer: Renderer2,
    resolver: ComponentFactoryResolver,
    navigationGlobalService: NavigationGlobalService,
    navigationService: NavigationService,
    changeDetectorRef: ChangeDetectorRef
  ) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);
    this.isRoot = true;
    this.navigationGlobalService = navigationGlobalService;
  }

  @ViewChild("topContainer",
    {
      read: ViewContainerRef,
      static: true
    }
  ) set topContainerSetter(topContainer: ViewContainerRef) {
    this.topContainer = topContainer;
    this.checkInitialized();
  }

  checkInitialized() {
    if (this.afterViewInit && this.topContainer) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.hideSidebarShadow();
      this.changeDetectorRef.detectChanges();
    }
  }

  getContainer(container?: string): ViewContainerRef {
    return this.topContainer;
  }

  clear() {
    super.clear();
    this.topContainer.clear();
  }

  setConfig(config: any) {
    super.setConfig(config);

    this.updateConfig(config);

    this.navigationService.setRootComponent(this.id, this);
  }

  updateConfig(config) {
    if (config.sidebarClasses) {
      this.sidebarClasses = config.sidebarClasses;
    }
    if (config.sidebarExpanded !== undefined) {
      this.sidebarExpanded = config.sidebarExpanded;
    }
    if (config.draggable !== undefined) {
      this.draggable = config.draggable;
    }
    if (config.showCollapse !== undefined) {
      this.showCollapse = config.showCollapse;
    }
    if (config.sidebarSplitterWidth) {
      this.sidebarSplitterWidth = config.sidebarSplitterWidth;
    }
    if (config.sidebarSize) {
      this.sidebarSizeMemory = config.sidebarSize;
      this.sidebarSize = config.sidebarSize;
    }
    if (config.collapsedSize) {
      this.collapsedSize = config.collapsedSize;
    }
    if (config.minWidth) {
      this.minWidth = config.minWidth;
    }
    if (config.maxWidth) {
      this.maxWidth = config.maxWidth;
    }
    if (config.hidden !== undefined) {
      this.setHidden(config.hidden);
    }

    // If we don't show the splitter, then set its width to 0.
    if (!this.draggable) {
      this.sidebarSplitterWidth = 0;
    }

    this.setSidebarExpanded(this.sidebarExpanded);

    super.updateConfig(config);
  }

  setHidden(hidden: boolean) {
    this.hidden = hidden;

    this.renderer.removeClass(this.elementRef.nativeElement, "hide");
    if (this.hidden) {
      this.renderer.addClass(this.elementRef.nativeElement, "hide");
    }
  }

  negateSidebarExpanded() {
    this.setSidebarExpanded(!this.sidebarExpanded);
    this.hideSidebarShadow();
  }

  hideSidebarShadow() {
    this.renderer.removeClass(this.elementRef.nativeElement.querySelector("#hci-sidebar-div"), "hide-shadow");
    if (this.draggable && this.sidebarExpanded) {
      this.renderer.addClass(this.elementRef.nativeElement.querySelector("#hci-sidebar-div"), "hide-shadow");
    }
  }

  setSidebarExpanded(sidebarExpanded) {
    this.sidebarExpanded = sidebarExpanded;

    if (this.sidebarExpanded) {
      this.setSidebarSize(this.sidebarSizeMemory);
    } else {
      this.sidebarSizeMemory = this.sidebarSize;
      this.setSidebarSize(this.collapsedSize);
    }
  }

  /**
   * Sets the sidebar width and notifies listeners about the change.  The size change emit can happen in different ways.
   * You can directly adjust the parent style which means the parent doesn't need to know anything about the sidebar.
   * Or a parent can listen to the sidebar change, and it would have to have its own behavior to deal with that.
   *
   * @param sidebarSize
   */
  setSidebarSize(sidebarSize: number) {
    this.sidebarSize = sidebarSize;

    if (this.output !== "padding-left") {
      this.sidebarSizeChange.emit(this.sidebarSize);
    }
  }

  mouseDown(event: MouseEvent) {
    if (this.sidebarExpanded && this.draggable) {
      this.dragging = true;
    }
  }

  @HostListener("document:mousemove", ["$event"])
  mouseMove(event: MouseEvent) {
    if (this.dragging) {
      this.setSidebarSize(Math.min(this.maxWidth, Math.max(this.minWidth, event.clientX)));
      event.stopPropagation();
      event.preventDefault();
    }
  }

  @HostListener("document:mouseup", ["$event"])
  mouseUp(event: MouseEvent) {
    if (this.dragging) {
      this.dragging = false;
    }
  }
}
