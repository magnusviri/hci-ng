import {ChangeDetectorRef, Component, ComponentFactoryResolver, ElementRef, Renderer2, ViewChild, ViewContainerRef} from "@angular/core";
import {TestBed, async} from "@angular/core/testing";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {} from "jasmine";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {RoleEntity} from "@huntsman-cancer-institute/user";

import {NavComponent} from "../components/nav.component";
import {NavigationService} from "../services/navigation.service";
import {LiNavComponent} from "../components/li-nav.component";
import {NavigationGlobalService} from "../services/navigation-global.service";
import {NavigationModule} from "../navigation.module";
import {UlNavComponent} from "../components/ul-nav.component";

@Component({
  selector: "hci-test",
  template: `
    <div>
      <ng-container #container></ng-container>
    </div>
  `,
  providers: [
    NavigationService
  ]
})
class TestComponent extends NavComponent {
  container: ViewContainerRef;

  constructor(elementRef: ElementRef, renderer: Renderer2, resolver: ComponentFactoryResolver, navigationService: NavigationService, changeDetectorRef: ChangeDetectorRef) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);

    this.isRoot = true;
  }

  @ViewChild("container", {read: ViewContainerRef, static: true}) set containerSetter(container: ViewContainerRef) {
    this.container = container;
    this.checkInitialized();
  }

  checkInitialized() {
    if (this.afterViewInit && this.container) {
      this.initialized = true;
      this.updateConfig(this.config);
      this.changeDetectorRef.detectChanges();
    }
  }

  getContainer(containerName?: string): ViewContainerRef {
    return this.container;
  }

}

describe("TestComponent Role Tests", () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        NavigationModule
      ],
      providers: [
        NavigationGlobalService
      ],
      declarations: [
        TestComponent
      ]
    });
  });

  it("Populates.",
    async(() => {
      let fixture = TestBed.createComponent(TestComponent);
      let cmpt = fixture.componentInstance;
      fixture.detectChanges();

      cmpt.setConfig({
        id: "test",
        children: [
          {type: LiNavComponent, id: "li-1", title: "Li 1"},
          {type: LiNavComponent, id: "li-2", title: "Li 2", roleName: "ADMIN"},
          {type: LiNavComponent, id: "li-3", title: "Li 3", roleName: "USER"},
          {type: LiNavComponent, id: "li-4", title: "Li 4", roleName: "USER"}
        ]
      });

      console.debug("nChildren: " + fixture.nativeElement.querySelectorAll("hci-li").length);
      console.debug();

      expect(fixture.nativeElement.querySelectorAll("hci-li").length).toEqual(1);
    })
  );

  it("Resorts with new roles.",
    async(() => {
      let fixture = TestBed.createComponent(TestComponent);
      let cmpt = fixture.componentInstance;
      fixture.detectChanges();

      cmpt.setConfig({
        id: "test",
        children: [
          {type: LiNavComponent, id: "li-1", title: "Li 1"},
          {type: LiNavComponent, id: "li-2", title: "Li 2", roleName: "ADMIN"},
          {type: LiNavComponent, id: "li-3", title: "Li 3", roleName: "USER"},
          {type: LiNavComponent, id: "li-4", title: "Li 4", roleName: "USER"}
        ]
      });

      console.debug("nChildren: " + fixture.nativeElement.querySelectorAll("hci-li").length);
      console.debug();
      expect(fixture.nativeElement.querySelectorAll("hci-li").length).toEqual(1);

      cmpt.getNavigationService().setUserRoles([new RoleEntity("ADMIN")]);

      console.debug("nChildren: " + fixture.nativeElement.querySelectorAll("hci-li").length);
      console.debug();
      expect(fixture.nativeElement.querySelectorAll("hci-li").length).toEqual(2);

      cmpt.getNavigationService().setUserRoles([new RoleEntity("USER")]);

      console.debug("nChildren: " + fixture.nativeElement.querySelectorAll("hci-li").length);
      console.debug();
      expect(fixture.nativeElement.querySelectorAll("hci-li").length).toEqual(3);
    })
  );

  it("Resorts with new roles inside sub container.",
    async(() => {
      let fixture = TestBed.createComponent(TestComponent);
      let cmpt = fixture.componentInstance;
      fixture.detectChanges();

      cmpt.setConfig({
        id: "test",
        children: [
          {type: UlNavComponent, id: "ul-1", title: "Ul 1", children: [
              {type: LiNavComponent, id: "li-1", title: "Li 1"},
              {type: LiNavComponent, id: "li-2", title: "Li 2", roleName: "ADMIN"},
              {type: LiNavComponent, id: "li-3", title: "Li 3", roleName: "USER"},
              {type: LiNavComponent, id: "li-4", title: "Li 4", roleName: "USER"}
            ]
          }
        ]
      });

      console.debug("nChildren: " + fixture.nativeElement.querySelectorAll("hci-li").length);
      console.debug();
      expect(fixture.nativeElement.querySelectorAll("hci-li").length).toEqual(1);

      cmpt.getNavigationService().setUserRoles([new RoleEntity("ADMIN")]);

      console.debug("nChildren: " + fixture.nativeElement.querySelectorAll("hci-li").length);
      console.debug();
      expect(fixture.nativeElement.querySelectorAll("hci-li").length).toEqual(2);

      cmpt.getNavigationService().setUserRoles([new RoleEntity("USER")]);

      console.debug("nChildren: " + fixture.nativeElement.querySelectorAll("hci-li").length);
      console.debug();
      expect(fixture.nativeElement.querySelectorAll("hci-li").length).toEqual(3);
    })
  );

});
