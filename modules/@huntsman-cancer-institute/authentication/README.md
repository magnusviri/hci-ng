# Angular Authentication

TODO: Separate out the parts from @hci/user.

This library provides elements for authenticating the user, protecting routes, 
defining that user representation in the client application, directives for controlling the UI based on aspects of the 
user like authorization claims and destroying the user session (logout).

A component can be imported directly though the main index.ts barrel file which groups all components in this library for
easy importing.

## Usage

This package provides a configurable service to interact with a server-side REST api. It is designed to be configurable
through the Angular injection framework and the usage of OpaqueTokens. See the development harnesses application module
for details or review the jsDocs on the `UserService` class.

### Features
- Configurable user service for interactions with server-side REST api
- A route protection service that implements `CanActivate`
- Immutable entities to represent the user representation in the client application

### API

#### User Service
The user service is configured through module injection. The tokens that must be defined are:
- AUTHENTICATED_USER_ENDPOINT: the url to the endpoint for the currently authenticated subject
- DEFAULT_SUCCESS_URL: the url to navigate the user to after a successful authentication attempt, if another location wasn't declared in the request
- USER_SESSION_ENDPOINT: a user session endpoint configuration (e.g. https://localhost/core/api/user-session)
- SERVER_URL: a url to define the base server api, if a variable login/logout api configuration is necessary (e.g. https://localhost/core/api)
- LOGIN_PATH: a variable path for login that extends SERVER_URL (e.g. /login)
- LOGOUT_PATH: a variable path for logout that extends SERVER_URL (e.g. /logout)

When configuring the user service it must include either USER_SESSION_ENDPOINT or SERVER_URL, LOGIN_PATH and LOGOUT_PATH, 
depending on the type of API to be supported (consistent vs. variable).  The service will fail to instantiate if all 
opaque tokens are provided with values other than null. See the UserService jsdocs for details about configuring this aspect
of the user feature module.

##### Login Callbacks
The user service also supports login success callbacks.  Components may add one or more callback functions that are invoked 
in the order they were added upon successful authentication. This is useful when you require something special to happen after 
authentication, but outside of the scope of component initialization and creation.

#### Route Guard Service
A service to enable secured endpoints to force authentication. The tokens that must be defined are:
- LOGIN_ROUTE: the configured login component path 

#### LoginComponent
This component can be included with the selector **hci-login-form** and will provide a login form that will integrate with
the UserService to authenticate a user and provide the expected form validation.
