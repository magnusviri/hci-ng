/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the HCI ng authorization package.
 *
 * @since 1.0.0
 */
export {AuthenticationModule} from "./src/authentication.module";

/**
 * The injection tokens for service configuration.
 */
export {
  AUTHENTICATION_LOGOUT_PATH,
  AUTHENTICATION_SERVER_URL,
  AUTHENTICATION_TOKEN_ENDPOINT,
  AUTHENTICATION_DIRECT_ENDPOINT,
  AUTHENTICATION_ROUTE,
  AUTHENTICATION_MAX_INACTIVITY_MINUTES,
  AUTHENTICATION_USER_COUNTDOWN_SECONDS,
  AUTHENTICATION_IDP_INACTIVITY_MINUTES
} from "./src/authentication.service";

export {AUTHENTICATION_TOKEN_KEY} from "./src/authentication.provider";

export {AuthenticationComponent} from "./src/authentication.component"
export {DirectLoginComponent} from "./src/directlogin.component"
export {RouteGuardService} from "./src/route-guard.service"
export {AuthenticationService} from "./src/authentication.service"
export {TimeoutNotificationComponent} from "./src/timeout-notification.component";
export {AuthorizationInterceptor} from "./src/authorization.interceptor";
