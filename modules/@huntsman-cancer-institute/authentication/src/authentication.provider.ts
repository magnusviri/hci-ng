import {Inject, Injectable, InjectionToken, Injector} from "@angular/core";

import {Subject} from "rxjs";

import {CoolLocalStorage} from '@angular-cool/storage';

export let AUTHENTICATION_TOKEN_KEY = new InjectionToken<string>("authentication_token_key");

@Injectable()
export class AuthenticationProvider {

  public whitelistedDomains = [
    "localhost",
    new RegExp(".*[.]utah[.]edu")
  ];

  constructor(private _localStorageService: CoolLocalStorage,
              @Inject(AUTHENTICATION_TOKEN_KEY) private _authenticationTokenKey: string) {}

  public tokenGetter = () => {
    return this.authToken;
  }

  get authenticationTokenKey(): string {
    return this._authenticationTokenKey;
  }

  set authenticationTokenKey(_authenticationTokenKey: string) {
    this._authenticationTokenKey = _authenticationTokenKey;
  }

  get authToken(): string {
    return <string>this._localStorageService.getItem(this._authenticationTokenKey);
  }

}
