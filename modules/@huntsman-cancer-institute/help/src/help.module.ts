/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {UserModule} from "@huntsman-cancer-institute/user";

import {HelpService} from "./help.service";
import {HelpComponent} from "./help.component";

/**
 * @since 1.0.0
 */
@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    UserModule
  ],
  declarations: [
    HelpComponent
  ],
  exports: [
    HelpComponent
  ],
  providers: [
    HelpService
  ]
})
export class HelpModule {}
