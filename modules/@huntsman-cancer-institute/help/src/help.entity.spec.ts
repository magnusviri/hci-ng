/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
import {HelpEntity} from "./help.entity";
import {ApplicationContextEnum} from "./application-context.enum";

/**
 * @since 1.0.0
 */
describe("HelpEntity Tests", () => {
  let gnomexAppCtxt: string = ApplicationContextEnum[0];
  let fooKey: HelpEntity.Key = new HelpEntity.Key(1, gnomexAppCtxt);
  let sameFooKey: HelpEntity.Key = new HelpEntity.Key(1, gnomexAppCtxt);
  let gooKey: HelpEntity.Key = new HelpEntity.Key(42, gnomexAppCtxt);
  let fooHelpEntity: HelpEntity = new HelpEntity("foo", "foo title", "foo body");

  it("Should have its key evaluated as equal to a key with the same composite values.", () => {
    expect(fooKey.equals(sameFooKey)).toBeTruthy();
  });

  it("Should have its key evaluated as not equal to a key with different composite values.", () => {
    expect(fooKey.equals(gooKey)).toBeFalsy();
  });

  it("Should provide the expected canonical JSON representation.", () => {
    expect(JSON.stringify(fooHelpEntity)).toBe('{"tooltip":"foo","title":"foo title","body":"foo body"}');
  });
});
