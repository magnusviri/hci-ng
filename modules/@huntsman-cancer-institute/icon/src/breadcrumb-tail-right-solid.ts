export const hciBreadcrumbTailRightSolid = {
  prefix: "hci",
  iconName: "breadcrumb-tail-right-solid",
  icon: [512, 512, [], "f129", "M512 0H325.7L512 256 325.7 512H512z"]
};
