export const hciConsentHexOpen = {
  prefix: "hci",
  iconName: "consent-hex-open",
  icon: [512, 512, [], "f130", "M256,0l221.7,128v256L256,512L34.3,384V128L256,0z M477.7,34.3c-179.5,93.7-278,254.3-278,254.3L76.5,256l172,172 C248.6,428,291.7,201.4,477.7,34.3z"]
};
