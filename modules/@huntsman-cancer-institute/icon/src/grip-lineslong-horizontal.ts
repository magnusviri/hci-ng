export const hciGripLinesLongHorizontal = {
  prefix: "hci",
  iconName: "grip-lineslong-horizontal",
  icon: [512, 512, [], "f202", "M236 512h-8c-2.2 0-4-1.8-4-4V4c0-2.2 1.8-4 4-4h8c2.2 0 4 1.8 4 4v504c0 2.2-1.8 4-4 4zm48 0h-8c-2.2 0-4-1.8-4-4V4c0-2.2 1.8-4 4-4h8c2.2 0 4 1.8 4 4v504c0 2.2-1.8 4-4 4z"]
  };
