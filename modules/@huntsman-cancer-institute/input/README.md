# Angular Input Library

This is a collection of input components that will be imported by projects that contain forms and other screens that
require input devices.

## Usage

There are three distinct modules: date, inline and dropdown.

## Inline

Inline inputs are designed to work like inputs in JIRA.  Normally they just display text.  When you hover over them,
it gives you the option of editing that field.  While editing, you have the option of saving the edit or cancelling out.
Saving can be done by clicking the check or typing enter.  Cancelling can be done by clicking the X or typing escape.
