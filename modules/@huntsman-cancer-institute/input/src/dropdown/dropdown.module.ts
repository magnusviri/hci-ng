import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import {DropdownSelectComponent} from "./dropdown-select.component";
import {DropdownSelectResultComponent} from "./dropdown-select-result.component";
import {DropdownComponent} from "./dropdown.component";
import {TemplateDropdownDirective} from "./template-dropdown.directive";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  declarations: [
    TemplateDropdownDirective,
    DropdownSelectComponent,
    DropdownComponent,
    DropdownSelectResultComponent
  ],
  exports: [
    TemplateDropdownDirective,
    DropdownSelectComponent,
    DropdownComponent,
    DropdownSelectResultComponent
  ]
})
export class DropdownModule {}
