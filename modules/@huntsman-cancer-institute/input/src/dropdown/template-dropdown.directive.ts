import {Directive, Host, TemplateRef} from "@angular/core";
import {DropdownSelectComponent} from "./dropdown-select.component";

/**
 * A customized template for Dropdown list items.
 */
@Directive({
  selector: "[appTemplateDropdown]"
})
export class TemplateDropdownDirective<T> {

  constructor(private templateRef: TemplateRef<T>, @Host() host: DropdownSelectComponent) {
    if (host instanceof DropdownSelectComponent) {
      host.templateRef = templateRef;
    }
  }

}
