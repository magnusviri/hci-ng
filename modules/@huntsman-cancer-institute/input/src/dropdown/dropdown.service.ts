import {Inject, Injectable, isDevMode} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";

import {Observable, of, timer} from "rxjs";
import {map} from "rxjs/operators";

import {DICTIONARY_ENDPOINT} from "@huntsman-cancer-institute/dictionary-service";

export const DROPDOWN_TYPE = Object.freeze({
  INPUT_SINGLE: 0,
  INPUT_MULTI: 1,
  SELECT_SINGLE: 2,
  SELECT_MULTI: 3,
  SELECT_SINGLE_CUSTOM: 4,
  SELECT_MULTI_CUSTOM: 5
});

@Injectable()
export class DropdownService {

  listField : string[] = [];

  DICTIONARY_ENDPOINT = "";

  private headers: HttpHeaders = new HttpHeaders().set("Content-Type", "application/json");
  private options = {headers: this.headers};

  sortFunction: (dropdownObj1: Object, dropdownObj2: Object) => number = (dropdownObj1: Object, dropdownObj2: Object) => {
    if (dropdownObj1[this.listField[0]] < dropdownObj2[this.listField[0]]) {
      return -1;
    } else if (dropdownObj1[this.listField[0]] > dropdownObj2[this.listField[0]]) {
      return 1;
    } else {
      return 0;
    }
  }

  constructor(private http: HttpClient, @Inject(DICTIONARY_ENDPOINT) private dictionaryEndpoint: string) {}

  /**
   * Get a list of data from the given object or url to set to the dropdown list.
   * @param {string} urlStr
   * @param {Object[]} dataObject
   * @param {string} displayField
   * @returns {any[]}
   */
  setData(urlStr: string, dataObject: Object[], displayField: string): any[] {
    let listFieldSet: string[] = [];
    let dataListSet: Object[] = [];

    if (dataObject) {
      // Set data from the passed object.
      if (isDevMode) {
        console.debug("DropdownService.setData. dataObject=:", dataObject);
      }
      if (dataObject) {
        if (listFieldSet.length === 0 || listFieldSet[0] === "") {
          dataObject.forEach(item => dataListSet.push(item));
          listFieldSet.push(displayField);
          this.listField.push(displayField);
        }
      }
    } else if (urlStr && urlStr.length > 0) {
    //  Set data from a passed url.
      if (isDevMode) {
        console.debug("DropdownService.setData. urlStr=:", urlStr);
      }
      this.getDataList(urlStr).subscribe((data: Object[]) => {
        if (data) {
          if (listFieldSet.length === 0 || listFieldSet[0] === "") {
            data.forEach(item => dataListSet.push(item));
            listFieldSet.push(displayField);
            this.listField.push(displayField);

            console.debug(data);
            console.debug(this.listField);
          }
        }
      });
    } else {
      throw new Error("Neither optionData or dataUrl were defined.");
    }

    return dataListSet;
  }

  listData(pattern: string, dataSet: any[], maxResults?: number): Observable<Object[]> {
    return of(dataSet
      .filter((dropdownObj) => dropdownObj[this.listField[0]].toUpperCase().indexOf(pattern.toUpperCase()) !== -1)
      .sort(this.sortFunction));
  }

  /**
   * Get the max list data
   * @param {string} pattern
   * @param {number} maxResults
   * @param {any[]} dataSet
   * @returns {Observable<{count: number; results: Object[]}>}
   */
  listDataMax(pattern: string, maxResults: number, dataSet: any[]): Observable<{ count: number, results: Object[] }> {
    const filteredList = dataSet
      .filter((dropdownObj) => dropdownObj[this.listField[0]].toUpperCase().indexOf(pattern.toUpperCase()) !== -1)
      .sort(this.sortFunction);

    return timer(3000)
      .pipe(map((t) => {
        return {
          count: filteredList.length,
          results: maxResults && maxResults < filteredList.length ? filteredList.splice(0, maxResults) : filteredList
        };
      }));
  }

  /**
   * Push the selected items to a list to show in the input text area.
   * @param {any[]} ids
   * @param {any[]} dataSet
   * @returns {Observable<Object[]>}
   */
  getItems(ids: any[], dataSet: any[]): Observable<Object[]> {
    const selectedItems: Object[] = [];

    ids.forEach((id) => {
      dataSet
        .filter((item) => item.id === id)
        .map((item) => selectedItems.push(item));
    });

    return of(selectedItems);
  }

  // Get data from the given url to set in the dropdown list.
  getDataList(urlStr: string): Observable<any> {
    return this.http.get(this.dictionaryEndpoint + urlStr);
  }

}
