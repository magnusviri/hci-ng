import {Component, forwardRef, HostBinding, Input, ChangeDetectorRef} from "@angular/core";

import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {DictionaryService, DropdownEntry} from "@huntsman-cancer-institute/dictionary-service";

@Component({
  selector: "hci-native-select",
  template: `
    <div *ngIf="label" class="label">
      {{label}}{{(required) ? " *" : ""}}
    </div>
    <select [(ngModel)]="value"
            [style.height.px]="height"
            (change)="onChange()"
            [disabled]="disabled"
            class="form-control flex-grow-1"
            style="min-width: 4rem;">
      <ng-container *ngIf="!required">
        <option [ngValue]="undefined">
        </option>
      </ng-container>
      <option *ngFor="let row of entries"
              [ngValue]="row[idKey]">
        {{ row[displayKey] }}
      </option>
    </select>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NativeSelectComponent),
      multi: true
    }
  ]
})
export class NativeSelectComponent implements ControlValueAccessor {

  @HostBinding("class") @Input("class") classList: string = "";

  value: any;
  height: number;
  unfilteredEntries: DropdownEntry[];

  @Input() name: string;
  @Input() label: string;
  @Input() url: string;
  @Input() entries: DropdownEntry[];
  @Input() required: boolean = false;
  @Input() idKey: string = "id";
  @Input() displayKey: string = "display";
  @Input() disabled: boolean = false;
  @Input() sortKey: string = null;
  @Input() sortNumeric: boolean = false;
  @Input() filterKey: string = null;

  _filter: any = null;
  get filter(): any{
    return this._filter;
  }
  @Input() set filter(f: any){
    this._filter = f;

    if(this.filterKey != null && this.filter != null && this.unfilteredEntries != null) {
      this.entries = this.unfilteredEntries;
      var newlist = this.entries.filter(entry => entry[this.filterKey] == this.filter);
      this.entries = newlist;

      if(this.sortKey != null) {
        if(this.sortNumeric) {
          this.entries.sort((a, b) => {
            return a[this.sortKey] - b[this.sortKey];
          });
        }else {
          this.entries.sort((a, b) => {
            return a[this.sortKey].localeCompare(b[this.sortKey]);
          });
        }
      }
    }

  }

  onChange: any;

  constructor(private dictionaryService: DictionaryService, private http: HttpClient, private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (this.url && this.url.indexOf("/") === -1) {
      this.dictionaryService.getDictionaryDropdownEntries(this.url).subscribe((entries: DropdownEntry[]) => {
        this.entries = entries;

        if(this.filterKey != null && this.filter != null) {
          this.unfilteredEntries = this.entries;
          var newlist = this.entries.filter(entry => entry[this.filterKey] == this.filter);
          this.entries = newlist;
        }

        if(this.sortKey != null) {
          if(this.sortNumeric) {
            this.entries.sort((a, b) => {
              return a[this.sortKey] - b[this.sortKey];
            });
          }else {
            this.entries.sort((a, b) => {
              return a[this.sortKey].localeCompare(b[this.sortKey]);
            });
          }
        }

      });
    } else if (this.url) {
      this.http.get(this.url).subscribe((entries: any[]) => {
        this.entries = entries;

        if(this.filterKey != null && this.filter != null) {
          this.unfilteredEntries = this.entries;
          var newlist = this.entries.filter(entry => entry[this.filterKey] == this.filter);
          this.entries = newlist;
        }

        if(this.sortKey != null) {
          if(this.sortNumeric) {
            this.entries.sort((a, b) => {
              return a[this.sortKey] - b[this.sortKey];
            });
          }else {
            this.entries.sort((a, b) => {
              return a[this.sortKey].localeCompare(b[this.sortKey]);
            });
          }
        }
      });
    }else {

      if(this.filterKey != null && this.filter != null) {
        this.unfilteredEntries = this.entries;
        var newlist = this.entries.filter(entry => entry[this.filterKey] == this.filter);
        this.entries = newlist;
      }

      if(this.sortKey != null) {
        if(this.sortNumeric) {
          this.entries.sort((a, b) => {
            return a[this.sortKey] - b[this.sortKey];
          });
        }else {
          this.entries.sort((a, b) => {
            return a[this.sortKey].localeCompare(b[this.sortKey]);
          });
        }
      }
    }

    if(this.classList.length > 0) {
      this.classList += " native-input"; //?
    }else {
      this.classList = "native-input";
    }
  }

  public setHeight(h: number) {
    this.height = h;
    // avoid ExpressionChangedAfterItHasBeenCheckedError
    this.cdr.detectChanges();
  }

  writeValue(value: any) {
    if (value !== undefined) {
      this.value = value;
    }
  }

  registerOnChange(fn) {
    this.onChange = () => {
      fn(this.value);
    };
  }

  registerOnTouched(fn: () => void): void {}
}
