import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatSelectModule} from "@angular/material/select";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatInputModule} from "@angular/material/input";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatIconModule} from "@angular/material/icon";
import {ScrollingModule} from "@angular/cdk/scrolling";

import {MdSelectComponent} from "./md-select.component";
import {NativeSelectComponent} from "./native-select.component";
import {MdMultiSelectComponent} from "./md-multi-select.component";
import {CustomComboBoxComponent} from "./custom-combobox.component";
import {CustomMultiComboBoxComponent} from "./custom-multi-combobox.component";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {DictionaryServiceModule} from "@huntsman-cancer-institute/dictionary-service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    ScrollingModule,
    MatAutocompleteModule,
    MatInputModule,
    MatTooltipModule,
    MatIconModule,
    MatProgressSpinnerModule,
    DictionaryServiceModule
  ],
  declarations: [
    MdSelectComponent,
    MdMultiSelectComponent,
    NativeSelectComponent,
    CustomComboBoxComponent,
    CustomMultiComboBoxComponent
  ],
  exports: [
    MdSelectComponent,
    MdMultiSelectComponent,
    NativeSelectComponent,
    CustomComboBoxComponent,
    CustomMultiComboBoxComponent
  ]
})
export class SelectModule {}
