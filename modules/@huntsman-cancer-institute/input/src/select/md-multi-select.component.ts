import {Component, forwardRef, HostBinding, Input} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from "@angular/forms";

import {DictionaryService, DropdownEntry} from "@huntsman-cancer-institute/dictionary-service";

@Component({
  selector: "hci-md-multi-select",
  template: `
    <mat-form-field class="flex-grow-1">
      <mat-label>{{label}}</mat-label>
      <mat-select [(value)]="value" [formControl]="options" [required]="required" multiple>
        <mat-select-trigger class="d-flex flex-wrap">
          <div *ngFor="let option of options.value" class="chip">
            {{getOptionDisplay(option)}}
            <div (click)="$event.stopPropagation(); remove(option)" class="chip-remove">
              <i class="fas fa-times-circle fa-xs"></i>
            </div>
          </div>
        </mat-select-trigger>
        <mat-option *ngFor="let row of entries" [value]="row[idKey]">{{ row[displayKey] }}</mat-option>
      </mat-select>
    </mat-form-field>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MdMultiSelectComponent),
      multi: true
    }
  ]
})
export class MdMultiSelectComponent implements ControlValueAccessor {

  @HostBinding("class") @Input("class") classList: string = "";

  @Input() name: string;
  @Input() label: string;
  @Input() url: string;
  @Input() entries: DropdownEntry[];
  @Input() required: boolean = false;
  @Input() idKey: string = "id";
  @Input() displayKey: string = "display";
  @Input() sortKey: string = null;
  @Input() sortNumeric: boolean = false;

  options = new FormControl();

  private _value: any[];
  private onChangeCallback: (_: any[]) => void = () => {};
  private onTouchedCallback: () => void = () => {};

  constructor(private dictionaryService: DictionaryService, private http: HttpClient) {}

  ngOnInit(): void {
    if (this.url && this.url.indexOf("/") === -1) {
      this.dictionaryService.getDictionaryDropdownEntries(this.url).subscribe((entries: DropdownEntry[]) => {
        this.entries = entries;
        this.sortEntries();
      });
    } else if (this.url) {
      this.http.get(this.url).subscribe((entries: any[]) => {
        this.entries = entries;
        this.sortEntries();
      });
    } else {
      this.sortEntries();
    }

    if(this.classList.length > 0) {
      this.classList += " material-input";
    }else {
      this.classList = "material-input";
    }
  }

  sortEntries() {
    if(this.sortKey != null) {
      if(this.sortNumeric) {
        this.entries.sort((a, b) => {
          return a[this.sortKey] - b[this.sortKey];
        });
      }else {
        this.entries.sort((a, b) => {
          return a[this.sortKey].localeCompare(b[this.sortKey]);
        });
      }
    }
  }

  getOptionDisplay(value: any): string {
    for (let entry of this.entries) {
      if (entry[this.idKey] === value) {
        return entry[this.displayKey];
      }
    }

    return undefined;
  }

  remove(value: any): void {
    console.debug(this.options);

    let newValue: any[] = [];

    for (let option of this.options.value) {
      if (option !== value) {
        newValue.push(option);
      }
    }

    this.writeValue(newValue);
  }

  public get value(): any[] {
    return this._value;
  }

  public set value(newValue: any[]) {
    this._value = newValue;
    this.options.setValue(newValue);
    this.onChangeCallback(newValue);
  }

  public onBlur() {
    this.onTouchedCallback();
  }

  public writeValue(value: any[]) {
    if (value !== this.value) {
      this.value = value;
      this.options.setValue(value);
    }
  }

  public registerOnChange(callback: (_: any[]) => void) {
    this.onChangeCallback = callback;
  }


  public registerOnTouched(callback: () => void) {
    this.onTouchedCallback = callback;
  }
}
