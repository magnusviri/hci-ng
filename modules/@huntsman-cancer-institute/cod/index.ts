/*
 * Copyright (c) 2016 Huntsman Cancer Institute at the University of Utah, Confidential and Proprietary
 */
/**
 * A barrel file for the cod package.
 *
 * @since 1.0.0
 */

import {AttributeAbsoluteComponent} from "./src/components/attribute-absolute.component";

export {CodModule} from "./src/cod.module";

export {AttributeConfigurationComponent} from "./src/components/attribute-configuration.component";

export {AttributeService} from "./src/services/attribute.service";

export {Attribute} from "./src/model/attribute.entity";
export {AttributeChoice} from "./src/model/attribute-choice.entity";
export {AttributeConfiguration} from "./src/model/attribute-configuration.entity";
export {AttributeContainer} from "./src/model/attribute-container.entity";
export {AttributeDictionary} from "./src/model/attribute-dictionary.entity";
export {AttributeValue} from "./src/model/attribute-value.entity";
export {AttributeValueGridRow} from "./src/model/attribute-value-grid-row.entity";
export {AttributeValueSet} from "./src/model/attribute-value-set.entity";
export {ExtractableFieldStatus} from "./src/model/extractable-field-status.entity";
export {AttributeConfigurationDTO} from "./src/model/attribute-configuration.dto";
export {GraphicalAttribute} from "./src/model/graphical-attribute.entity";

export {AttributeBase} from "./src/components/attribute-base";
export {AttributeAbsoluteComponent} from "./src/components/attribute-absolute.component";
export {AttributeFlexComponent} from "./src/components/attribute-flex.component";
export {AttributeEditComponent} from "./src/components/attribute-edit.component";
export {AttributeDefaultComponent} from "./src/components/attribute-default.component";
export {AttributeContainerComponent} from "./src/components/attribute-container.component";

export {ATTRIBUTE_ENDPOINT} from "./src/services/attribute.service";
