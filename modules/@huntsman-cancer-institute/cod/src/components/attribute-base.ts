import {
  ElementRef, EventEmitter, Input, isDevMode, Output, Renderer2, ViewChild, ViewChildren, OnInit, OnDestroy, Inject, Directive, QueryList
} from "@angular/core";

import {DatePipe} from '@angular/common'

import { BehaviorSubject, Subscription} from "rxjs";
import {first} from "rxjs/operators";

import {AttributeService} from "../services/attribute.service";
import {Attribute} from "../model/attribute.entity";
import {AttributeValue} from "../model/attribute-value.entity";
import {AttributeChoice} from "../model/attribute-choice.entity";
import {AttributeLongText} from "../model/attribute-long-text.entity";
import {GraphicalAttribute} from "../model/graphical-attribute.entity";
import {DictionaryEntriesDTO} from "../model/dictionary-entries.dto";
import {DateUtil} from "../date/date-util";
import {NativeSelectComponent} from "@huntsman-cancer-institute/input";
import {NgbModal, NgbModalRef, NgbPanel} from "@ng-bootstrap/ng-bootstrap";
import {GridApi, RowDoubleClickedEvent, RowNode, RowSelectedEvent, AgGridEvent, GridSizeChangedEvent, ModelUpdatedEvent} from "ag-grid-community";

/**
 * The base class for an attribute.  Attributes are iterated over by container.  These may be in traditional absolute
 * positioning, flex positioning, or an edit mode as a column in a modal.  Regardless of the template, the fundamental
 * needs of setting up the attribute values and persisting data are the same.  So this class provides base functionality
 * and the components that extend it provide the template and may also extend the functionality.
 *
 * Types:
 * LINE
 * NLP_LINK
 * DICT
 * GA
 * LABEL
 * DT
 * AC
 * TXT
 * D
 * S
 * I
 * CB
 * B
 * N
 * EB
 */
@Directive({
    selector: "hci-attribute-base"
  }
)
export class AttributeBase implements OnInit, OnDestroy {

  @Input() attribute: GraphicalAttribute;
  @Input() internalValues: boolean = false;
  @Input() editInline: boolean = true;
  @Input() groupAttributeRowId: number = undefined;

  @Output() resortColumns: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild("inputRef", {static: false}) inputRef: ElementRef;
  @ViewChild("nativeSelectRef", {static: false}) nativeSelectRef: NativeSelectComponent;

  attributeValues: AttributeValue[] = [];
  attributeChoices: AttributeChoice[];

  childAttributes: GraphicalAttribute[];
  gridData: any[];
  gridColumns: any[];

  //Used for the edit group row template
  editGroupAttributeRowId: number;
  editGroupRowAttributes: GraphicalAttribute[];

  dictionaryEntries: any[];

  width: number = 0;

  attributeService: AttributeService;
  elementRef: ElementRef;
  renderer: Renderer2;

  gridApi: GridApi;

  gridOptions = {
      headerHeight: 23,
      rowHeight: 23,
      rowStyle: {
          'font-family': 'Prompt, sans-serif',
          'color': '#3d7a99'
      },
      enableCellTextSelection: true,
      ensureDomOrder: true,
      suppressColumnVirtualisation: false
    };

  subscriptions: Subscription = new Subscription();

  constructor(attributeService: AttributeService, elementRef: ElementRef, renderer: Renderer2, private modalService: NgbModal) {
    this.attributeService = attributeService;
    this.elementRef = elementRef;
    this.renderer = renderer;
  }

  /**
   * Get any attribute values for this attribute.  If a complex attribute like a grid, set it up.
   */
  ngOnInit(): void {
    this.init();

    this.subscriptions.add(this.attributeService.getContainerUpdated().subscribe((updated: boolean) => {
        this.init();
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  onGridReady(params : any) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  }

  onModelUpdated(_event: ModelUpdatedEvent) {
    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }

  onGridSizeChanged(_event: GridSizeChangedEvent) {
    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }

  /**
   * Pulls the attribute values associated with this attribute and performs any specialized initialization such as for
   * the grid or multi choice.
   */
  init(): void {
    if (isDevMode()) {
      console.debug("Attribute.init: " + this.attribute.idAttribute);
    }

    this.gridData = undefined;
    this.gridColumns = undefined;
    this.attributeChoices = undefined;
    this.width = this.attributeService.getWidth();

    if (!this.internalValues) {
      this.attributeValues = this.attributeService.getAttributeValues(this.attribute.idAttribute, this.groupAttributeRowId);
    }

    if (this.attributeValues.length === 0 && this.attribute.codeAttributeDataType !== "GA" && this.attribute.isMultiValue !== "Y") {
      // TODO: Decide when to persist this to get the idAttributeValue.
      this.attributeValues.push(<AttributeValue>{
        codeAttributeDataType: this.attribute.codeAttributeDataType,
        idAttributeValue: this.attributeService.getUniqueId(),
        idAttribute: this.attribute.idAttribute,
        idGroupAttribute: this.attribute.idGroupAttribute,
        groupAttributeRowId: this.groupAttributeRowId
      });
    }

    if (this.attribute.codeAttributeDataType === "GA") {
      this.initializeGrid();
    } else if (this.attribute.codeAttributeDataType === "AC") {
      this.initializeAttributeChoices();
    } else if (this.attribute.codeAttributeDataType === "TXT" && !this.attributeValues[0].valueLongText) {
      this.attributeValues[0].valueLongText = <AttributeLongText>{
        idLongText: undefined,
        textData: undefined
      };
    }
    else if (this.attribute.codeAttributeDataType === "DICT") {
      this.loadDictionaryEntries(this.attribute, this.attributeValues, this.groupAttributeRowId);
    }
    else if (this.attribute.codeAttributeDataType === "DT") {
      //Make a proper date for the date time component
      this.attributeValues[0].date = new Date(this.attributeValues[0].valueDateTime);
    }

  }


  loadDictionaryEntries(attribute: Attribute, values: AttributeValue[], groupAttributeRowId: number) {
    //Adding this to subscriptions is probably unnecessary, since the first() will unsubscribe. We only get one value.
    this.subscriptions.add(this.attributeService.getDictionaryEntries(attribute, values, groupAttributeRowId).pipe(first()).subscribe((dto: DictionaryEntriesDTO) => {
      this.dictionaryEntries = dto.entries;

      if (this.attribute.isMultiValue === 'Y') {
        this.initializeMultiDictEntries();
      }

      //If there is a referenced attribute this means this attribute is filtered by another attribute.
      if (dto.referencedAttribute) {
        //Need to subscribe to attribute pushes, in order to update our FILTERED DICTIONARY when the referenced attribute was changed
        this.subscriptions.add(this.attributeService.getAttributeValuePushedSubject().subscribe((av: AttributeValue) => {
          //If it is the referenced attribute, (and for the same row if this is a grid row) we need to update the dictionary entries
          if (av.idAttribute === dto.referencedAttribute.idAttribute
              && ((av.groupAttributeRowId == null && this.groupAttributeRowId == null) || av.groupAttributeRowId === this.groupAttributeRowId)) {

            //Make another call to get the dictionary entries
            //Note that when passing in the referenceAttributeValue, the resulting DTO won't contain a referencedAttribute. A second subscription won't be created.
            this.reloadFilteredDictionaryEntries(attribute, values, groupAttributeRowId, av);
          }
        }));
      }
    }));
  }

  reloadFilteredDictionaryEntries(attribute: Attribute, values: AttributeValue[], groupAttributeRowId: number, referencedAttributeValue: AttributeValue) {
    //Adding this to subscriptions is probably unnecessary, since the first() will unsubscribe. We only get one value.
    this.subscriptions.add(this.attributeService.getDictionaryEntries(attribute, values, groupAttributeRowId, referencedAttributeValue).pipe(first()).subscribe((dto: DictionaryEntriesDTO) => {
      this.dictionaryEntries = dto.entries;

      //Multi
      if (this.attribute.isMultiValue === 'Y') {
        this.initializeMultiDictEntries();

        //Probably need to slice out multi values that don't apply
        this.deleteFilteredOutDictionaryValues();
      }
      //Single
      else {
        //Maybe need to clear out the value if it no longer applies
        if (! dto.entriesIncludeSelectedValue) {
          this.valueDictChange(undefined);
        }
      }
    }));
  }

  deleteFilteredOutDictionaryValues() {
    //Iterate existing values
    for (let attributeValue of this.attributeValues) {
      let applicable:boolean = false;

      //Does it exist in new dicationryEntries?
      for (let entry of this.dictionaryEntries) {
        if (attributeValue.valueIdDictionary && entry.value === attributeValue.valueIdDictionary) {
          //keep
          applicable = true;
          break;
        }
      }

      if (! applicable) {
        //Get rid of values that no longer apply
        this.attributeService.spliceAttributeValueDict(this.attribute, attributeValue.valueIdDictionary)
      }
    }
  }

  initializeMultiDictEntries() {
    for (let entry of this.dictionaryEntries) {
      entry.checked = false;

      for (let attributeValue of this.attributeValues) {
        if (attributeValue.valueIdDictionary && entry.value === attributeValue.valueIdDictionary) {
          entry.checked = true;
          break;
        }
      }
    }
  }

  /**
   * Set a transient value on the attributeChoice to keep track of selected values.
   */
  initializeAttributeChoices() {
    this.attributeChoices = this.attribute.attributeChoices.slice();

    for (let attributeChoice of this.attributeChoices) {
      attributeChoice.value = false;

      for (let attributeValue of this.attributeValues) {
        if (attributeValue.valueAttributeChoice && attributeChoice.idAttributeChoice === attributeValue.valueAttributeChoice.idAttributeChoice) {
          attributeChoice.value = true;
          break;
        }
      }
    }
  }

  /**
   * If the attribute is a grid, find its children attributes which make up the columns and generate data based on all
   * the attribute values by their groupAttributeRowId.
   */
  initializeGrid(): void {
    if (isDevMode()) {
      console.debug("AttributeComponent.initializeGrid");
    }

    let columns: any[] = [];
    let data: any[] = [];

    if (!this.attribute.attributes) {
      return;
    }

    this.childAttributes = <GraphicalAttribute[]>this.attribute.attributes.filter((attribute: GraphicalAttribute) => {
      return attribute["tabOrder"] !== undefined || attribute["w"] !== undefined;
    }).sort((a: GraphicalAttribute, b: GraphicalAttribute) => {
      if (a.tabOrder < b.tabOrder) {
        return -1;
      } else if (a.tabOrder > b.tabOrder) {
        return 1;
      } else {
        return 0;
      }
    });

    if (isDevMode()) {
      console.debug("AttributeComponent.initializeGrid: n Grid Attributes: " + this.childAttributes.length);
    }

    this.initializeGridColumns(this.childAttributes, columns);

    if (isDevMode()) {
      console.debug("AttributeComponent.initializeGrid: n Columns: " + columns.length);
      console.debug(columns);
    }

    if (isDevMode()) {
      console.debug("AttributeComponent.initializeGrid: n attributeValues: " + this.attributeValues.length);
      console.debug(this.attributeValues);
    }


    let sortedRowValues = this.attributeValues.filter((attributeValue) => attributeValue.idGroupAttribute = this.attribute.idAttribute)
      .sort((a: AttributeValue, b: AttributeValue) => {
      if (a.groupAttributeRowId < b.groupAttributeRowId) {
        return -1;
      } else if (a.groupAttributeRowId > b.groupAttributeRowId) {
        return 1;
      } else {
        return 0;
      }
    });

    let n: number = 0;
    for (let datum of sortedRowValues) {
      if (datum.groupAttributeRowId > n) {
        n = datum.groupAttributeRowId;
        data.push({
          groupAttributeRowId: datum.groupAttributeRowId
        });
      }
    }

    if (isDevMode()) {
      console.debug("AttributeComponent.initializeGrid: n Data: " + data.length);
    }

    this.initializeGridValues(data, sortedRowValues);

    if (isDevMode()) {
      console.debug("AttributeComponent.initializeGrid: Data");
      console.debug(data);
    }

    this.gridColumns = columns;
    this.gridData = data;
  }

  /**
   * Generate a column array based on the grouped attributes within the grid.
   *
   * @param {Attribute[]} attributes
   * @param {any[]} columns
   */
  initializeGridColumns(attributes: GraphicalAttribute[], columns: any[]) {

    /*
    columns.push({
      field: "groupAttributeRowId", isKey: true, visible: false
    });*/

    for (const [i, attribute] of attributes.entries()) {
      let column: any = {
        field: attribute.attributeName,
        headerName: attribute.displayName,
        editable: false,
        sortable: true,
        resizable: true,
        filter: false,
        width: attribute.w,
      };

      // the last column should auto fill the remaining space
      if (attribute.w && i < attributes.length - 1) {
        column.suppressSizeToFit = true;
      }

      if (attribute.codeAttributeDataType === "D") {
        column.comparator = DateUtil.dateComparator;
      }
      else if (attribute.codeAttributeDataType === "DT") {
        column.comparator = DateUtil.dateTimeComparator;
      }

      columns.push(column);
    }
  }

  /**
   * After the columns are created, assigned
   *
   * @param {any[]} data
   */
  initializeGridValues(data: any[], attributeValues: AttributeValue[]): void {
    for (let datum of attributeValues) {
      let attribute: Attribute = this.childAttributes.filter((attribute: Attribute) => {
        return attribute.idAttribute === datum.idAttribute;
      })[0];

      //Make a rowValue object with values for each attribute of the row
      let rowValue: any = data.find((row) => row.groupAttributeRowId === datum.groupAttributeRowId);
      if (! rowValue) {
        rowValue = {groupAttributeRowId: datum.groupAttributeRowId};
        data.push(rowValue);
      }

      if (!attribute) {
        console.error("Attribute.initializeGrid: idAttribute " + datum.idAttribute + " not found.");
        continue;
      }

      if (attribute.codeAttributeDataType === "DT") {
        rowValue[attribute.attributeName] = new DatePipe("en-US").transform(new Date(datum.valueDateTime), "M/d/yy, h:mm:ss a");
      }
      else if (attribute.codeAttributeDataType === "AC" && attribute.isMultiValue === "N") {
        if (datum.valueAttributeChoice) {
          rowValue[attribute.attributeName] = datum.valueAttributeChoice.choice;
        }
      }
      else if (attribute.codeAttributeDataType === "AC" && attribute.isMultiValue === "Y") {
        //Concatenated display
        if (rowValue[attribute.attributeName]) {
          rowValue[attribute.attributeName] = rowValue[attribute.attributeName] + ", " + datum.valueAttributeChoice.choice;
        } else {
          rowValue[attribute.attributeName] = datum.valueAttributeChoice.choice;
        }
      }
      else if (attribute.codeAttributeDataType === "DICT") {
        this.subscriptions.add(this.attributeService.getDictionaryEntries(attribute, attributeValues, datum.groupAttributeRowId).pipe(first()).subscribe((dto: DictionaryEntriesDTO) => {
          let entry = dto.entries.find((entry:any) => entry.value === datum.valueIdDictionary);

          if (entry) {
            rowValue[attribute.attributeName] = dto.entries.find((entry:any) => entry.value === datum.valueIdDictionary).display;
            if (this.gridApi) {
              this.gridApi.refreshCells();
            }
          }
        }));
      }
      else if (attribute.codeAttributeDataType === "S" || attribute.codeAttributeDataType === "B" || attribute.codeAttributeDataType === "EB" || attribute.codeAttributeDataType === "CB") {
        rowValue[attribute.attributeName] = datum.valueString;
      }
      else {
        rowValue[attribute.attributeName] = datum.value;
      }
    }
  }

  /**
   * For override.
   */
  refresh(): void {
    if (isDevMode()) {
      console.debug("Attribute.refresh: " + this.attribute.idAttribute);
    }
  }



  valueStringChange(value: any): void {
    if (value === "") {
      value = undefined;
    }

    this.attributeValues[0].valueString = value;
    this.attributeValues[0].value = value;

    this.attributeService.pushAttributeValue(this.attributeValues[0]);
  }

  valueCheckboxChange(event: any) {
    if (event.target.checked) {
      this.attributeValues[0].valueString = "Y";
    }
    else {
      this.attributeValues[0].valueString = "N";
    }
    this.attributeValues[0].value = this.attributeValues[0].valueString;

    this.attributeService.pushAttributeValue(this.attributeValues[0]);
  }

  valueIntegerChange(value: any): void {
    this.attributeValues[0].valueInteger = value;
    this.attributeValues[0].value = (value)?value.toString():undefined;

    this.attributeService.pushAttributeValue(this.attributeValues[0]);
  }

  valueNumericChange(value: any): void {
    this.attributeValues[0].valueNumeric = value;
    this.attributeValues[0].value = (value)?value.toString():undefined;

    this.attributeService.pushAttributeValue(this.attributeValues[0]);
  }

  valueDictChange(value: any): void {
    this.attributeValues[0].valueIdDictionary = value;
    this.attributeValues[0].value = (value)?value.toString():undefined;

    this.attributeService.pushAttributeValue(this.attributeValues[0]);
  }

  valueDateChange(value: any): void {
    this.attributeValues[0].valueDateTime = value;
    this.attributeValues[0].value = (value)?value.toString():undefined;

    this.attributeService.pushAttributeValue(this.attributeValues[0]);
  }

  valueTextChange(value: any): void {
    if (this.attributeValues[0].valueLongText) {
      this.attributeValues[0].valueLongText.textData = value;
    } else {
      this.attributeValues[0].valueLongText = <AttributeLongText>{
        idLongText: undefined,
        textData: value
      };
    }
    this.attributeValues[0].value = (value)?value.toString():undefined;

    this.attributeService.pushAttributeValue(this.attributeValues[0]);
  }

  valueChoiceChange(value: any): void {
    this.attributeValues[0].valueAttributeChoice = this.attributeChoices.find((attr) => attr.idAttributeChoice === value);
    this.attributeValues[0].value = (value)?value.toString():undefined;

    this.attributeService.pushAttributeValue(this.attributeValues[0]);
  }

  valueMultiChoiceChange(attributeChoice: any): void {
    attributeChoice.value = !attributeChoice.value;

    //Remove
    if (!attributeChoice.value) {
      this.attributeService.spliceAttributeValueChoice(this.attribute, attributeChoice);
    }
    //Add
    else {
      this.attributeValues.push(<AttributeValue>{
        codeAttributeDataType: "AC",
        idAttributeValue: this.attributeService.getUniqueId(),
        idAttributeValueSet: this.attributeService.getAttributeValueSet().getValue().idAttributeValueSet,
        idAttribute: this.attribute.idAttribute,
        valueAttributeChoice: attributeChoice,
        groupAttributeRowId: this.groupAttributeRowId,
        idGroupAttribute: this.attribute.idGroupAttribute
      });

      this.attributeService.pushAttributeValueMultiChoice(this.attributeValues[this.attributeValues.length - 1]);
    }
  }

  valueMultiDictChange(entry: any): void {
    entry.checked = !entry.checked;

    //Remove
    if (!entry.checked) {
      this.attributeService.spliceAttributeValueDict(this.attribute, entry.value);
    }
    //Add
    else {
      this.attributeValues.push(<AttributeValue>{
        codeAttributeDataType: "DICT",
        idAttributeValue: this.attributeService.getUniqueId(),
        idAttributeValueSet: this.attributeService.getAttributeValueSet().getValue().idAttributeValueSet,
        idAttribute: this.attribute.idAttribute,
        valueIdDictionary: entry.value,
        groupAttributeRowId: this.groupAttributeRowId,
        idGroupAttribute: this.attribute.idGroupAttribute
      });

      this.attributeService.pushAttributeValueMultiDict(this.attributeValues[this.attributeValues.length - 1]);
    }
  }


  addGridRow(modal: NgbModalRef, idGroupAttribute: number) {
    console.log("add grid row: " + idGroupAttribute);

    //Get a negative ID, so we know this is a new row
    this.editGroupAttributeRowId = this.attributeService.getUniqueId();
    this.editGroupRowAttributes = this.childAttributes;


    this.modalService.open(modal, {windowClass: "modal-lg"}).result.then((result) => {
      if (result === "Save") {
        this.saveGridRow(idGroupAttribute);
      } else if (result === "Cancel") {
        this.cancelEditCodGridRow();
      }
    }, (reason) => {});
  }

  editGridRow(modal: NgbModalRef, idGroupAttribute: number, event: RowDoubleClickedEvent) {
    //EditInline is actually the ONLY way this can be editable, so this is really whether the grid is editable, at all
    if (this.editInline) {
      this.editGroupAttributeRowId = event.data.groupAttributeRowId;
      this.editGroupRowAttributes = this.childAttributes;


      this.modalService.open(modal, {windowClass: "modal-lg"}).result.then((result) => {
        if (result === "Save") {
          this.saveGridRow(idGroupAttribute);
        } else if (result === "Cancel") {
          this.cancelEditCodGridRow();
        }
      }, (reason) => {});
    }
  }

  removeGridRow(idGroupAttribute: number) {
    console.log("remove grid row: " + idGroupAttribute);
    let selectedColumns: RowNode[] = this.gridApi.getSelectedNodes();

    if (selectedColumns.length > 0) {
      this.attributeService.deleteGridRowAttributeValues(idGroupAttribute, selectedColumns[0].data.groupAttributeRowId);
    }
  }

  cancelEditCodGridRow() {
    this.attributeService.clearUpdatedGridRowAttributeValues(this.editGroupAttributeRowId);
    this.editGroupRowAttributes = [];
    this.editGroupAttributeRowId = undefined;
  }


  /**
   * Values for a new grid row are stored separately.  This function will add that data to the current grid data and
   * push the values to the service.
   */
  saveGridRow(idGroupAttribute: number): void {
    console.log("save grid row");

    this.attributeService.saveGridRowAttributeValues(idGroupAttribute, this.editGroupAttributeRowId);

    this.editGroupAttributeRowId = undefined;
    this.editGroupRowAttributes = [];
  }

  private comboFilterValueGetter(params): any {
    let value = params.data[params.colDef.field];
    if (value) {
      let option = ((params.colDef as any).selectOptions as any[]).find((entry) => (entry[params.colDef.selectOptionsValueField] === value));
      return option ? option[params.colDef.selectOptionsDisplayField] : "";
    }
    return "";
  }
}
