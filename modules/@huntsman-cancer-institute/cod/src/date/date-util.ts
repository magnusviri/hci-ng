import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {BehaviorSubject} from "rxjs";
import { parse } from "date-fns";


@Injectable()
export class DateUtil {
  
  static dateComparator(date1, date2) {
    var date1Number = DateUtil.monthToComparableNumber(date1);
    var date2Number = DateUtil.monthToComparableNumber(date2);
    if (date1Number === null && date2Number === null) {
        return 0;
    }
    if (date1Number === null) {
        return -1;
    }
    if (date2Number === null) {
        return 1;
    }
    return date1Number - date2Number;
  }

  static dateTimeComparator(date1, date2){
    var date1Number = DateUtil.daytimeToComparableNumber(date1);
    var date2Number = DateUtil.daytimeToComparableNumber(date2);
    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }
    return date1Number - date2Number;
  }

  static monthToComparableNumber(date) {
    if (date === undefined || date === null || date.length !== 10) {
      return null;
    }
    
    var newDate = parse(date, "MM/dd/yyyy", new Date());
    
    if(newDate.getTime() == NaN){//parse error
      return null;
    }
    return newDate.getTime();
  }

  static  daytimeToComparableNumber(date) {
    if (date === undefined || date === null || date === "") {
      return null;
    }

    var newDate = parse(date, "MM/dd/yyyy HH:mm:ss", new Date());
    if(newDate.getTime() == NaN){//parse error
      return null;
    }
    
    return newDate.getTime();
  }
}


