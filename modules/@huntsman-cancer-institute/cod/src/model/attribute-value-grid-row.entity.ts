import {AttributeValue} from "./attribute-value.entity";

export interface AttributeValueGridRow {
  idAttributeValueSet: number;
  idGroupAttribute: number;
  groupAttributeRowId: number;
  attributeValues: AttributeValue[];
}
