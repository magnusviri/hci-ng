import {AttributeValue} from "./attribute-value.entity";
import {AttributeValueGridRow} from "./attribute-value-grid-row.entity";
import {ExtractableFieldStatus} from "./extractable-field-status.entity";

export interface AttributeValueSet {
  idAttributeValueSet: number;
  attributeValues: AttributeValue[];
  attributeValueGridRows?: AttributeValueGridRow[];
  extractableFieldStatuses?: ExtractableFieldStatus[];
  createDateTime?: string;
  extractionValidated?: string;
}
