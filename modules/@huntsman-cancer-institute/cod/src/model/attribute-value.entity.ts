import {AttributeChoice} from "./attribute-choice.entity";
import {AttributeDictionary} from "./attribute-dictionary.entity";
import {AttributeLongText} from "./attribute-long-text.entity";

export class AttributeValue {
  idAttributeValue: number;
  idAttributeValueSet: number;
  idAttribute: number;
  codeAttributeDataType: string;
  valueString?: string;
  value?:string;
  valueDate?: string;
  valueDateTime?: string;
  date?: Date;
  valueInteger?: number;
  valueNumeric?: number;
  valueBoolean?: string;
  valueExtendedBoolean?: string;
  valueAttributeChoice?: AttributeChoice;
  valueIdDictionary?: string;
  valueLongText?: AttributeLongText;
  attributeDictionary?: AttributeDictionary;
  idGroupAttribute?: number;
  groupAttributeRowId?: number;
  extractAttributeName?: string;
  extractionStatus?: string;
  extractReportAlias?: string;
  extractionDate?: string;
}
