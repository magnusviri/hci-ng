import {Attribute} from "./attribute.entity";

export class DictionaryEntriesDTO {
  entries: any[];
  entriesIncludeSelectedValue: boolean;
  referencedAttribute: Attribute;
}