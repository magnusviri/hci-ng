
export interface AttributeChoice {
  idAttributeChoice: number;
  idAttribute: number;
  choice: string;
  description: string;
  sortOrder: number;
  isActive: string;
  isAjcc: string;
  isCap: string;
  ajccStagingCode: string;
  icdoCode: string;
  icd9Code: string;
  researchCode: string;
  codeHciConcept: number;
  value: any;
}
